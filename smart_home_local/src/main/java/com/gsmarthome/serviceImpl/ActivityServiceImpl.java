package com.gsmarthome.serviceImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.service.ActivityService;
import com.gsmarthome.util.Utility;

public class ActivityServiceImpl implements ActivityService {

	static Logger logger = Logger.getLogger(ActivityServiceImpl.class.getName());
	
	private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	
	@Autowired
	ActivityRepository activityRepository;

	@Override
	public Activity addActivity(Activity activity) {
		
		activityRepository.save(activity);
		logger.debug("Add Activity saved to DB Sucessfully."+"Activity ID : "+activity.getId());
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<ActivityResponseDTO> getActivityList() {
		Date date;
		
		Page<Integer> page = activityRepository.findLatestId(new PageRequest(0, 20));
		
		Activity activity[] = activityRepository.selectByExcludedId(page.getContent());

		List<ActivityResponseDTO> listActivityRespDTO = new ArrayList<ActivityResponseDTO>();

		for (int i = 0; i < activity.length; i++) {
			if (i == 20)
				break;
			ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();

			try {
				
				date = format.parse(activity[i].getCreated_date().toString());
				
				activityResponseDTO.setCreated_date((date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
						+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString()));
				
			} catch (ParseException parseException) {
				logger.error("Get Activity List Date Parse Exception."+activityResponseDTO.getId());
				parseException.printStackTrace();
			}

			activityResponseDTO.setId(activity[i].getId());
			activityResponseDTO.setImage(activity[i].getImage());
			activityResponseDTO.setMessage(activity[i].getMessage());
			activityResponseDTO.setMessageType(activity[i].getMessageType());
			activityResponseDTO.setRoomname(activity[i].getRoomname());

			listActivityRespDTO.add(activityResponseDTO);
			
			logger.error("Get Activity List Response Created Successfully."+
			"Room Id: "+listActivityRespDTO.get(0).getId()+
			"Room Name: "+listActivityRespDTO.get(0).getRoomname());
		}

		return listActivityRespDTO;

	}

/*	public static String getTime(String time) {
		String time1 = "";

		try {
			String _24HourTime = time;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);
			System.out.println(_24HourDt);
			System.out.println(_12HourSDF.format(_24HourDt));
			time1 = _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time1;
	}

	public static String theMonth(int month) {
		String[] monthNames = { Constants.JAN, Constants.FEB, Constants.MAR, Constants.APR, 
				Constants.MAY, Constants.JUN, Constants.JUL, Constants.AUG, Constants.SEP, Constants.OCT, Constants.NOV, Constants.DEC };
		return monthNames[month];
	}*/

}
