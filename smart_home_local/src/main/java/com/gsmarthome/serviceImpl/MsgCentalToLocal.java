package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Command;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;

import MqttReadDataPaho.MqttlConnection;

@Component
public class MsgCentalToLocal {
	
	static Logger logger = Logger.getLogger(SwitchServiceImpl.class.getName());
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:00");


	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	CommandRepository commandRepository;

	
	public void switchonoff(MqttMessage mqttmessage)
	{
System.out.println("INNNNNN");

		Switch switchValidOrInvalid =  null;
		Activity activity = new Activity();
		JSONObject jMessage = new JSONObject();
		Panel panel = new Panel();

		
		String msg1="";
		msg1=""+mqttmessage;

System.out.println("mqttmessage = "+mqttmessage);
if(!msg1.equalsIgnoreCase(""))
{
		String msg2[]=msg1.split("/");

		Long user_id=(long) Integer.parseInt(msg2[0]);
		Long switch_id=(long) Integer.parseInt(msg2[1]);

System.out.println("user_id = "+user_id);
System.out.println("switch_id = "+switch_id);

		UserDetails userDetails = userDetailsRepository.findOne((long) 133);

System.out.println("userDetails = "+userDetails.toString());

		if(userDetails != null){
			switchValidOrInvalid = switchRepository.findOne(switch_id);
		}

		

System.out.println("switchValidOrInvalid = "+switchValidOrInvalid.toString());

		try {

			int panelId = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(panelId);
			
			System.out.println("panelId= "+panelId);
			System.out.println("panel= "+panel.toString());

			if (panel != null) {

				if(switchValidOrInvalid.getHideStatus().equals("0") && switchValidOrInvalid.getIotProduct().getRoom().getHideStatus().equals("0")){
				

					try {
						String topic = panel.getPanelName() + "/out";
						System.out.println("topic = "+topic);
						StringBuffer switchOnOffCommand = new StringBuffer("$[");
						switchOnOffCommand.append(switchValidOrInvalid.getSwitchNumber());
						// command formation
						// switch number dimmer value lock
						if (switchValidOrInvalid.getSwitchNumber() < 5) {
							switchOnOffCommand.append("0");
							switchOnOffCommand.append(msg2[2]);
							switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());

						} else {
							if (msg2[2].toString().equals("0")) {
								switchOnOffCommand.append("00");
							} else {
								int dimmerValue = Integer.parseInt(msg2[3]);
								if (dimmerValue < 10) {
									switchOnOffCommand.append("0");
									switchOnOffCommand.append(msg2[3]);

								} else {
									switchOnOffCommand.append(msg2[3]);
								}

							}
							switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());
						}

						switchOnOffCommand.append("]");

						System.out.println("switchOnOffCommand= "+switchOnOffCommand);
						MqttMessage messageOff = new MqttMessage(switchOnOffCommand.toString().getBytes());
						
						messageOff.setQos(Constants.QoS_EXACTLY_ONCE);
						System.out.println("messageOff Central = "+messageOff);
						
						MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageOff);
						
						
						Command commandSave = new Command(String.valueOf(switchOnOffCommand),
								dateFormat.format(new Date()).toString());
						
						saveSwitchCommand(commandSave, panel);

					} catch (Exception me) {
						me.printStackTrace();
					}
					
				}
				
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
}
		
		
		
		
		
		
		
		
		/*
		
		try {

		String topic = "panel1" + "/out";
		StringBuffer switchOnOffCommand = new StringBuffer("$[");
		switchOnOffCommand.append(msg2[1]);
		// command formation
		// switch number dimmer value lock
		
			switchOnOffCommand.append("0");
			switchOnOffCommand.append(msg2[2]);
			switchOnOffCommand.append("0");
		    switchOnOffCommand.append("]");

		MqttMessage messageOff = new MqttMessage(switchOnOffCommand.toString().getBytes());
		System.out.println("messageOff = "+messageOff);
		
		messageOff.setQos(Constants.QoS_EXACTLY_ONCE);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageOff);
					
					} catch (MqttException e) {
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+topic);
						e.printStackTrace();
					}
				}
			});
			
			}catch (Exception e) {
				// TODO: handle exception
			}
			*/
	}

	public boolean saveSwitchCommand(Command command, Panel panel) {
		commandRepository.save(command);
		return false;
	}
	
}
