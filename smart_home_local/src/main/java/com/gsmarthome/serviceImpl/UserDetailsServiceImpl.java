package com.gsmarthome.serviceImpl;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.BackupRequestConfigDTO;
import com.gsmarthome.dto.BackupResponseConfigDTO;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdatePasswordDTO;
import com.gsmarthome.dto.UpdatePasswordOTPDTO;
import com.gsmarthome.dto.UpdateProfileDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UpdateUserTypeDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserShareControlDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.ValidateUserDTO;
import com.gsmarthome.dto.VerifyEmailOTP;
import com.gsmarthome.dto.VerifyOTPDTO;
import com.gsmarthome.dto.VerifyOTPResponse;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.DeviceDetails;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.InstallationConfiguration;
import com.gsmarthome.entity.Profile;
import com.gsmarthome.entity.ScheduleSwitch;
import com.gsmarthome.entity.ShareControlDetails;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.entity.UserOTP;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.DeviceDetailsRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.InstallationConfigurationRepository;
import com.gsmarthome.repository.ProfileRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.ShareControlDetailsRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.repository.UserOTPRepository;
import com.gsmarthome.security.domain.User;
import com.gsmarthome.security.token.TokenAuthenticationService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.util.Utility;
import com.gsmarthome.util.ValidationErrorHandler;

import MqttReadDataPaho.MqttlConnection;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

	final static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class.getName());
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	InstallationConfigurationRepository installationConfigurationRepository;
	
	@Autowired
	HomeRepository homeRepository;
	
	@Autowired
	ProfileRepository profileRepository;
	
	@Autowired
	UserOTPRepository userOTPRepository;
	
	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;
	
	@Autowired
	DeviceDetailsRepository deviceDetailsRepository;
	
	@Autowired
	ShareControlDetailsRepository shareControlDetailsRepository;
	
	@Autowired
	ActivityRepository activityRepository;
	
	
	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@Autowired
	private SecureRandom secureRandom;
	
	@Value("${EMAIL_ALREADY_PRESENT}")
	private String EMAIL_ALREADY_PRESENT;
	
	@Value("${INVALID_HOME_ID}")
	private String INVALID_HOME_ID;
	
	@Value("${EMAIL_CANNOT_CHNAGE_TO_EXISTING_USER}")
	private String EMAIL_CANNOT_CHNAGE_TO_EXISTING_USER;
	
	@Value("${PHONE_NUMBER_ALREADY_PRESENT}")
	private String PHONE_NUMBER_ALREADY_PRESENT;
	
	@Value("${USER_TYPE_SUPER_ADMIN}")
	private Integer USER_TYPE_SUPER_ADMIN;
	
	@Value("${USER_DETAILS_NOT_PRESENT}")
	private String USER_DETAILS_NOT_PRESENT;
			
	@Value("${USER_DETAILS_UPDATE_PASSWORD_MISMATCH}")
	private String USER_DETAILS_UPDATE_PASSWORD_MISMATCH;
	
	@Value("${USER_DETAILS_UPDATE_PASSWORD_SUCCESS}")
	private String USER_DETAILS_UPDATE_PASSWORD_SUCCESS;
	
	@Value("${USER_DETAILS_NOT_PRESENT_FOR_EMAIL}")
	private String USER_DETAILS_NOT_PRESENT_FOR_EMAIL;
	
	@Value("${USER_DETAILS_OTP_MISMATCH}")
	private String USER_DETAILS_OTP_MISMATCH;
	
	@Value("${USER_DETAILS_OTP_RECORD_NOT_FOUND}")
	private String USER_DETAILS_OTP_RECORD_NOT_FOUND;
	
	@Value("${INVALID_REQUEST}")
	private String INVALID_REQUEST;
	
	@Value("${DEVICE_ID_ALREADY_USED}")
	private String DEVICE_ID_ALREADY_USED;
	
	@Value("${DEVICE_ID_NOT_PRESENT}")
	private String DEVICE_ID_NOT_PRESENT;
	
	@Value("${SHARE_CONTROL_OTP_MISMATCH}")
	private String SHARE_CONTROL_OTP_MISMATCH;
	
	@Value("${SHARE_CONTROL_DETAILS_NOT_PRESENT}")
	private String SHARE_CONTROL_DETAILS_NOT_PRESENT;
	
	@Value("${EMAIL_ALREADY_PRESENT_AND_VERIFIED}")
	private String EMAIL_ALREADY_PRESENT_AND_VERIFIED;
	
	@Value("${EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED}")
	private String EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED;
	
	@Value("${USER_DETAILS_UPDATE_PROFILE_SUCCESS}")
	private String USER_DETAILS_UPDATE_PROFILE_SUCCESS;
	
	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	
	@Override
	public ResponseDTO<UserDetails> save(UserDetails userDetails, Boolean shareControl, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()){
			logger.error("User save Exception Binding Result "+ API_STATUS_FAILURE +" "+ bindingResult);
			return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, ValidationErrorHandler.getErrorMessage(bindingResult));
		}
			if(userDetails.getId() == null)
			{
				UserDetails userDetailsByEmail = userDetailsRepository.findByEmail(userDetails.getEmail());
				
				if(userDetailsByEmail != null)
				{
					if(userDetailsByEmail.getIsEmailVerified())
					{
						return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, EMAIL_ALREADY_PRESENT_AND_VERIFIED);
					}
					else
					{
						sendOTP(new EmailDTO(userDetails.getEmail()));
						return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, EMAIL_ALREADY_PRESENT_AND_NOT_VERIFIED);
					}
				}
				else if(userDetailsRepository.findByPhoneNumber(userDetails.getPhoneNumber())!=null)
					return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, PHONE_NUMBER_ALREADY_PRESENT);
							
				userDetails.setIsEmailVerified(false);
				userDetails.setIsFirstLogin(true);
				userDetails.setUserType(null);
				
				UserDetails details = userDetailsRepository.save(userDetails);
				logger.info("User Details Saved");
				
				sendOTP(new EmailDTO(userDetails.getEmail()));
				
				return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, details, API_STATUS_SUCCESS);
			}
			else
			{
				UserDetails details = userDetailsRepository.findOne(userDetails.getId());
				
				if(details == null)
					return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null,USER_DETAILS_NOT_PRESENT);
				
				if(userDetails.getEmail() != null)
					details.setEmail(userDetails.getEmail());
				if(userDetails.getFirstName()!=null)
					details.setFirstName(userDetails.getFirstName());
				if(userDetails.getImage()!=null)
					details.setImage(userDetails.getImage());
				if(userDetails.getLastName()!=null)
					details.setLastName(userDetails.getLastName());
				if(userDetails.getPhoneNumber()!=null)
					details.setPhoneNumber(userDetails.getPhoneNumber());				
				userDetailsRepository.save(details);					
				return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, details,API_STATUS_SUCCESS);
			}
	}
	
	//change password
	@Override
	public ResponseDTO<Boolean> updatePassword(UpdatePasswordDTO passwordDTO) {
		
		UserDetails details=userDetailsRepository.findOne(passwordDTO.getUserId());
		
		if(details==null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);
		
		if(!passwordDTO.getOldPassword().equals(details.getPassword()))
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_UPDATE_PASSWORD_MISMATCH);
		
		details.setPassword(passwordDTO.getNewPassword());		
		userDetailsRepository.save(details);		
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, USER_DETAILS_UPDATE_PASSWORD_SUCCESS);
	}
	
	//forgot password	
	@Override
	public ResponseDTO<Boolean> updatePasswordOTP(UpdatePasswordOTPDTO passwordDTO) {
		
		UserDetails details=userDetailsRepository.findOne(passwordDTO.getUserId());		
		if(details==null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);	
		
		details.setPassword(passwordDTO.getNewPassword());		
		userDetailsRepository.save(details);		
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, USER_DETAILS_UPDATE_PASSWORD_SUCCESS);
	}

	@Override
	public ResponseDTO<UserIdDTO> sendOTP(EmailDTO emailDTO) {
		
		UserDetails userDetails=userDetailsRepository.findByEmail(emailDTO.getEmail());		
		if(userDetails == null)
			return new ResponseDTO<UserIdDTO>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT_FOR_EMAIL);
		else
		{
			UserOTP old=userOTPRepository.findByUserId(userDetails.getId());			
			UserOTP userOTP=null;			
			if(old!=null)
			{
				old.setOtp(genarateMobilePin().toString());
				userOTP=old;
			}
			else
			{
				userOTP=new UserOTP(userDetails.getId(),genarateMobilePin().toString());
			}
			//	mailSenderJava.sendEmail(userDetails.getEmail(), "New OTP Request", "The OTP is "+userOTP.getOtp());
			//	mailSenderJava.SendAWSMail(userDetails.getEmail(), "New OTP Request", "The OTP is " + userOTP.getOtp());
			String messeage="The OTP is "+userOTP.getOtp();
			userOTPRepository.save(userOTP);
			return new ResponseDTO<UserIdDTO>(API_STATUS_SUCCESS, new UserIdDTO(userDetails.getId()), null);
		}
	}
	
	@Override
	public String sendHomeVerificationOTP(EmailDTO emailDTO) {
		
		UserDetails userDetails=userDetailsRepository.findByEmail(emailDTO.getEmail());		
		if(userDetails==null)
			return null;
		else
		{
			UserOTP old=userOTPRepository.findByUserId(userDetails.getId());
			String OTP=genarateMobilePin().toString();			
			UserOTP userOTP=null;			
			if(old!=null)
			{
				old.setOtp(OTP);
				userOTP=old;
			}
			else
			{
				userOTP=new UserOTP(userDetails.getId(),OTP);
			}			
			//mailSenderJava.sendEmail(userDetails.getEmail(), "New OTP Request", "The OTP is "+userOTP.getOtp());			
			userOTPRepository.save(userOTP);			
			return OTP;
		}

	}
	
	public  Integer genarateMobilePin() {

		int number=secureRandom.nextInt(9)+1;
		
		for(int i=0;i<5;i++)
		{
			number=(number*10)+secureRandom.nextInt(10);
		}	
		
		return number;
	}

	@Override
	public ResponseDTO<VerifyOTPResponse> verifyOTP(VerifyOTPDTO emailDTO) {
		
		UserOTP userIdDTO = userOTPRepository.findByUserId(emailDTO.getUserId());

		if(userIdDTO != null && userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp()))
		{
			
			String x_auth_token = tokenAuthenticationService.getToken(new User(0, emailDTO.getUserId(), new Date().getTime()+900000));
			
			return new ResponseDTO<VerifyOTPResponse>(API_STATUS_SUCCESS, new VerifyOTPResponse(userIdDTO.getUserId(),x_auth_token));
		}
		else
		{
			if(userIdDTO != null && !userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp()))
				return new ResponseDTO<VerifyOTPResponse>(API_STATUS_FAILURE, null,USER_DETAILS_OTP_MISMATCH);
			
		}
		
		return new ResponseDTO<VerifyOTPResponse>(API_STATUS_FAILURE, null,null);
	}

	@Override
	public ResponseDTO<Boolean> verifyEmailOTP(VerifyEmailOTP emailDTO) {
		
		UserDetails userDetails=userDetailsRepository.findByEmail(emailDTO.getEmail());
		
		if(userDetails==null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT_FOR_EMAIL);
		
		UserOTP userIdDTO=userOTPRepository.findByUserId(userDetails.getId());
		
		if(userIdDTO != null && userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp()))
		{
			
			userDetails.setIsEmailVerified(true);
			
			userDetailsRepository.save(userDetails);
			
			return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true);
		}
		else
		{
			if(userIdDTO != null && !userIdDTO.getOtp().equalsIgnoreCase(emailDTO.getOtp()))
				return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false,USER_DETAILS_OTP_MISMATCH);
			
		}
		
		return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false,USER_DETAILS_OTP_RECORD_NOT_FOUND);
		
	}

	@Override
	public ResponseDTO<Integer> validateUser(ValidateUserDTO validateUserDTO) {
		
		if(validateUserDTO.getDeviceId() != null && validateUserDTO.getShareControlOTP() != null)
			return new ResponseDTO<Integer>(API_STATUS_FAILURE, null,INVALID_REQUEST);
		
		if(validateUserDTO.getDeviceId() != null)
		{
			DeviceDetails deviceDetails = deviceDetailsRepository.findOne(validateUserDTO.getDeviceId());
			
			if(deviceDetails == null)
				return new ResponseDTO<Integer>(API_STATUS_FAILURE, null,DEVICE_ID_NOT_PRESENT);
			
			UserDetails userDetails=userDetailsRepository.findByEmail(validateUserDTO.getEmail());
			
			if(deviceDetails.getAdminUser() == null)
			{
				deviceDetails.setAdminUser(userDetails.getId());
				
				deviceDetailsRepository.save(deviceDetails);
				
				List<UserDetails> userList=new ArrayList<UserDetails>();
				
				userList.add(userDetails);
				
				Home home=new Home(userDetails.getFirstName()+" "+userDetails.getLastName()+"'s Home", userList, null);
				
				Home savedHome = homeRepository.save(home);
			
				userDetails.setHome(savedHome);				
				//
				userDetails.setUserType(USER_TYPE_SUPER_ADMIN);
				userDetails.setIsFirstLogin(false);
				userDetailsRepository.save(userDetails);
				
				return new ResponseDTO<Integer>(API_STATUS_SUCCESS, USER_TYPE_SUPER_ADMIN,null);
				
			}
			else
			{
				
				Long userid=deviceDetails.getAdminUser();
				UserDetails existingUser=userDetailsRepository.findOne(userid);
			
				return new ResponseDTO<Integer>(API_STATUS_FAILURE, null,DEVICE_ID_ALREADY_USED+" by "+existingUser.getFirstName()+" "+existingUser.getLastName());
			}
		}
		else
		{
			ShareControlDetails controlDetails=shareControlDetailsRepository.findOne(validateUserDTO.getEmail());
			
			if(controlDetails==null)
				return new ResponseDTO<Integer>(API_STATUS_FAILURE, null,SHARE_CONTROL_DETAILS_NOT_PRESENT);
			
			if(controlDetails.getOtp().equals(validateUserDTO.getShareControlOTP()))
			{
				UserDetails userDetailsValidate=userDetailsRepository.findByEmail(validateUserDTO.getEmail());
				
				UserDetails userDetails=userDetailsRepository.findOne(controlDetails.getAdminUser());
				
				Home home=userDetails.getHome();
				
				home.getUserList().add(userDetailsValidate);
				
				Home savedHome = homeRepository.save(home);
			
				userDetailsValidate.setUserType(controlDetails.getUserType());
				userDetailsValidate.setHome(savedHome);
				
				userDetailsValidate.setIsFirstLogin(false);
				userDetailsRepository.save(userDetailsValidate);
				
				return new ResponseDTO<Integer>(API_STATUS_SUCCESS, controlDetails.getUserType(),null);
				
			}
			else
			{
				return new ResponseDTO<Integer>(API_STATUS_FAILURE, null,SHARE_CONTROL_OTP_MISMATCH);
			}

		}
		
	}
	
	@Override
	public ResponseDTO<Boolean> userShareControl(UserShareControlDTO shareControlDTO, BindingResult bindingResult) {
<<<<<<< HEAD

=======
System.out.println("INNNN \n ");
>>>>>>> stage

		try
		{
		UserDetails adminUserDetails = userDetailsRepository.findOne(shareControlDTO.getAdminUserId());

		if (adminUserDetails == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);
		
		
		if(shareControlDetailsRepository.findByEmail(shareControlDTO.getEmail()) == null &&
				shareControlDetailsRepository.findByPhoneNumber(shareControlDTO.getPhoneNumber()) == null &&
				userDetailsRepository.findByEmail(shareControlDTO.getEmail()) == null &&
				userDetailsRepository.findByPhoneNumber(shareControlDTO.getPhoneNumber()) == null){
			
			
			ShareControlDetails shareControlDetails = new ShareControlDetails(shareControlDTO.getPhoneNumber(),shareControlDTO.getEmail(),
					shareControlDTO.getAdminUserId(), shareControlDTO.getUserType(), genarateMobilePin().toString());

			shareControlDetailsRepository.save(shareControlDetails);
			
		}else{
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, "User Email or Phone Number Already Present");
		}
		
		Activity activity= new Activity();
		if(adminUserDetails.getImage()==null)
		{
			activity.setImage("");
		}
		else
		{
			activity.setImage(adminUserDetails.getId()+ ".png");
			//activity.setImage(IMAGE_LOCATION + "/" + adminUserDetails.getId()+".png");
		}
		activity.setMessage(adminUserDetails.getFirstName().toString()+" share control to  " + shareControlDTO.getEmail());
		activity.setMessageType("Share_Control");
		activity.setRoomname("");
		activity.setCreated_date(dateFormat.format(new Date()).toString());
		activity.setMessageFrom("Internet");
		
		Activity addactivity= new Activity();
		addactivity = activityRepository.save(activity);
		
		JSONObject jMessage = new JSONObject();		
		jMessage.put(Constants.STATUS, "Share_Control");
		jMessage.put(Constants.USER_ID, adminUserDetails.getId().toString());
		jMessage.put(Constants.USER_NAME, adminUserDetails.getFirstName().toString());
		jMessage.put(Constants.MESSAGE, adminUserDetails.getFirstName().toString()+" share control to " +shareControlDTO.getEmail());
		jMessage.put(Constants.SWITCH_ID," "); 
		jMessage.put(Constants.SWITCH_NUMBER, "");
		jMessage.put(Constants.SWITCH_NAME, "");
		jMessage.put(Constants.SWITCH_STATUS, "");
		jMessage.put(Constants.DIMMER_STATUS, "");
		jMessage.put(Constants.DIMMER_VALUE, "");
		jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());		
		if(adminUserDetails.getImage()==null)
		{
			jMessage.put(Constants.USER_IMAGE, "");
		}
		else
		{
			jMessage.put(Constants.USER_IMAGE, adminUserDetails.getId()+".png");
			
		}
		jMessage.put(Constants.ROOM_ID, " ");
		jMessage.put(Constants.ROOM_NAME, " ");
		Date dateCurrent = new Date();
		jMessage.put(Constants.TIME,dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth())+" "+Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

		jMessage.put(Constants.MESSAGE_FROM, "Internet");

		System.out.print(jMessage);
		MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
		refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
		// refreshBackMessage to mobile or to other thin end clients
		MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
		String publicTopic = "refreshIntBackGenieHomeId_" + adminUserDetails.getHome().getId().toString();			
		
		//child thread to publish on Internet 
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
		});
		
		}
		catch(Exception ex)
		{
			return null;
		}
		
		
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, "Share Control Given");

	}

/*	public static String geTime(String time) {
		String time1 = "";
		
		try {
			String _24HourTime = time;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);
			System.out.println(_24HourDt);
			System.out.println(_12HourSDF.format(_24HourDt));
			time1 = _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time1;
	}*/
	@Override
	public ResponseDTO<List<UserListDTO>> getByUser(UserIdDTO userIdDTO) {
		
		UserDetails userDetails=userDetailsRepository.findOne(userIdDTO.getUserId());
		
		List<UserListDTO> returnList=new ArrayList<UserListDTO>();
		
		if(userDetails==null)
			return new ResponseDTO<List<UserListDTO>>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);
		
		List<UserDetails> userList=userDetails.getHome().getUserList();
		
		for(UserDetails details:userList)
		{
					returnList.add(new UserListDTO(details.getId(), details.getFirstName(), details.getLastName(),
					details.getEmail(), details.getPhoneNumber(), details.getUserType(), details.getImage(),
					details.getIsEmailVerified(), details.getIsFirstLogin()));
		}
		
		return new ResponseDTO<List<UserListDTO>>(API_STATUS_SUCCESS, returnList, null);
	}

	@Override
	public ResponseDTO<Boolean> updateUserType(UpdateUserTypeDTO updateUserTypeDTO) {
		try
		{
		UserDetails userDetails = userDetailsRepository.findOne(updateUserTypeDTO.getUserId());
		int ioldusertype = userDetails.getUserType();
		String oldUserType="";
		String userType="";
		UserDetails adminUserDetails = userDetailsRepository.findOne(updateUserTypeDTO.getAdminUserID());
		
		if(ioldusertype == 0)
		{
			oldUserType=Constants.USER_TYPE_ADMIN;
		}
		else if(ioldusertype == 1)
		{
			oldUserType=Constants.USER_TYPE_NORMAL;
			
		}
		
		else if(ioldusertype == 2)
		{
			oldUserType=Constants.USER_TYPE_MODERATE;
			
		}
		
		if(updateUserTypeDTO.getNewUserType() == 0)
		{
			userType=Constants.USER_TYPE_ADMIN;
		}
		else if(updateUserTypeDTO.getNewUserType() == 1)
		{
			userType=Constants.USER_TYPE_NORMAL;
			
		}
		
		else if(updateUserTypeDTO.getNewUserType() == 2)
		{
			userType=Constants.USER_TYPE_MODERATE;
			
		}
		
		Activity activity= new Activity();
		if(userDetails.getImage()==null)
		{
			activity.setImage("");
		}
		else
		{
			activity.setImage(userDetails.getId()+ ".png");
		}
		activity.setMessage(adminUserDetails.getFirstName()+ " updated " +  userDetails.getFirstName().toString()+ "'s usertype from " + oldUserType.toString() + " to "+ userType);
		activity.setMessageType("Update_Share_Control");
		activity.setRoomname("");
		activity.setCreated_date(dateFormat.format(new Date()).toString());
		activity.setMessageFrom("Internet");
		
		Activity addactivity= new Activity();
		addactivity = activityRepository.save(activity);
		
		JSONObject jMessage = new JSONObject();		
		jMessage.put(Constants.STATUS, "Update_Share_Control");
		jMessage.put(Constants.USER_ID, userDetails.getId().toString());
		jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
		jMessage.put(Constants.MESSAGE, adminUserDetails.getFirstName()+ " updated " +  userDetails.getFirstName().toString()+ "'s susertype from " + oldUserType.toString() + " to "+ userType);
		jMessage.put(Constants.SWITCH_ID," "); 
		jMessage.put(Constants.SWITCH_NUMBER, "");
		jMessage.put(Constants.SWITCH_NAME, "");
		jMessage.put(Constants.SWITCH_STATUS, "");
		jMessage.put(Constants.DIMMER_STATUS, "");
		jMessage.put(Constants.DIMMER_VALUE, "");
		jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());		
		if(userDetails.getImage() == null)
		{
			jMessage.put(Constants.USER_IMAGE, "");
		}
		else
		{
			jMessage.put(Constants.USER_IMAGE, userDetails.getId()+".png");
			
		}
		jMessage.put(Constants.ROOM_ID, " ");
		jMessage.put(Constants.ROOM_NAME, " ");
		Date dateCurrent = new Date();
		jMessage.put(Constants.TIME,dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth())+" "+Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

		jMessage.put(Constants.MESSAGE_FROM, "Internet");

		System.out.print(jMessage);
		MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
		refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
		// refreshBackMessage to mobile or to other thin end clients
		MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
		String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();			
		
		//child thread to publish on Internet 
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
		});

		
		userDetails.setUserType(updateUserTypeDTO.getNewUserType());
		
		userDetailsRepository.save(userDetails);
		
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Update User Type Method, Exception Message: "+ex.getMessage()+"Cause: "+ex.getCause()); 
			return null;
		}
	}

/*	public static String theMonth(int month) {
		String[] monthNames = { Constants.JAN, Constants.FEB, Constants.MAR, Constants.APR, Constants.MAY, 
				Constants.JUN, Constants.JUL, Constants.AUG, Constants.SEP, Constants.OCT, Constants.NOV, Constants.DEC };
		return monthNames[month];
	}*/

	@Override
	public ResponseDTO<Boolean> updateProfile(UpdateProfileDTO updateProfileDTO) {
	UserDetails details = userDetailsRepository.findOne(updateProfileDTO.getUserId());
		
		if(details == null)
			return new ResponseDTO<Boolean>(API_STATUS_FAILURE, false, USER_DETAILS_NOT_PRESENT);		
		
		details.setFirstName(updateProfileDTO.getFirstName());
		details.setLastName(updateProfileDTO.getLastName());
		details.setPhoneNumber(updateProfileDTO.getPhoneNumber());
		details.setBirthDate(updateProfileDTO.getBirthDate());
		
		userDetailsRepository.save(details);
		
		return new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true, USER_DETAILS_UPDATE_PROFILE_SUCCESS);

	}

	@Override
	public Boolean updateUserDevicetype(UpdateUserDeviceDTO updateUserDeviceDTO) {
	
		UserDetails details = userDetailsRepository.findOne(updateUserDeviceDTO.getUserId());
		
		if(details == null)
			return false;
		
		if(updateUserDeviceDTO.getDeviceId() != null){
			details.setDeviceId(updateUserDeviceDTO.getDeviceId());
		}
		
		details.setDeviceType(updateUserDeviceDTO.getDeviceType());
		
		userDetailsRepository.save(details);
		
		return true;
	}

	@Override
	public ResponseDTO<BackupResponseConfigDTO> backupConfig(BackupRequestConfigDTO backupRequestConfigDTO) {
		
		BackupResponseConfigDTO backupResponseConfigDTO = new BackupResponseConfigDTO();
		
		try{
			Home home = homeRepository.findOne(backupRequestConfigDTO.getHomeId());
			
			UserDetails userDetails[] = userDetailsRepository.findByHome(home);
			
			if(userDetails.length > 0){
				InstallationConfiguration objInstallationConfig[] =  installationConfigurationRepository.getInstallationConfiguration();
				
				for(int i =0; i < objInstallationConfig.length; i++){
					backupResponseConfigDTO.setSecureUsername(objInstallationConfig[i].getSecureSSId());
					backupResponseConfigDTO.setSecurePassword(objInstallationConfig[i].getSecurePassword());	
					if(backupResponseConfigDTO.getSecurePassword() != null && backupResponseConfigDTO.getSecureUsername() != null){					
						break;
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			logger.error("Excertion Message: "+ex.getMessage()+" Cause: "+ex.getCause());
		}
		
		return new ResponseDTO<BackupResponseConfigDTO>(API_STATUS_SUCCESS, backupResponseConfigDTO, "");
	}
	
	@Override
	public ResponseDTO<UserDetails> syncShareControlData(UserdetailsResponseDTO userDetails, Boolean shareControl,
			BindingResult bindingResult) {
		if(bindingResult.hasErrors())
			return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null, ValidationErrorHandler.getErrorMessage(bindingResult));
		UserDetails findUserDetails = userDetailsRepository.findOne(userDetails.getId());
		UserDetails userDetailsFindByEmail = userDetailsRepository.findByEmail(userDetails.getEmail());
		
		if(userDetailsFindByEmail != null && findUserDetails == null && userDetailsFindByEmail.getHome().getId().equals(userDetails.getHomeid())){
	
			//if the existing user has profile created then delete them first
			Profile profile[] = profileRepository.findByUserId(userDetailsFindByEmail.getId());
			
			if( profile != null){
				for(int i =0; i < profile.length; i++){
					profileRepository.delete(profile[i]);
					logger.info("Sync Share Control Data Method, All Profile deleted from existing user");
				}
			}
			
			userDetailsRepository.delete(userDetailsFindByEmail);
			logger.info("Sync Share Control Data Method, All Profile deleted from existing user");			
			userDetailsFindByEmail.setId(userDetails.getId());
			userDetailsFindByEmail.setFirstName(userDetails.getFirstName());
			userDetailsFindByEmail.setLastName(userDetails.getLastName());
			userDetailsFindByEmail.setImage(userDetails.getImage());	
			userDetailsFindByEmail.setPhoneNumber(userDetails.getPhoneNumber());
			userDetailsFindByEmail.setDeviceId(userDetails.getDeviceId());
			userDetailsFindByEmail.setDeviceType(userDetails.getDeviceType());
	
			userDetailsFindByEmail.setIsEmailVerified(userDetails.getIsEmailVerified());
			userDetailsFindByEmail.setIsFirstLogin(userDetails.getIsFirstLogin());
			userDetailsFindByEmail.setUserType(userDetails.getUserType());
			userDetailsFindByEmail.setPassword(userDetails.getPassword());
			
			UserDetails newUserDetails = userDetailsRepository.save(userDetailsFindByEmail);
			
			return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, newUserDetails,API_STATUS_SUCCESS);
		}else if(findUserDetails == null){	    			
				UserDetails newuser = new UserDetails();
				//List<Home> home = (List<Home>) homeRepository.findAll();
				Home home = new Home();
				
				try
				{
					newuser.setId(userDetails.getId());
					newuser.setFirstName(userDetails.getFirstName());
					newuser.setLastName(userDetails.getLastName());
					newuser.setEmail(userDetails.getEmail());		
					newuser.setImage(userDetails.getImage());	
					newuser.setPhoneNumber(userDetails.getPhoneNumber());
					newuser.setDeviceId(userDetails.getDeviceId());
					newuser.setDeviceType(userDetails.getDeviceType());
					newuser.setBirthDate(userDetails.getBirthDate());
					
					home.setId(userDetails.getHomeid());
					home.setHomeName(userDetails.getFirstName()+" "+"Home");
					
					newuser.setHome(home);
					
					//insert home details first in local
					newuser.setIsEmailVerified(userDetails.getIsEmailVerified());
					newuser.setIsFirstLogin(userDetails.getIsFirstLogin());
					newuser.setUserType(userDetails.getUserType());
					newuser.setPassword(userDetails.getPassword());
					
					userDetailsRepository.save(newuser);
					
				}
				catch(Exception ex)
				{
					logger.error(""+ex.getMessage());
					ex.printStackTrace();
				}
				
				UserDetails details1 = userDetailsRepository.save(newuser);
				
				return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, details1,API_STATUS_SUCCESS);
			}
			else{
				Home home = new Home();
				
				UserDetails existingUser = userDetailsRepository.findOne(findUserDetails.getId());
				
				
				if(findUserDetails.getHome().getId().equals(userDetails.getHomeid())){
				
					if(userDetails.getFirstName() != null)
						existingUser.setFirstName(userDetails.getFirstName());
					if(userDetails.getImage() != null)
						existingUser.setImage(userDetails.getImage());
					if(userDetails.getLastName() != null)
						existingUser.setLastName(userDetails.getLastName());
					if(userDetails.getPhoneNumber() != null)
						existingUser.setPhoneNumber(userDetails.getPhoneNumber());
					if(userDetails.getBirthDate() != null)
						existingUser.setBirthDate(userDetails.getBirthDate());
					
					
					userDetailsRepository.save(existingUser);					
					return new ResponseDTO<UserDetails>(API_STATUS_SUCCESS, existingUser,API_STATUS_SUCCESS);
				}else{
					return new ResponseDTO<UserDetails>(API_STATUS_FAILURE, null,INVALID_HOME_ID);
				}
			}
	}

	@Override
	public Boolean userDeleteShareControl(UserShareControlDTO shareControlDTO,
			BindingResult bindingResult) {
		
		try{
		UserDetails findUserDetails = userDetailsRepository.findByEmail(shareControlDTO.getEmail());
		logger.info("User Delete Share Control method, user Details Fetched");
		
		
		if(findUserDetails != null){
			Profile arrProfile[] = profileRepository.findByUserId(findUserDetails.getId());
			logger.info("User Delete Share Control method, user Details Profile Fetched from local");
			
			for(int i =0 ; i < arrProfile.length; i++){
				profileRepository.delete(arrProfile[i].getId());
			}
			
			logger.info("User Delete Share Control method, user Details Profile Deleted from local");
			
			
			ScheduleSwitch scheduleSwitch[] = scheduleSwitchRepository.findScheduleByUserId(findUserDetails.getId());
			logger.info("User Delete Share Control method, user Details Scheduler Fetched from local");
			
			for(int i = 0; i < scheduleSwitch.length; i++){
				scheduleSwitchRepository.delete(scheduleSwitch[i].getId());
			}
			
			logger.info("User Delete Share Control method, user Details Scheduler Deleted from local");
			
			userDetailsRepository.delete(findUserDetails.getId());
			logger.info("User Delete Share Control method, user Details Deleted from local");
		}else{
			logger.info("User Delete Share Control method, user Details not found in local");
				
			return false;
		}
		
		
		}catch(Exception e){
			logger.error("Exception occured in User Delete Share Control method, Message: "+e.getMessage()+" Cause: "+e.getCause());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}
