package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.exception.InvalidParameterException;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttlConnection;

@Component
public class RoomServiceImpl implements RoomService {
	
	static Logger logger = Logger.getLogger(RoomServiceImpl.class.getName());
	
	public static String lockCode = "";
	
	@Autowired
	HomeRepository homeRepository;
	
	@Autowired
	ActivityRepository  activityRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	RoomRepository roomRepository;
	
	@Autowired
	SwitchService switchService;
	
	@Autowired
	IOTProductRepository iotProductRepository;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Value("${ROOM_NAME_ALREADY_PRESENT}")
	private String ROOM_NAME_ALREADY_PRESENT;
	
	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

	
	@Override
	public Room addUpdateRoom(AddUpdateRoomDTO addUpdateRoomDTO) {
		
		Activity activity= new Activity();
		JSONObject jMessage = new JSONObject();		
		Activity addactivity= new Activity();
		
		try
		{
			
		if(addUpdateRoomDTO.getRoomId() != null)
		{
			
			UserDetails userDetails = userDetailsRepository.findOne(addUpdateRoomDTO.getUserId());
			Room room = roomRepository.findOne(addUpdateRoomDTO.getRoomId());			
			String oldRoomName = room.getRoomName();
			
			room.setRoomType("1");	//room type is ignored field and need 1 by default
			if(addUpdateRoomDTO.getRoomName()!=null)
				room.setRoomName(addUpdateRoomDTO.getRoomName());
			if(addUpdateRoomDTO.getRoomImageId() != null)
				room.setRoomImageId(addUpdateRoomDTO.getRoomImageId());
			roomRepository.save(room);		
			
			if(userDetails.getImage() == null)
			{
				activity.setImage("");
			}
			else
			{
				activity.setImage(userDetails.getId()+ ".png");
			}
			activity.setMessage(userDetails.getFirstName().toString()+" update room name from " +oldRoomName+ " to " + room.getRoomName());
			activity.setMessageType("Room_Name_Update");
			activity.setRoomname(room.getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(addUpdateRoomDTO.getMessageFrom());
			
			addactivity = activityRepository.save(activity);
			
			jMessage.put(Constants.STATUS, "Room_Name_Update");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			jMessage.put(Constants.MESSAGE, userDetails.getFirstName().toString()+" update room name from " +oldRoomName+ " to " + room.getRoomName());
			jMessage.put(Constants.SWITCH_ID," "); 
			jMessage.put(Constants.SWITCH_NUMBER, "");
			jMessage.put(Constants.SWITCH_NAME, "");
			jMessage.put(Constants.SWITCH_STATUS, "");
			jMessage.put(Constants.DIMMER_STATUS, "");
			jMessage.put(Constants.DIMMER_VALUE, "");
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());		
			jMessage.put(Constants.ROOM_IMAGE_ID, addUpdateRoomDTO.getRoomImageId().toString());
			
			if(userDetails.getImage() == null)
			{
				jMessage.put(Constants.USER_IMAGE, "");
			}
			else
			{
				jMessage.put(Constants.USER_IMAGE,  userDetails.getId()+".png");
				
			}
			jMessage.put(Constants.ROOM_ID, room.getId().toString());
			jMessage.put(Constants.ROOM_NAME, room.getRoomName().toString());
			
			Date dateCurrent = new Date();
			jMessage.put(Constants.TIME,dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth())+" "+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			jMessage.put(Constants.MESSAGE_FROM, addUpdateRoomDTO.getMessageFrom());
			logger.info(jMessage);
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();			
			
			//child thread to call Internet Connection
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});

			return room;
			
		}
		else
		{
			UserDetails userDetails = userDetailsRepository.findOne(addUpdateRoomDTO.getUserId());
			
			Home home = userDetails.getHome();
			
			Boolean isRoomAlreadyPresent = false;
			
			for(Room room : home.getRoomList())
			{
				if(room.getRoomName().equals(addUpdateRoomDTO.getRoomName()))
					isRoomAlreadyPresent=true;
			}
			
			if(isRoomAlreadyPresent)
				throw new RuntimeException(ROOM_NAME_ALREADY_PRESENT);
			
			Room newRoom = new Room(addUpdateRoomDTO.getRoomName(), "1"); 	//2nd parameter is room type is ignored field and need 1 by default
			
			newRoom.setHome(home);
			
			home.getRoomList().add(newRoom);
			
			roomRepository.save(newRoom);
			
			return newRoom;
			
		}
		}catch(Exception ex)
		{
			logger.info("Exception in addUpdateRoom() Exception message: "+ex.getMessage());
			ex.printStackTrace();
		}
		return null;
		
	}

	@Override
	public Boolean deleteRoom(Long roomId) {
		
		Room room = roomRepository.findOne(roomId);
		
		Home home = room.getHome();
				
		home.getRoomList().remove(room);
		
		Home savedHome = homeRepository.save(home);
		
		return true;
		
	}


	@Override
	public List<RoomListByUserDTO> getRoomListByVendorId(Long userId,String messageFrom) {

		System.out.println("\n \n getRoomListByVendorId \n");
		JSONObject jMessage = new JSONObject();
		Date dateCurrent = new Date();

		try
		{
			
		UserDetails userDetails = userDetailsRepository.findOne(userId);
		
		Switch homeSwitch[] = switchRepository.getHomeLock(Constants.DIMMER_STATUS_FOR_HOME_LOCK, Constants.DIMMER_STATUS_FOR_LOCK);
		
		List<RoomListByUserDTO> roomListbyUser = new ArrayList<RoomListByUserDTO>();
		
		for(Room room : userDetails.getHome().getRoomList())
		{
			
			RoomType roomType = roomTypeRepository.findOne(Long.parseLong(room.getRoomType()));
			
			roomListbyUser.add(new RoomListByUserDTO(room.getId(), room.getRoomName(), room.getRoomType(), roomType.getRoomImage(),room.getRoomImageId().toString(),room.getHideStatus()));
			
		}
		
		for(int i = 0; i < homeSwitch.length; i++){
			
			if(homeSwitch[i].getDimmerStatus().equals(Constants.DIMMER_STATUS_FOR_HOME_LOCK)){
				roomListbyUser.add(new RoomListByUserDTO(homeSwitch[i]));
			}
			
			lockCode = homeSwitch[i].getLockCode();
			
			if(lockCode == null){
				lockCode = "";
			}
		}
		System.out.println("messageFrom  ="+messageFrom);
		if(messageFrom.equals("Internet"))
		{
			jMessage.put(Constants.STATUS, "Room_List");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			jMessage.put(Constants.MESSAGE, userDetails.getFirstName()+ " updated " +  "room list");
			
			if(userDetails.getImage()==null){
				jMessage.put(Constants.USER_IMAGE, "");
			}
			else{
				jMessage.put(Constants.USER_IMAGE, userDetails.getId()+".png");
			}
			
			jMessage.put(Constants.ROOM_LIST, roomListbyUser);
			jMessage.put(Constants.IS_ARMED, userDetails.getHome().getIsArmed());
			jMessage.put(Constants.LOCK_CODE, lockCode);
			jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth())+" "+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());
			jMessage.put(Constants.MESSAGE_FROM, "Internet");
	
			logger.info("JSON Message Created: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
		}
		
		return roomListbyUser;
		
		}
		catch(Exception ex)
		{
			logger.error("Exception in getRoomListByVendorId()  User Id: "+userId+" \nException Message: "+ex.getMessage());
			ex.printStackTrace();
			return null;
		}	 
	}

	@Override
	public Boolean changeRoomHideStatus(UpdateHideRoomDTO updateHideRoomDTO) throws Exception{

		JSONObject jMessage = new JSONObject();
		Activity activity = new Activity();
		Date date = new Date();
		
		String hideStatus="";
		String message = "";
		int updateRoomHideStatus = 0;
		try{
			UserDetails userDetails = userDetailsRepository.findOne(updateHideRoomDTO.getUserId());
			
			if(userDetails != null){
				Room roomDetails = roomRepository.findOne(updateHideRoomDTO.getRoomId());
				
				if(updateHideRoomDTO.getHideStatus().equals("1")){
					hideStatus = Constants.HIDE;
				}else{
					hideStatus = Constants.UNHIDE;
				}
				
				if(Integer.parseInt(updateHideRoomDTO.getHideStatus()) >= 0 && Integer.parseInt(updateHideRoomDTO.getHideStatus()) <= 1){
					if(Integer.parseInt(updateHideRoomDTO.getHideStatus()) == 1){
						updateRoomHideStatus = roomRepository.updateHideStatusById(updateHideRoomDTO.getRoomId(), updateHideRoomDTO.getHideStatus());
						logger.info("Room Hide API: Room data Updated");

						IOTProduct iotProduct[] = iotProductRepository.findAllByRoomId(roomDetails.getId());

						for(int i = 0 ; i < iotProduct.length; i++){
							
							List<Switch> listSwitch = switchRepository.findByIotProduct(iotProduct[i]);							
					
							//change the switch status if room status have changed to "1"
							for(int j = 0; j < listSwitch.size(); j++){
								ChangeSwitchStatusDTO changeSwitchStatusDTO = new ChangeSwitchStatusDTO();
								
								changeSwitchStatusDTO.setSwitchId(listSwitch.get(j).getId());
								changeSwitchStatusDTO.setDimmerValue("0");
								changeSwitchStatusDTO.setSwitchStatus("0");
								changeSwitchStatusDTO.setMessageFrom(updateHideRoomDTO.getMessageFrom());
								changeSwitchStatusDTO.setUserid(updateHideRoomDTO.getUserId());
								
								switchService.switchOnOff(changeSwitchStatusDTO);
							}
						}
						logger.info("Room Hide API: switch status updated to OFF");
					}else{
						//don't change the switch status just change the room hide status to "0"
						updateRoomHideStatus = roomRepository.updateHideStatusById(updateHideRoomDTO.getRoomId(), updateHideRoomDTO.getHideStatus());
						logger.info("Room Hide API: Room data Updated to HIDE");
					}
				}
				else{
					logger.error("Room Hide API: Invalid parameter to change hide status");
					throw new InvalidParameterException();
				}
								
				if(updateRoomHideStatus != 0 && roomDetails != null){
					
					if (userDetails.getImage() == null) {
						activity.setImage("");
					} else {
						activity.setImage(userDetails.getId()+ ".png");
					}
					
					activity.setMessage(userDetails.getFirstName() + " " + hideStatus + " " + roomDetails.getRoomName());
					activity.setMessageType("Room_Hide_Unhide");
					activity.setRoomname(roomDetails.getRoomName());
					activity.setCreated_date(dateFormat.format(new Date()).toString());
					activity.setMessageFrom(updateHideRoomDTO.getMessageFrom());
				
					activityRepository.save(activity);
					
					message = userDetails.getFirstName()+ " " + hideStatus + " "+ roomDetails.getRoomName();
					
					jMessage.put(Constants.STATUS, "Room_Hide_Unhide");
					jMessage.put(Constants.USER_ID, userDetails.getId().toString());
					jMessage.put(Constants.USER_NAME, userDetails.getFirstName());
					jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
					jMessage.put(Constants.ACTIVITY_ID, activity.getId().toString());
					jMessage.put(Constants.HIDE_STATUS, updateHideRoomDTO.getHideStatus());
					jMessage.put(Constants.MESSAGE_FROM, updateHideRoomDTO.getMessageFrom());
					if (userDetails.getImage() == null) {
						jMessage.put(Constants.USER_IMAGE, "");
					} else {
						jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");
					}
					jMessage.put(Constants.ROOM_ID, roomDetails.getId().toString());
					jMessage.put(Constants.ROOM_NAME, roomDetails.getRoomName());
					jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
							+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

					logger.info("Call Back Message sent"+jMessage);
					
					MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
					refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
					MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
					String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();

					//child thread to publish on Internet 
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
							} catch (MqttException e) {
								e.printStackTrace();
							}
						}
					});
					
				}else{
					logger.error("changeRoomHideStatus() Database issue Room Hide Status did not updated Successfully or RoomId Not Found, User Id: "+updateHideRoomDTO.getRoomId());
					return false;					
				}
			
			}else{
				logger.error("changeRoomHideStatus() User Details not found, Room Id: "+updateHideRoomDTO.getRoomId());
				throw new Exception();
				//return false;
			}
		}catch(NumberFormatException | InvalidParameterException ex){
			logger.error("Exception in changeRoomHideStatus()- Invalid HideStatus value, Room Id: "+updateHideRoomDTO.getRoomId()+" "+ex.getMessage());
			ex.printStackTrace();
			throw new InvalidParameterException("Invalid HideStatus value"+ex.getMessage());
		}catch(Exception ex){
			logger.error("Exception in changeRoomHideStatus, Room Id: "+updateHideRoomDTO.getRoomId()+" "+ex.getMessage());
			ex.printStackTrace();
		}
		return true;
	}
}
