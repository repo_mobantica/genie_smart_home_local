package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Profile;
import com.gsmarthome.entity.ScheduleSwitch;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.ProfileRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.ScheduleService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttlConnection;

@Component
public class ScheduleServiceImpl implements ScheduleService {
	
	/* Get actual class name to be printed on */
	static Logger logger = Logger.getLogger(ScheduleServiceImpl.class.getName());
	
	private static final SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:00");
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	
	public static final String SET_DIMMER_VALUE_WHEN_ON = "38"; //0-75 is dimmer value to hub hence 50% of it is 37avg
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	ProfileRepository profileRepository;	

	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;

	@Autowired
	ActivityRepository activityRepository;

	@Value("${SWITCH_NAME_ALREADY_PRESENT}")
	private String SWITCH_NAME_ALREADY_PRESENT;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;

	@SuppressWarnings("deprecation")
	@Override
	public ScheduleSwitchResponseDTO scheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception{

		ScheduleSwitch scheduleSwitch = new ScheduleSwitch();
		JSONObject jMessage = new JSONObject();
		Activity addactivity = new Activity();
		Activity activity = new Activity();
		
		String daymonth="";
		String hrstime = "";
		String strScheduleDate = "";
		String switchStatus = "";
		String message = "";
		
		Date date = new Date();
		Date convertedDate = null;
		
		try {
			Switch objSwitch = switchRepository.findOne(scheduleSwitchDTO.getSwitchId());
			UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}

			if(scheduleSwitchDTO.getSwitchStatus().equals("0")) {
				switchStatus = "OFF";
			}else {
				switchStatus = "ON";
			}
			
			if (scheduleSwitchDTO.getSwitchId() != null)
				scheduleSwitch.setSwitchId(scheduleSwitchDTO.getSwitchId());

			if (scheduleSwitchDTO.getLockStatus() != null)
				scheduleSwitch.setLockStatus(scheduleSwitchDTO.getLockStatus());

			if (scheduleSwitchDTO.getSwitchStatus() != null) {
				scheduleSwitch.setSwitchStatus(scheduleSwitchDTO.getSwitchStatus());
			}
			if (scheduleSwitchDTO.getUserId() != null) {
				scheduleSwitch.setUserId(scheduleSwitchDTO.getUserId());
			}
			
			if (scheduleSwitchDTO.getRepeatStatus() != null) {
				scheduleSwitch.setRepeatStatus(scheduleSwitchDTO.getRepeatStatus());
			}
			
			if(scheduleSwitchDTO.getRepeatStatus().equals("1") || scheduleSwitchDTO.getRepeatStatus() == "1"){
				if (scheduleSwitchDTO.getScheduleDateTime() != null) {
					logger.info("*********Repeat Scheduler Time: "+scheduleSwitchDTO.getScheduleDateTime().substring(11, 19)+"****************");
					scheduleSwitch.setScheduleDateTime(scheduleSwitchDTO.getScheduleDateTime().substring(11, 19));
				}
				
			}else if(scheduleSwitchDTO.getScheduleDateTime() != null) {
					scheduleSwitch.setScheduleDateTime(scheduleSwitchDTO.getScheduleDateTime());
			}
			
			if (scheduleSwitchDTO.getRepeatWeek() != null) {
				scheduleSwitch.setRepeatWeek(scheduleSwitchDTO.getRepeatWeek());
			}
			
			if (scheduleSwitchDTO.getDimmerValue() != null) {
				scheduleSwitch.setDimmerValue(scheduleSwitchDTO.getDimmerValue());
			}
			
			if (scheduleSwitchDTO.getDimmerStatus() != null) {
				scheduleSwitch.setDimmerStatus(scheduleSwitchDTO.getDimmerStatus());
			}
			
			scheduleSwitch = scheduleSwitchRepository.save(scheduleSwitch);
			logger.info("scheduleSwitch() Schedule Switch added in DB Successfully Schedule Id: " + scheduleSwitch.getId()+" for Switch ID: "+scheduleSwitch.getSwitchId());
			
			strScheduleDate = scheduleSwitchDTO.getScheduleDateTime().toString();
			
			convertedDate = (Date) formatter.parse(strScheduleDate);
			
			logger.info("Date from dd-MM-yy String in Java : " + convertedDate);

			daymonth = convertedDate.getDate() + " " + Utility.theMonth(convertedDate.getMonth());
			hrstime = Utility.getTime(convertedDate.getHours() + ":" + convertedDate.getMinutes());

			if(scheduleSwitchDTO.getRepeatStatus().equals("1") || scheduleSwitchDTO.getRepeatStatus() == "1"){
				activity.setMessage(userDetails.getFirstName().toString() + " scheduled to repeat mode to turn " + switchStatus + " "
						+ objSwitch.getSwitchName() + " on " + daymonth + " at " + hrstime.toString());				
			}else{
				activity.setMessage(userDetails.getFirstName().toString() + " scheduled to turn  " + switchStatus + " "
					+ objSwitch.getSwitchName() + " on " + daymonth + " at " + hrstime.toString());
			}
			
			activity.setMessageType("Schedule_Switch");
			activity.setRoomname(objSwitch.getIotProduct().getRoom().getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(scheduleSwitchDTO.getMessageFrom());
			
			addactivity = activityRepository.save(activity);
			logger.info("scheduleSwitch() Activity added DB Successfully : " + activity+" for User ID: "+userDetails.getFirstName());

			jMessage.put(Constants.STATUS, "Schedule_Switch");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			if(scheduleSwitchDTO.getRepeatStatus().equals("1") || scheduleSwitchDTO.getRepeatStatus() == "1"){
				message = userDetails.getFirstName().toString() + " scheduled to repeat mode to turn " + switchStatus + " "
						+ objSwitch.getSwitchName() +
						" on " + daymonth + " at " + hrstime.toString();	
			}else{
				message = userDetails.getFirstName().toString() + " scheduled to turn " + switchStatus + " "
						+ objSwitch.getSwitchName() + " on " + daymonth + " at " + hrstime.toString();
				
			}
			
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.SWITCH_ID, objSwitch.getId().toString());
			jMessage.put(Constants.REPEAT_STATUS, scheduleSwitch.getRepeatStatus());
			jMessage.put(Constants.REPEAT_WEEK, scheduleSwitch.getRepeatWeek());
			jMessage.put(Constants.SWITCH_NUMBER, objSwitch.getSwitchNumber().toString());
			jMessage.put(Constants.SWITCH_NAME, objSwitch.getSwitchName().toString());
			jMessage.put(Constants.SWITCH_STATUS, scheduleSwitchDTO.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_STATUS, objSwitch.getDimmerStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, objSwitch.getDimmerValue().toString());
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleSwitch.getId().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");
			}
			
			jMessage.put(Constants.ROOM_ID, objSwitch.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, objSwitch.getIotProduct().getRoom().getRoomName().toString());

			jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
					+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
			
			logger.info("scheduleSwitch() JSON Message: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();

			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});

			return new ScheduleSwitchResponseDTO(scheduleSwitch.getSwitchId(), scheduleSwitch.getSwitchStatus(),
					scheduleSwitch.getLockStatus(),scheduleSwitch.getRepeatStatus(), scheduleSwitch.getRepeatWeek(), scheduleSwitch.getUserId(), 
					scheduleSwitchDTO.getScheduleDateTime(), scheduleSwitch.getId(), objSwitch.getSwitchName());
		
		} catch(JDBCConnectionException ioEx){
			logger.error("JDBC Exception in scheduleSwitch(), Exception Message: "+ioEx.getMessage());
			ioEx.printStackTrace();
			throw new Exception("Cause: "+ioEx.getCause()+" Message: "+ioEx.getMessage());
		}catch (Exception e) {
			logger.error("Exception in scheduleSwitch(), Exception Message: "+e.getMessage());
			e.printStackTrace();
			throw new Exception("Cause: "+e.getCause()+" Message: "+e.getMessage());
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ScheduleProfileResponseDTO scheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception{

		ScheduleSwitch scheduleSwitch = new ScheduleSwitch();
		JSONObject jMessage = new JSONObject();
		Activity addactivity = new Activity();
		Activity activity = new Activity();
		
		String daymonth="";
		String hrstime = "";
		String strScheduleDate = "";
		String switchStatus = "";
		String message = "";
		
		Date date = new Date();
		Date convertedDate = null;
		
		try {
			Profile objProfile = profileRepository.findOne(scheduleProfileDTO.getProfileId());
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
			
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}

			if (scheduleProfileDTO.getProfileId() != null) {
				scheduleSwitch.setProfileId(scheduleProfileDTO.getProfileId());
			}
			
			if(objProfile.getSwitchOnOffStatus().equals("0")) {
				switchStatus = "OFF";
			}else {
				switchStatus = "ON";
			}
		
			if (scheduleProfileDTO.getUserId() != null) {
				scheduleSwitch.setUserId(scheduleProfileDTO.getUserId());
			}
			scheduleSwitch.setDimmerStatus("NA");
			scheduleSwitch.setDimmerValue("NA");
			scheduleSwitch.setLockStatus("NA");
			scheduleSwitch.setSwitchId(0L);
			scheduleSwitch.setSwitchStatus(objProfile.getSwitchOnOffStatus());
			
			if (scheduleProfileDTO.getRepeatStatus() != null) {
				scheduleSwitch.setRepeatStatus(scheduleProfileDTO.getRepeatStatus());
			}
			
			if(scheduleProfileDTO.getRepeatStatus().equals("1") || scheduleProfileDTO.getRepeatStatus() == "1"){
				if (scheduleProfileDTO.getScheduleDateTime() != null) {
					logger.info("*********Repeat Scheduler Time: "+scheduleProfileDTO.getScheduleDateTime().substring(11, 19)+"****************");
					scheduleSwitch.setScheduleDateTime(scheduleProfileDTO.getScheduleDateTime().substring(11, 19));
				}
				
			}else if(scheduleProfileDTO.getScheduleDateTime() != null) {
					scheduleSwitch.setScheduleDateTime(scheduleProfileDTO.getScheduleDateTime());
			}
			
			if (scheduleProfileDTO.getRepeatWeek() != null) {
				scheduleSwitch.setRepeatWeek(scheduleProfileDTO.getRepeatWeek());
			}
			
			scheduleSwitch = scheduleSwitchRepository.save(scheduleSwitch);
			logger.info("scheduleSwitch() Schedule Switch added in DB Successfully Schedule Id: " + scheduleSwitch.getId()+" for Switch ID: "+scheduleSwitch.getSwitchId());
			
			strScheduleDate = scheduleProfileDTO.getScheduleDateTime().toString();
			
			convertedDate = (Date) formatter.parse(strScheduleDate);
			
			logger.info("Date from dd-MM-yy String in Java : " + convertedDate);

			daymonth = convertedDate.getDate() + " " + Utility.theMonth(convertedDate.getMonth());
			hrstime = Utility.getTime(convertedDate.getHours() + ":" + convertedDate.getMinutes());

			if(scheduleProfileDTO.getRepeatStatus().equals("1")){
				
				activity.setMessage(userDetails.getFirstName().toString() + " scheduled for repeat mode to turn  " + switchStatus + " "
						+ objProfile.getProfileName() + " mode on " + daymonth + " at " + hrstime.toString());
			}else{
				activity.setMessage(userDetails.getFirstName().toString() + " scheduled to turn  " + switchStatus + " "
						+ objProfile.getProfileName() + " mode on " + daymonth + " at " + hrstime.toString());
			}
			
			activity.setMessageType("Schedule_Profile");
			activity.setRoomname("Home");
			activity.setCreated_date(dateFormatYear.format(new Date()).toString());
			activity.setMessageFrom(scheduleProfileDTO.getMessageFrom());
			
			addactivity = activityRepository.save(activity);
			logger.info("scheduleProfile() Activity added DB Successfully : " + activity+" for User ID: "+userDetails.getFirstName());

			jMessage.put(Constants.STATUS, "Schedule_Profile");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			if(scheduleProfileDTO.getRepeatStatus().equals("1")){
				message = userDetails.getFirstName().toString() + " scheduled for repeat mode to turn " + switchStatus + " "
						+ objProfile.getProfileName()+ " mode on " + daymonth + " at " + hrstime.toString();
				
			}else{
				message = userDetails.getFirstName().toString() + " scheduled to turn " + switchStatus + " "
						+ objProfile.getProfileName()
						+ " mode on " + daymonth + " at " + hrstime.toString();
			}
			
			jMessage.put(Constants.MESSAGE,message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.PROFILE_ID, objProfile.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, objProfile.getProfileName().toString());
			jMessage.put(Constants.REPEAT_STATUS, scheduleSwitch.getRepeatStatus());
			jMessage.put(Constants.REPEAT_WEEK, scheduleSwitch.getRepeatWeek());
			jMessage.put(Constants.SWITCH_STATUS, objProfile.getSwitchOnOffStatus().toString());
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleSwitch.getId().toString());
		
			if(scheduleSwitch.getScheduleDateTime().length() > 9){
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitch.getScheduleDateTime());
			}
			else{
				String strDate = scheduleSwitch.getScheduleDateTime();
				String strCurrDate = formatter.format(new Date());
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, strCurrDate + " "+strDate);
			}
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");
			}
			
			//jMessage.put(Constants.ROOM_ID, objSwitch.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, "Home");

			jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
					+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
			
			logger.info("scheduleSwitch() JSON Message: "+jMessage);
			
			if(scheduleProfileDTO.getMessageFrom().equalsIgnoreCase("Internet")){
				MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
				refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				// refreshBackMessage to mobile or to other thin end clients
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			
			
				String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
	
				//child thread to publish on Internet 
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				});
			}
			return new ScheduleProfileResponseDTO(scheduleSwitch.getProfileId().toString(), objProfile.getProfileName(), scheduleSwitch.getId().toString(), scheduleSwitch.getSwitchStatus(),
					scheduleSwitch.getRepeatStatus(), scheduleSwitch.getRepeatWeek(), scheduleSwitch.getUserId().toString(), 
					scheduleProfileDTO.getScheduleDateTime());
		
		} catch(JDBCConnectionException ioEx){
			logger.error("JDBC Exception in scheduleProfile(), Exception Message: "+ioEx.getMessage());
			ioEx.printStackTrace();
			throw new Exception("Cause: "+ioEx.getCause()+" Message: "+ioEx.getMessage());
		}catch (Exception e) {
			logger.error("Exception in scheduleProfile(), Exception Message: "+e.getMessage());
			e.printStackTrace();
			throw new Exception("Cause: "+e.getCause()+" Message: "+e.getMessage());
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ScheduleSwitchResponseDTO editScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception{
		
		Activity addactivity = new Activity();
		Activity activity = new Activity();
		JSONObject jMessage = new JSONObject();
		ScheduleSwitch scheduleSwitch = new ScheduleSwitch();
		String message = "";
		String switchStatus = "";
		
		Date dateCurrent = new Date();
		//Date convertedDate = null;
		Date convertedDate1 = null;
		
		try {
			
			Switch objSwitch = switchRepository.findOne(scheduleSwitchDTO.getSwitchId());
			
			scheduleSwitch = scheduleSwitchRepository.findOne(Long.parseLong(scheduleSwitchDTO.getScheduleSwitchId()));	
			
			logger.info("scheduleList(), Schedule List fetched Successfully");	
			
			UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			
			if (scheduleSwitchDTO.getSwitchStatus().equals("0")) {
				switchStatus = "Off";
			} else {
				switchStatus = "On";
			}

			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}

			/*String scheduleDate = scheduleSwitch.getScheduleDateTime().toString();
			convertedDate = (Date) formatter.parse(scheduleDate);
			
			logger.info("Date from dd-MMM-yy String in Java : " + convertedDate);
			
			String Month = Utility.theMonth(convertedDate.getMonth());
			String daymonth = convertedDate.getDate() + " " + Month;
			String time = convertedDate.getHours() + ":" + convertedDate.getMinutes();
			String hrstime = Utility.getTime(time);*/
			if (scheduleSwitch != null) {

				if (scheduleSwitchDTO.getSwitchId() != null)
					scheduleSwitch.setSwitchId(scheduleSwitchDTO.getSwitchId());

				if (scheduleSwitchDTO.getLockStatus() != null)
					scheduleSwitch.setLockStatus(scheduleSwitchDTO.getLockStatus());

				if (scheduleSwitchDTO.getSwitchStatus() != null) {
					scheduleSwitch.setSwitchStatus(scheduleSwitchDTO.getSwitchStatus());
				}
				if (scheduleSwitchDTO.getUserId() != null) {
					scheduleSwitch.setUserId(scheduleSwitchDTO.getUserId());
				}
				if (scheduleSwitchDTO.getRepeatStatus() != null) {
					scheduleSwitch.setRepeatStatus(scheduleSwitchDTO.getRepeatStatus());
				}
				
				if(scheduleSwitchDTO.getRepeatStatus().equals("1") || scheduleSwitchDTO.getRepeatStatus() == "1"){
					if (scheduleSwitchDTO.getScheduleDateTime() != null) {
						logger.info("*********Repeat Scheduler Time: "+scheduleSwitchDTO.getScheduleDateTime().substring(11, 19)+"****************");
						scheduleSwitch.setScheduleDateTime(scheduleSwitchDTO.getScheduleDateTime().substring(11, 19));
					}
					
				}else if(scheduleSwitchDTO.getScheduleDateTime() != null) {
						scheduleSwitch.setScheduleDateTime(scheduleSwitchDTO.getScheduleDateTime());
				}
				
				if (scheduleSwitchDTO.getRepeatWeek() != null) {
					scheduleSwitch.setRepeatWeek(scheduleSwitchDTO.getRepeatWeek());
				}
				
				if (scheduleSwitchDTO.getDimmerValue() != null) {
					scheduleSwitch.setDimmerValue(scheduleSwitchDTO.getDimmerValue());
				}
				if (scheduleSwitchDTO.getDimmerStatus() != null) {
					scheduleSwitch.setDimmerStatus(scheduleSwitchDTO.getDimmerStatus());
				}
				
			scheduleSwitch = scheduleSwitchRepository.save(scheduleSwitch);
			
			String scheduleDate1 = scheduleSwitchDTO.getScheduleDateTime();
			convertedDate1 = (Date) formatter.parse(scheduleDate1);
			logger.info("Date from dd-MMM-yy String in Java : " + convertedDate1);
			
			String Month1 = Utility.theMonth(convertedDate1.getMonth());
			String daymonth1 = convertedDate1.getDate() + " " + Month1;
			String time1 = convertedDate1.getHours() + ":" + convertedDate1.getMinutes();
			String hrstime1 = Utility.getTime(time1);
			
			if(scheduleSwitchDTO.getRepeatStatus().equals("1")){
				activity.setMessage(userDetails.getFirstName().toString() + " updated scheduler to repeat mode" + " "+
					 " at " + daymonth1 + " " + hrstime1 + " to turn " + switchStatus + " "
					+ objSwitch.getSwitchName());
			}else{
				activity.setMessage(userDetails.getFirstName().toString() + " updated scheduler" + " to " + daymonth1 + " " + hrstime1 + " to turn " + switchStatus + " "
						+ objSwitch.getSwitchName());
			}
			
			activity.setMessageType("Schedule_Switch_update");
			activity.setRoomname(objSwitch.getIotProduct().getRoom().getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(scheduleSwitchDTO.getMessageFrom());
			
			addactivity = activityRepository.save(activity);
			
			logger.info("editScheduleSwitch() Activity added DB Successfully : " + activity+" for User ID: "+userDetails.getFirstName());

			jMessage.put(Constants.STATUS, "Schedule_Switch_Update");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			if(scheduleSwitchDTO.getRepeatStatus().equals("1")){
					
				message = userDetails.getFirstName().toString() + " updated scheduler from repeat mode to turn " + switchStatus + " "
					+ objSwitch.getSwitchName() +" on "+ daymonth1 + " at " + hrstime1;
			}else{
				message = userDetails.getFirstName().toString() + " updated scheduler to turn " + switchStatus + " "
						+ objSwitch.getSwitchName() +" on " + daymonth1 + " at " + hrstime1;
			}
			
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.SWITCH_ID, objSwitch.getId().toString());
			jMessage.put(Constants.REPEAT_STATUS, scheduleSwitch.getRepeatStatus());
			jMessage.put(Constants.REPEAT_WEEK, scheduleSwitch.getRepeatWeek());
			jMessage.put(Constants.SWITCH_NUMBER, objSwitch.getSwitchNumber().toString());
			jMessage.put(Constants.SWITCH_NAME, objSwitch.getSwitchName().toString());
			jMessage.put(Constants.SWITCH_STATUS, scheduleSwitchDTO.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_STATUS, objSwitch.getDimmerStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, objSwitch.getDimmerValue().toString());
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleSwitchDTO.getScheduleSwitchId().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());

			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
			}
			jMessage.put(Constants.ROOM_ID, objSwitch.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, objSwitch.getIotProduct().getRoom().getRoomName().toString());
			jMessage.put("Time", dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info("scheduleSwitch() JSON Message: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
				
			}
			return new ScheduleSwitchResponseDTO(scheduleSwitch.getSwitchId(), scheduleSwitch.getSwitchStatus(),
					scheduleSwitch.getLockStatus(),scheduleSwitch.getRepeatStatus(), scheduleSwitch.getRepeatWeek(), 
					scheduleSwitch.getUserId(), scheduleSwitchDTO.getScheduleDateTime(),
					scheduleSwitch.getId(), objSwitch.getSwitchName());
		} catch(JDBCConnectionException jdbcEx){
			logger.error("JDBC Exception in editScheduleSwitch(), JDBC Exception Message: "+jdbcEx.getMessage());
			jdbcEx.printStackTrace();
			throw new Exception(jdbcEx.getMessage());
		}catch (Exception e) {
			logger.error("General Exception in editScheduleSwitch(), Exception Message: "+e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ScheduleProfileResponseDTO editScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception{
		
		Activity addActivity = new Activity();
		Activity activity = new Activity();
		JSONObject jMessage = new JSONObject();
		ScheduleSwitch scheduleProfile = new ScheduleSwitch();
		
		String message = "";
		String switchStatus = "";
		
		Date dateCurrent = new Date();
		//Date convertedDate = null;
		Date convertedDate1 = null;
		
		try {
			
			Profile objProfile = profileRepository.findOne(scheduleProfileDTO.getProfileId());
			
			if(objProfile != null){
				scheduleProfile = scheduleSwitchRepository.findOne(scheduleProfileDTO.getScheduleId());
				logger.info("editScheduleProfile(), Profile Schedule Fetched Successfully");	
			}else{
				logger.error("Profile not found, Profile Id:"+scheduleProfileDTO.getProfileId());
				throw new Exception("profile not found, Profile Id:"+scheduleProfileDTO.getProfileId());
			}
			
			
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
				
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}

			if (scheduleProfile != null) {

				if (scheduleProfileDTO.getProfileId() != null)
					scheduleProfile.setProfileId(scheduleProfileDTO.getProfileId());

				//in case of profile schedule no need to set switch 
				scheduleProfile.setSwitchId(0L);				
				scheduleProfile.setLockStatus("NA");
				scheduleProfile.setSwitchStatus("NA");
				scheduleProfile.setDimmerValue("NA");
				scheduleProfile.setDimmerStatus("NA");
				
				
				if (scheduleProfileDTO.getUserId() != null) {
					scheduleProfile.setUserId(scheduleProfileDTO.getUserId());
				}
				
				if (scheduleProfileDTO.getRepeatStatus() != null) {
					scheduleProfile.setRepeatStatus(scheduleProfileDTO.getRepeatStatus());
				}
				
				if(scheduleProfileDTO.getRepeatStatus().equals("1") || scheduleProfileDTO.getRepeatStatus() == "1"){
					if (scheduleProfileDTO.getScheduleDateTime() != null) {
						logger.info("*********Repeat Scheduler Time: "+scheduleProfileDTO.getScheduleDateTime().substring(11, 19)+"****************");
						scheduleProfile.setScheduleDateTime(scheduleProfileDTO.getScheduleDateTime().substring(11, 19));
					}
					
				}else if(scheduleProfileDTO.getScheduleDateTime() != null) {
						scheduleProfile.setScheduleDateTime(scheduleProfileDTO.getScheduleDateTime());
				}
				
				if (scheduleProfileDTO.getRepeatWeek() != null) {
					scheduleProfile.setRepeatWeek(scheduleProfileDTO.getRepeatWeek());
				}
				
				scheduleProfile = scheduleSwitchRepository.save(scheduleProfile);
				
				if (objProfile.getSwitchOnOffStatus().equals("0")) {
					switchStatus = "Off";
				} else {
					switchStatus = "On";
				}
			
			logger.info("editScheduleProfile() Schedule Profile added to DB Successfully : " + activity+" for User ID: "+userDetails.getFirstName());
			
			String scheduleDate1 = scheduleProfileDTO.getScheduleDateTime();
			convertedDate1 = (Date) formatter.parse(scheduleDate1);
			logger.info("Date from dd-MMM-yy String in Java : " + convertedDate1);
			
			String Month1 = Utility.theMonth(convertedDate1.getMonth());
			String daymonth1 = convertedDate1.getDate() + " " + Month1;
			String time1 = convertedDate1.getHours() + ":" + convertedDate1.getMinutes();
			String hrstime1 = Utility.getTime(time1);
			
			if(scheduleProfileDTO.getRepeatStatus().equals("1")){
				activity.setMessage(userDetails.getFirstName().toString() + " updated scheduler to repeat mode to turn "+ switchStatus +" "+  objProfile.getProfileName() 
				+" mode on "+ daymonth1 + " at " + hrstime1);
			}else{
				activity.setMessage(userDetails.getFirstName().toString() + " updated scheduler to turn " + switchStatus +" "+ objProfile.getProfileName()
						 +" mode on " + daymonth1 + " at " + hrstime1);
			}
			
			activity.setMessageType("Schedule_Profile_Update");
			activity.setRoomname("Home");
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(scheduleProfileDTO.getMessageFrom());
			
			addActivity = activityRepository.save(activity);
			logger.info("editScheduleSwitch() Activity added to DB Successfully : " + activity+" for User ID: "+userDetails.getFirstName());

			jMessage.put(Constants.STATUS, "Schedule_Profile_Update");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			if(scheduleProfileDTO.getRepeatStatus().equals("1") || scheduleProfileDTO.getRepeatStatus() == "1"){
				message = userDetails.getFirstName().toString() + " updated scheduler to " + "repeat mode" + " "
						+ " to " + daymonth1 + " " + hrstime1 + " to activate " + objProfile.getProfileName();
			}else{
				message = userDetails.getFirstName().toString() + " updated scheduler from " + daymonth1 + " " + hrstime1 + " to activate " + objProfile.getProfileName();
			}
			
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.PROFILE_ID, objProfile.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, objProfile.getProfileName().toString());
			jMessage.put(Constants.SWITCH_STATUS, objProfile.getSwitchOnOffStatus().toString());
			jMessage.put(Constants.ROOM_NAME, "Home");
			jMessage.put(Constants.REPEAT_STATUS, scheduleProfile.getRepeatStatus());
			jMessage.put(Constants.REPEAT_WEEK, scheduleProfile.getRepeatWeek());
			jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleProfile.getId().toString());
			
			if(scheduleProfile.getScheduleDateTime().length() > 9){
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleProfile.getScheduleDateTime());
			}
			else{
				String strDate = scheduleProfile.getScheduleDateTime();
				String strCurrDate = formatter.format(new Date());
				jMessage.put(Constants.SCHEDULAR_DATE_TIME, strCurrDate + " "+strDate);
			}
			
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
			}
			
			jMessage.put("Time", dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info("scheduleSwitch() JSON Message: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
				
			}
			return new ScheduleProfileResponseDTO(scheduleProfile.getProfileId().toString(), 
					objProfile.getProfileName(), 
					scheduleProfile.getId().toString(), 
					scheduleProfile.getSwitchStatus(),
					scheduleProfile.getRepeatStatus(), 
					scheduleProfile.getRepeatWeek(), 
					scheduleProfile.getUserId().toString(), 
					scheduleProfileDTO.getScheduleDateTime());
			
		} catch(JDBCConnectionException jdbcEx){
			logger.error("JDBC Exception in editScheduleSwitch(), JDBC Exception Message: "+jdbcEx.getMessage());
			jdbcEx.printStackTrace();
			throw new Exception(jdbcEx.getMessage());
		}catch (Exception e) {
			logger.error("General Exception in editScheduleSwitch(), Exception Message: "+e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	

	@SuppressWarnings("deprecation")
	@Override
	public ScheduleSwitchResponseDTO deleteScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) {
		try {
			Switch switch1 = switchRepository.findOne(scheduleSwitchDTO.getSwitchId());
			UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
			
			String switchStatus;
			Activity activity = new Activity();
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}

			if (scheduleSwitchDTO.getSwitchStatus().equals("0")) {
				switchStatus = "Off";
			} else {
				switchStatus = "On";
			}


			scheduleSwitchRepository.delete(Long.parseLong(scheduleSwitchDTO.getScheduleSwitchId()));
			logger.info("deleteScheduleSwitch(), Scheduler Deleted Successfully");
			
			Date convertedDate = null;
			String scheduleDate = scheduleSwitchDTO.getScheduleDateTime().toString();
			// formatter = new SimpleDateFormat("dd-MMM-yy");
			convertedDate = (Date) formatter.parse(scheduleDate);
			String Month = Utility.theMonth(convertedDate.getMonth());
			String daymonth = convertedDate.getDate() + " " + Month;
			String time = convertedDate.getHours() + ":" + convertedDate.getMinutes();
			String hrstime = Utility.getTime(time);
			activity.setMessage(userDetails.getFirstName().toString() + " deleted scheduler to turn  " + switchStatus
					+ " " + switch1.getSwitchName() + " on " + daymonth.toString() + " at " + hrstime.toString());
			activity.setMessageType("Schedule_Switch_delete");
			activity.setRoomname(switch1.getIotProduct().getRoom().getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(scheduleSwitchDTO.getMessageFrom());
			//activity.setHome(userDetails.getHome());
			Activity addactivity = new Activity();
			addactivity = activityRepository.save(activity);

			JSONObject jMessage = new JSONObject();
			jMessage.put(Constants.STATUS, "Schedule_Switch_delete");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			String message = userDetails.getFirstName().toString() + " deleted scheduler to turn  " + switchStatus + " "
					+ switch1.getSwitchName() + " from " + switch1.getIotProduct().getRoom().getRoomName()
					+ " on " + daymonth.toString() + " at " + hrstime.toString();
			jMessage.put(Constants.MESSAGE,
					message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.SWITCH_ID, switch1.getId().toString());
			jMessage.put(Constants.SWITCH_NUMBER, switch1.getSwitchNumber().toString());
			jMessage.put(Constants.SWITCH_NAME, switch1.getSwitchName().toString());
			jMessage.put(Constants.SWITCH_STATUS, switch1.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_STATUS, switch1.getDimmerStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, switch1.getDimmerValue().toString());
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleSwitchDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleSwitchDTO.getScheduleSwitchId().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, scheduleSwitchDTO.getScheduleDateTime().toString());
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");

			}
			jMessage.put(Constants.ROOM_ID, switch1.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, switch1.getIotProduct().getRoom().getRoomName().toString());

			Date dateCurrent = new Date();
			jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info(jMessage);
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						
						e.printStackTrace();
					}
				}
			});
			
			return new ScheduleSwitchResponseDTO(switch1.getId(), scheduleSwitchDTO.getSwitchStatus(),
					switch1.getLockStatus(), scheduleSwitchDTO.getUserId(), scheduleSwitchDTO.getScheduleDateTime(),
					Long.parseLong(scheduleSwitchDTO.getScheduleSwitchId()), switch1.getSwitchName().toString());
		} catch (Exception ex) {
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ScheduleProfileResponseDTO deleteScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception {
		
		//String switchStatus;

		Date dateCurrent = new Date();
		Date convertedDate = null;
		Activity addactivity = new Activity();

		JSONObject jMessage = new JSONObject();
		try {
			
			Profile objProfile = profileRepository.findOne(scheduleProfileDTO.getProfileId());
			
			
			UserDetails userDetails = userDetailsRepository.findOne(scheduleProfileDTO.getUserId());
			
			Activity activity = new Activity();
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
				//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
			}

			/*if (scheduleProfileDTO.getSwitchStatus().equals("0")) {
				switchStatus = "Off";
			} else {
				switchStatus = "On";
			}*/


			scheduleSwitchRepository.delete(scheduleProfileDTO.getScheduleId());
			logger.info("deleteScheduleSwitch(), Scheduler Deleted Successfully");
			
			String scheduleDate = scheduleProfileDTO.getScheduleDateTime().toString();
			// formatter = new SimpleDateFormat("dd-MMM-yy");
			convertedDate = (Date) formatter.parse(scheduleDate);
			String Month = Utility.theMonth(convertedDate.getMonth());
			String daymonth = convertedDate.getDate() + " " + Month;
			String time = convertedDate.getHours() + ":" + convertedDate.getMinutes();
			String hrstime = Utility.getTime(time);
			
			activity.setMessage(userDetails.getFirstName().toString() + " deleted scheduler for profile  " + " " + objProfile.getProfileName());

			activity.setMessageType("Schedule_Switch_Delete");
			activity.setRoomname("Home");
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(scheduleProfileDTO.getMessageFrom());
			//activity.setHome(userDetails.getHome());
			addactivity = activityRepository.save(activity);

			jMessage.put(Constants.STATUS, "Schedule_Profile_Delete");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			String message = userDetails.getFirstName().toString() + " deleted scheduler for profile  "+objProfile.getProfileName();
			jMessage.put(Constants.MESSAGE,message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.PROFILE_ID, objProfile.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, objProfile.getProfileName().toString());
			jMessage.put(Constants.ROOM_NAME, "Home");
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.MESSAGE_FROM, scheduleProfileDTO.getMessageFrom());
			jMessage.put(Constants.SCHEDULAR_ID, scheduleProfileDTO.getScheduleId().toString());
			jMessage.put(Constants.SCHEDULAR_DATE_TIME, dateFormat.format(new Date()).toString());
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
			}

			jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info(jMessage);
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						
						e.printStackTrace();
					}
				}
			});
			
			return new ScheduleProfileResponseDTO(
					scheduleProfileDTO.getScheduleId().toString(), 
					userDetails.getId().toString(),
					scheduleProfileDTO.getScheduleDateTime());
			
		} catch (Exception ex) {
			logger.error("deleteScheduleProfile(), Exception Message: "+ex.getMessage());
			ex.printStackTrace();
			throw new Exception(ex);
		}
	}
	
	@Override
	public List<ScheduleSwitch> getScheduleSwitchList(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception{
	
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitchDTO.getUserId());
		
		List<ScheduleSwitch> listScheduleSwitch = new ArrayList<ScheduleSwitch>();
		
		//List<Switch> listSwitch = new ArrayList<Switch>();
		
		if(userDetails != null){
			try{
				
				ScheduleSwitch scheduleSwitch[] = scheduleSwitchRepository.findScheduleByUserId(scheduleSwitchDTO.getUserId());
				
				if(scheduleSwitch.length > 0){
					for(int index = 0; index < scheduleSwitch.length; ++index)
					{
						String strDate = scheduleSwitch[index].getScheduleDateTime();
						String strCurrDate = formatter.format(new Date());
					
						if(strDate.length() > 9)
						{
							Date date = formatter.parse(strDate);
							
							Date currDate = formatter.parse(strCurrDate);
							
							if( date.after(currDate) && date.getTime() > currDate.getTime()){
								listScheduleSwitch.add(scheduleSwitch[index]);
							}
						}
						else{
							//get Schedule 
							scheduleSwitch[index].setScheduleDateTime(strCurrDate.substring(0,10)+" "+strDate);
							
							listScheduleSwitch.add(scheduleSwitch[index]);
						}
					
					}
				}
				logger.info("getScheduleSwitchList() list of scheduler for userId: "+scheduleSwitchDTO.getUserId()); 
			}catch(Exception ex){
				logger.error("Exception in getScheduleSwitchList() Message: "+ex.getMessage());
				ex.printStackTrace();
				throw new Exception(ex);
			}
	
			try {
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
				
			jMessage.put(Constants.STATUS, "Schedule_Switch_List");		
			jMessage.put(Constants.SCHEDULE_LIST, listScheduleSwitch);
			jMessage.put(Constants.USER_ID, scheduleSwitchDTO.getUserId().toString());
	
			logger.info(jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						logger.error("Exception in getScheduleSwitchList() MQTT Internet Exception Message: "+e.getMessage());
						e.printStackTrace();
						
					}
				}
			});
	
			} catch (JSONException | MqttException e) {
				logger.error("Exception in getScheduleSwitchList() MQTT or JSON Exception, Message: "+e.getMessage());
				e.printStackTrace();
			}catch(Exception e){
				logger.error("Exception in getScheduleSwitchList() General Exception Message: "+e.getMessage());
				e.printStackTrace();
			}
		}else{
			logger.info("getScheduleSwitchList method, User Details not found:");
		}
		
		return listScheduleSwitch;
	}

}