package com.gsmarthome.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.GenieCamRequestDTO;
import com.gsmarthome.dto.GenieCamResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.GenieCam;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.GenieCamRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.GenieCamService;

import MqttReadDataPaho.MqttlConnection;

@Component
public class GenieCamServiceImpl implements GenieCamService {

	static Logger logger = Logger.getLogger(GenieCamServiceImpl.class.getName());

	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	GenieCamRepository genieCamRepository;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Value("${USER_DETAILS_NOT_PRESENT}")
	private String USER_DETAILS_NOT_PRESENT;

	@Value("${GENIE_CAMERA_UPDATE_SUCCESS}")
	private String GENIE_CAMERA_UPDATE_SUCCESS;

	@Value("${GENIE_CAMERA_FOUND_SUCCESS}")
	private String GENIE_CAMERA_FOUND_SUCCESS;

	@Value("${GENIE_CAMERA_NOT_FOUND}")
	private String GENIE_CAMERA_NOT_FOUND;
	
	@Value("${GENIE_CAMERA_UPDATE_FAILED}")
	private String GENIE_CAMERA_UPDATE_FAILED;
	
	@Value("${GENIE_CAMERA_DELETE_SUCCESS}")
	private String GENIE_CAMERA_DELETE_SUCCESS;
	
	@Value("${GENIE_CAMERA_DELETE_FAILED}")
	private String GENIE_CAMERA_DELETE_FAILED;
	
	@Override
	public ResponseDTO<List<GenieCam>> getAllGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		JSONObject jMessage = new JSONObject();
		
		UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
		
		if(userDetails != null){
			List<GenieCam> listGenieCam = new ArrayList<GenieCam>();
			
			logger.info("Get All Genie Cam Details, genie Camera DB requested . User Id: "+genieCamRequestDTO.getUserId());
			Iterable<GenieCam> ItrableGenieCam = genieCamRepository.findAll();
			
			Iterator<GenieCam> itrGenieCam = ItrableGenieCam.iterator();
			
			while(itrGenieCam.hasNext()){
				listGenieCam.add(itrGenieCam.next());
			}
			
			if(listGenieCam.size() > 0){
				
				//MQTT Callback message
				try {
					jMessage.put(Constants.GENIE_CAM_LIST, listGenieCam);
					jMessage.put(Constants.MESSAGE_FROM, genieCamRequestDTO.getMessageFrom().toString());
					jMessage.put(Constants.USER_ID, genieCamRequestDTO.getUserId().toString());
				} catch (JSONException e1) {
					logger.error("Add Genie Cam Details, Genie Camera JSON Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					e1.printStackTrace();
				}
				
				logger.debug("get all Genie Cam Details, JSON Message :"+jMessage);
				
				MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
			    
				refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				
			    try {
					MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
				} catch (MqttPersistenceException e1) {
					logger.error("get all  Genie Cam Details, Genie Camera MQTTPersistentException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					e1.printStackTrace();
				} catch (MqttException e1) {
					logger.error("get all Genie Cam Details, Genie Camera MqttException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					e1.printStackTrace();
				}
			    
				String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
				
				//child thread to publish on Internet
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
						} catch (MqttException e) {
							
							e.printStackTrace();
						}
					}
				});
				
				logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
				logger.info("Get All Genie Cam Details, genie Camera list fetched Successfully. User Id: "+genieCamRequestDTO.getUserId());
				return new ResponseDTO<List<GenieCam>>(API_STATUS_SUCCESS, listGenieCam, GENIE_CAMERA_FOUND_SUCCESS);
			}else{
				logger.error("Get All Genie Cam Details, genie Cam List fetched failed. User Id: "+genieCamRequestDTO.getUserId());
				return new ResponseDTO<List<GenieCam>>(API_STATUS_FAILURE, listGenieCam, GENIE_CAMERA_NOT_FOUND);				
			}
		}else{
			logger.error("Get All Genie Cam Details, UserDetails not found. User Id: "+genieCamRequestDTO.getUserId());
			return new ResponseDTO<List<GenieCam>>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);
		}
	}

	@Override
	public ResponseDTO<GenieCamResponseDTO> addGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		GenieCamResponseDTO genieCamResponseDTO = new GenieCamResponseDTO();
		JSONObject jMessage = new JSONObject();
		
		if(genieCamRequestDTO != null){
			logger.info("Add Genie Cam Details, Genie Camera request object. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
			
			UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
			
			try{
				//genie id null then new Genie Camera
				if(genieCamRequestDTO.getGenieCamId() == null){
					GenieCam genieCam = new GenieCam();
					genieCam.setCamName(genieCamRequestDTO.getCamName());
					genieCam.setInternetIP(genieCamRequestDTO.getInternetIP());
					genieCam.setInternetPort(genieCamRequestDTO.getInternetPort());
					genieCam.setLocalIP(genieCamRequestDTO.getLocalIP());
					genieCam.setLocalPort(genieCamRequestDTO.getLocalPort());

					logger.info("Add Genie Cam Details, Genie Camera saved Successfully. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					genieCamRepository.save(genieCam);
					
					//fill response body
					genieCamResponseDTO.setGenieCamId(genieCam.getId().toString());
					genieCamResponseDTO.setCamName(genieCam.getCamName());
					genieCamResponseDTO.setGenieCamId(genieCam.getId().toString());
					genieCamResponseDTO.setInternetIP(genieCam.getInternetIP());
					genieCamResponseDTO.setInternetPort(genieCam.getInternetPort());
					genieCamResponseDTO.setLocalIP(genieCam.getLocalIP());
					genieCamResponseDTO.setLocalPort(genieCam.getLocalPort());
				}else{
					GenieCam genieCamObj = genieCamRepository.findOne(genieCamRequestDTO.getGenieCamId());
					logger.info("Add Genie Cam Details, Genie Camera fetched for edit. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					
					genieCamObj.setCamName(genieCamRequestDTO.getCamName());
					genieCamObj.setInternetIP(genieCamRequestDTO.getInternetIP());
					genieCamObj.setLocalIP(genieCamRequestDTO.getLocalIP());
					genieCamObj.setLocalPort(genieCamRequestDTO.getLocalPort());
					genieCamObj.setInternetPort(genieCamRequestDTO.getInternetPort());

					logger.info("Add Genie Cam Details, Genie Camera updated Successfully. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
					genieCamRepository.save(genieCamObj);
					
					//fill response body
					genieCamResponseDTO.setCamName(genieCamObj.getCamName());
					genieCamResponseDTO.setGenieCamId(genieCamObj.getId().toString());
					genieCamResponseDTO.setInternetIP(genieCamObj.getInternetIP());
					genieCamResponseDTO.setInternetPort(genieCamObj.getInternetPort());
					genieCamResponseDTO.setLocalIP(genieCamObj.getLocalIP());
					genieCamResponseDTO.setLocalPort(genieCamObj.getLocalPort());
				}
				
			}catch(Exception e){
				logger.error("Add Genie Cam Details, Genie Camera request object Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e.printStackTrace();
				return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, genieCamResponseDTO, GENIE_CAMERA_UPDATE_FAILED);
			}
			
			//TODO MQTT Callback message
			try {
				jMessage.put(Constants.GENIE_CAM_NAME, genieCamResponseDTO.getCamName());
				jMessage.put(Constants.GENIE_LOCAL_IP, genieCamResponseDTO.getLocalIP().toString());
				jMessage.put(Constants.GENIE_LOCAL_PORT, genieCamResponseDTO.getLocalPort().toString());
				jMessage.put(Constants.GENIE_INTERNET_IP, genieCamResponseDTO.getInternetIP().toString());
				jMessage.put(Constants.GENIE_INTERNET_PORT, genieCamResponseDTO.getInternetPort().toString());
				jMessage.put(Constants.MESSAGE_FROM, genieCamRequestDTO.getMessageFrom().toString());
				jMessage.put(Constants.USER_ID, genieCamRequestDTO.getUserId().toString());
			} catch (JSONException e1) {
				logger.error("Add Genie Cam Details, Genie Camera JSON Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			}
			
			logger.debug("Add Genie Cam Details, JSON Message :"+jMessage);
			
			MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
		    
			refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			
		    try {
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
			} catch (MqttPersistenceException e1) {
				logger.error("Add Genie Cam Details, Genie Camera MQTTPersistentException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			} catch (MqttException e1) {
				logger.error("Add Genie Cam Details, Genie Camera MqttException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e1.printStackTrace();
			}
		    
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
					} catch (MqttException e) {
						
						e.printStackTrace();
					}
				}
			});
			
			logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
			
			return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_SUCCESS, genieCamResponseDTO, GENIE_CAMERA_UPDATE_SUCCESS);
		}else{
			logger.error("Add Genie Cam Details, Genie Camera request object Error.");
			return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, genieCamResponseDTO, GENIE_CAMERA_UPDATE_FAILED);
		}
	}

	@Override
	public ResponseDTO<GenieCamResponseDTO> deleteGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO) {
		JSONObject jMessage = new JSONObject();
		
		GenieCamResponseDTO genieCamResponseDTO = new GenieCamResponseDTO();
		
		if(genieCamRequestDTO != null){
			
			UserDetails userDetails = userDetailsRepository.findOne(genieCamRequestDTO.getUserId());
		
			try{
				
				GenieCam genieObj = genieCamRepository.findOne(genieCamRequestDTO.getGenieCamId());
				logger.info("Delete Genie Cam Details, Genie Camera delete request fetched genieObj. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				
				if(genieObj != null){
					genieCamRepository.delete(genieObj);
					genieCamResponseDTO.setUserId(userDetails.getId().toString());
					
					logger.debug("Add Genie Cam Details, JSON Message :"+jMessage);
					
					MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
				    
					refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
					
				    try {
						MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
					} catch (MqttPersistenceException e1) {
						logger.error("Add Genie Cam Details, Genie Camera MQTTPersistentException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
						e1.printStackTrace();
					} catch (MqttException e1) {
						logger.error("Add Genie Cam Details, Genie Camera MqttException Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
						e1.printStackTrace();
					}
				    
					String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
					
					//child thread to publish on Internet
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
							} catch (MqttException e) {
								
								e.printStackTrace();
							}
						}
					});
					
					logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
				}else{
					return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, genieCamResponseDTO, GENIE_CAMERA_NOT_FOUND);
				}
				
			}catch(Exception e){
				logger.error("Delete Genie Cam Details, Genie Camera request object Error. Genie Camera Id: "+genieCamRequestDTO.getGenieCamId());
				e.printStackTrace();
				return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, genieCamResponseDTO, GENIE_CAMERA_DELETE_FAILED);
			}
			return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_SUCCESS, genieCamResponseDTO, GENIE_CAMERA_DELETE_SUCCESS);
		}else{
			logger.error("Delete Genie Cam Details, Genie Camera Delete request Error.");
			return new ResponseDTO<GenieCamResponseDTO>(API_STATUS_FAILURE, null, GENIE_CAMERA_DELETE_FAILED);
		}
	}
}
