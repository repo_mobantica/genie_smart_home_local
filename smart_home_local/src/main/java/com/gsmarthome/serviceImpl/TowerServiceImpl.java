package com.gsmarthome.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.TowerRequestDTO;
import com.gsmarthome.entity.Tower;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.TowerRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.TowerService;

@Component
public class TowerServiceImpl implements TowerService {

	static Logger logger = Logger.getLogger(TowerServiceImpl.class.getName());

	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	TowerRepository towerRepository;
	
	@Value("${USER_DETAILS_NOT_PRESENT}")
	private String USER_DETAILS_NOT_PRESENT;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@Value("${TOWER_LIST_FETCHED_SUCCESSFULLY}")
	private String TOWER_LIST_FETCHED_SUCCESSFULLY;
	
	@Override
	public ResponseDTO<List<Tower>> getAllTower(TowerRequestDTO towerReqDTO) {
		
		List<Tower> listTower = new ArrayList<Tower>();
		
		UserDetails userDetails = userDetailsRepository.findOne(towerReqDTO.getUserId());
		
		if(userDetails != null){
			
			Iterable<Tower> iterableTower = towerRepository.findAll();
			
			Iterator<Tower> itrTower = iterableTower.iterator();
			
			while(itrTower.hasNext()){
				listTower.add(itrTower.next());
			}
			
			return new ResponseDTO<List<Tower>>(API_STATUS_SUCCESS, listTower, TOWER_LIST_FETCHED_SUCCESSFULLY);
			
		}else{
			return new ResponseDTO<List<Tower>>(API_STATUS_FAILURE, null, USER_DETAILS_NOT_PRESENT);
		}
	}
	
}
