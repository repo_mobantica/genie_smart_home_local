package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ProfileRequestDTO;
import com.gsmarthome.dto.ProfileResponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Profile;
import com.gsmarthome.entity.ScheduleSwitch;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.ProfileRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.ProfileService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttlConnection;

@Component
public class ProfileServiceImpl implements ProfileService {
	
	/* Get actual class name to be printed on */
	static Logger logger = Logger.getLogger(ProfileServiceImpl.class.getName());
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	//private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:00");

	public static final String TURN = " turn "; //purposely added the space before and after turn
	
	public static final String SET_DIMMER_VALUE_WHEN_ON = "38"; //0-75 is dimmer value to hub hence 50% of it is 37avg
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	ProfileRepository profileRepository;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;
	
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	SwitchService switchService;

	@Value("${SWITCH_NAME_ALREADY_PRESENT}")
	private String SWITCH_NAME_ALREADY_PRESENT;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	
	@Override
	public List<ChangeStatusByRoomResponseDTO> profileOnOff(ChangeStatusByHomeDTO changeStatusByHomeDTO) throws Exception{
		List<String> switchlist = null;
		List<String> dimmerList = null;
		Activity activity = new Activity();
		Activity addActivity = new Activity();
		
		JSONObject jMessage = new JSONObject();

		Date dateCurrent = new Date();
		Switch switch1 = null;
		String switchStatus = "";
		String message = "";
		
		// turn off/on home
		try {
			UserDetails userDetails = userDetailsRepository.findOne(changeStatusByHomeDTO.getUserId());
			Profile profileRepo = profileRepository.findOne(changeStatusByHomeDTO.getProfileId());
			
			List<ChangeStatusByRoomResponseDTO> switchList = new ArrayList<ChangeStatusByRoomResponseDTO>();
			
			if(profileRepo != null)
			{	
				switchlist = Arrays.asList(profileRepo.getSwitchList().split("\\:"));
				dimmerList = Arrays.asList(profileRepo.getDimmerValues().split("\\:"));
			
			
				if(!switchlist.isEmpty())
				{
					
					if (profileRepo.getSwitchOnOffStatus().equals("0")) {
						switchStatus = Constants.SWITCH_STATUS_OFF;
		
						for (int i = 0; i < switchlist.size(); i++) {
		
							switch1 = switchRepository.findOne(Long.parseLong(switchlist.get(i)));
							if(switch1.getIotProduct().getRoom().getHideStatus().equals("0")){
								if(switch1.getHideStatus() == "0" || switch1.getHideStatus().equals("0")){
									ChangeSwitchStatusDTO getSwitch = new ChangeSwitchStatusDTO();
									getSwitch.setSwitchId(Long.parseLong(switchlist.get(i)));
									getSwitch.setSwitchStatus(profileRepo.getSwitchOnOffStatus());
									getSwitch.setUserid(changeStatusByHomeDTO.getUserId());
									if(dimmerList.get(i).equals("NA")){
										getSwitch.setDimmerValue(switch1.getDimmerValue());
									}else{
										getSwitch.setDimmerValue(dimmerList.get(i));
									}
									switchService.changeSwitchStatusByRoom(getSwitch);
									switchList.add(new ChangeStatusByRoomResponseDTO(switch1.getId().toString(), profileRepo.getSwitchOnOffStatus(),getSwitch.getDimmerValue(),
											switch1.getIotProduct().getRoom().getId().toString()));
								}
							}else{
								logger.info("profileOnOff method,Room is Hidden to Switch Status: "+switchStatus);
							}
						}
					}else {
						switchStatus = Constants.SWITCH_STATUS_ON;
						for (int i = 0; i < switchlist.size(); i++) {
		
							switch1 = switchRepository.findOne(Long.parseLong(switchlist.get(i)));
							if(switch1.getIotProduct().getRoom().getHideStatus().equals("0")){
								if(switch1.getHideStatus() == "0" || switch1.getHideStatus().equals("0")){
									ChangeSwitchStatusDTO getSwitch = new ChangeSwitchStatusDTO();
									getSwitch.setSwitchId(Long.parseLong(switchlist.get(i)));
									getSwitch.setSwitchStatus(profileRepo.getSwitchOnOffStatus());
									getSwitch.setUserid(changeStatusByHomeDTO.getUserId());
									if(dimmerList.get(i).equals("NA")){
										getSwitch.setDimmerValue(switch1.getDimmerValue());
									}else{
										getSwitch.setDimmerValue(dimmerList.get(i));
									}
									switchService.changeSwitchStatusByRoom(getSwitch);
									switchList.add(new ChangeStatusByRoomResponseDTO(switch1.getId().toString(), profileRepo.getSwitchOnOffStatus(), getSwitch.getDimmerValue(),
											switch1.getIotProduct().getRoom().getId().toString()));
								}
							}else{
								logger.info("profileOnOff method, Room is Hidden to Switch Status: "+switchStatus);
							}
						}
					}
				}
			
			}else{
				logger.error("profileOnOff(), Profile list not found");
				throw new Exception("Profile list not Found");
			}
			
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}
			
			activity.setMessage(userDetails.getFirstName().toString() + " activated "+profileRepo.getProfileName()+ " profile to switch "+switchStatus);
			activity.setMessageType("TurnOffAll");
			activity.setRoomname("Home");
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(changeStatusByHomeDTO.getMessageFrom());
			
			addActivity = activityRepository.save(activity);
			logger.info("Activity Saved Successfully: "+activity);
			
			if(changeStatusByHomeDTO.getMessageFrom().equals("ProfileScheduler")){
				jMessage.put(Constants.STATUS, "ProfileScheduler");
			}else{
				jMessage.put(Constants.STATUS, "TurnOffAll");
			}
			
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			message = userDetails.getFirstName().toString() + " activated "+profileRepo.getProfileName()+" profile to turn "+switchStatus+" switches";
			//message = userDetails.getFirstName().toString() + TURN + switchStatus
				//	+ " home ";
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.SWITCH_LIST, switchList);
			jMessage.put(Constants.SWITCH_NUMBER, " ");
			jMessage.put(Constants.SWITCH_NAME, " ");
			jMessage.put(Constants.SWITCH_STATUS, profileRepo.getSwitchOnOffStatus());
			jMessage.put(Constants.DIMMER_STATUS, " ");
			jMessage.put(Constants.DIMMER_VALUE, profileRepo.getDimmerValues());
			jMessage.put(Constants.ACTIVITY_ID, addActivity.getId().toString());
			jMessage.put(Constants.LOCK_STATUS, " ");
			jMessage.put(Constants.MESSAGE_FROM, changeStatusByHomeDTO.getMessageFrom().toString());
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
			}
			jMessage.put(Constants.ROOM_ID, switch1.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, "Home");
			jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info(jMessage);
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException exception) {
						exception.printStackTrace();
					}
				}
			});

			return switchList;
		} catch (Exception exception) {
			logger.error("Exception in profileonoff(), "+exception.getMessage());
			exception.printStackTrace();
			throw new Exception("Exception Message: "+exception.getMessage());
		}
	}


	@Override
	public ProfileResponseDTO addProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		Profile profile = new Profile();
		JSONObject jMessage = new JSONObject();

		try{
			
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
			
			profile.setProfileName(profileRequestDTO.getProfileName());
			profile.setSwitchList(profileRequestDTO.getSwitchList());
			profile.setSwitchOnOffStatus(profileRequestDTO.getSwitchStatus());
			profile.setDimmerValues(profileRequestDTO.getDimmerValues());
			profile.setUser(userDetails);
			
			profileRepository.save(profile);
			logger.info("addProfileMode(), Profile data saved Successfully");
			
			jMessage.put(Constants.STATUS, "AddProfile");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_ID, profile.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, profileRequestDTO.getProfileName().toString());	
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			jMessage.put(Constants.SWITCH_LIST, profileRequestDTO.getSwitchList());
			jMessage.put(Constants.SWITCH_STATUS, profileRequestDTO.getSwitchStatus());
			jMessage.put(Constants.DIMMER_VALUE, profileRequestDTO.getDimmerValues());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("addProfileMode(), JSON Message Created: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
		
			return new ProfileResponseDTO(profile.getId().toString(), profile.getUser().getId().toString(), profile.getProfileName(), profile.getSwitchList(), 
					profile.getSwitchOnOffStatus(), profile.getDimmerValues(), profileRequestDTO.getMessageFrom());
		}catch(Exception exception){
			
			logger.error("Exception in addProfileMode() Exception Message: "+exception.getMessage());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}


	@Override
	public ProfileResponseDTO editProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();

		try{
			
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
			Profile profileDetails = profileRepository.findOne(profileRequestDTO.getProfileId());
			
			profileDetails.setProfileName(profileRequestDTO.getProfileName());
			profileDetails.setSwitchList(profileRequestDTO.getSwitchList());
			profileDetails.setSwitchOnOffStatus(profileRequestDTO.getSwitchStatus());
			profileDetails.setDimmerValues(profileRequestDTO.getDimmerValues());
			
			profileRepository.save(profileDetails);
			
			logger.info("editProfileMode(), Profile data saved Successfully");
			
			jMessage.put(Constants.STATUS, "EditProfile");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_ID, profileDetails.getId().toString());
			jMessage.put(Constants.PROFILE_NAME, profileDetails.getProfileName());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			jMessage.put(Constants.SWITCH_LIST, profileDetails.getSwitchList());
			jMessage.put(Constants.SWITCH_STATUS, profileDetails.getSwitchOnOffStatus());
			jMessage.put(Constants.DIMMER_VALUE, profileRequestDTO.getDimmerValues());
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("Edit Profile Mode Method, JSON Message Created: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});

			return new ProfileResponseDTO(profileDetails.getId().toString(), profileDetails.getUser().getId().toString(), profileDetails.getProfileName(), profileDetails.getSwitchList(), 
					profileDetails.getSwitchOnOffStatus(), profileDetails.getDimmerValues(), profileRequestDTO.getMessageFrom());
		
		}catch(Exception exception){
			logger.error("Exception in Edit Profile Mode Method, Exception Message: "+exception.getMessage());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}


	@Override
	public ProfileResponseDTO deleteProfileMode(ProfileRequestDTO profileRequestDTO) throws Exception {
		
		JSONObject jMessage = new JSONObject();
		String message = "";
		try{
			UserDetails userDetails = userDetailsRepository.findOne(profileRequestDTO.getUserId());
		
			profileRepository.delete(profileRequestDTO.getProfileId());
			message = "Profile Deleted Successfully";
			
			ScheduleSwitch scheduleSwitch[] = scheduleSwitchRepository.findScheduleByProfileId(profileRequestDTO.getProfileId());
			
			for(int index = 0; index < scheduleSwitch.length; index++){
				scheduleSwitchRepository.delete(scheduleSwitch[index].getId());
			}	
			
			logger.info("Delete Profile Mode Method, "+message+" User Id: "+profileRequestDTO.getUserId());
			
			jMessage.put(Constants.STATUS, "DeleteProfile");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.PROFILE_ID, profileRequestDTO.getProfileId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			jMessage.put(Constants.MESSAGE, message);
			jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
			
			logger.info("Delete Profile Mode Method, JSON Message Created: "+jMessage);
			
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
			
			return new ProfileResponseDTO(profileRequestDTO.getProfileId().toString(), userDetails.getId().toString(), profileRequestDTO.getProfileName(), profileRequestDTO.getSwitchList(), 
					profileRequestDTO.getSwitchStatus(), profileRequestDTO.getDimmerValues(), profileRequestDTO.getMessageFrom());
			
		}catch(Exception exception){
			message = "Error while deleting Profile Mode. Profile Id: "+profileRequestDTO.getProfileId();
			logger.error("Exception in deleteProfileMode(), "+message+" Exception Message: "+exception.getMessage()+" User Id: "+profileRequestDTO.getUserId());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
	}

	@Override
	public List<ProfileResponseDTO> getProfileList(ProfileRequestDTO profileRequestDTO) throws Exception {
		List<Profile> listProfile = new ArrayList<Profile>();
		List<ProfileResponseDTO> listRespProfileDTO = new ArrayList<ProfileResponseDTO>();
		
		JSONObject jMessage = new JSONObject();
		
		String message = "";
		
		try{
			
			listProfile = profileRepository.getProfileByUserId(profileRequestDTO.getUserId());

			if(listProfile.size() > 0){
			
				message = "Profile List Fetched Successfully";
				logger.info("getProfileList(), "+message+" User Id: "+profileRequestDTO.getUserId());
				
				for(int index = 0; index < listProfile.size();index++){
					
					listRespProfileDTO.add(new ProfileResponseDTO(
							listProfile.get(index).getId().toString(),
							listProfile.get(index).getUser().getId().toString(),
							listProfile.get(index).getProfileName(),
							listProfile.get(index).getSwitchList(),
							listProfile.get(index).getSwitchOnOffStatus(),
							listProfile.get(index).getDimmerValues(),
							profileRequestDTO.getMessageFrom()
							));
				}
				
				jMessage.put(Constants.STATUS, "GetProfileList");
				jMessage.put(Constants.PROFILE_LIST, listRespProfileDTO);
				jMessage.put(Constants.USER_ID, listProfile.get(0).getUser().getId().toString());
				jMessage.put(Constants.USER_NAME, listProfile.get(0).getUser().getFirstName().toString());
				jMessage.put(Constants.MESSAGE, message);
				jMessage.put(Constants.MESSAGE_FROM, profileRequestDTO.getMessageFrom());
				
				logger.info("getProfileList(), JSON Message Created: "+jMessage);
				
				MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
				refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				// refreshBackMessage to mobile or to other thin end clients
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
				
				String publicTopic = "refreshIntBackGenieHomeId_" + listProfile.get(0).getUser().getHome().getId().toString();
				//child thread to publish on Internet 
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				});
			
			}
			
		}catch(Exception exception){
			message = "Error while Fetching Profile Mode List. Profile Id: "+profileRequestDTO.getProfileId();
			logger.error("Exception in getProfileList(), "+message+" Exception Message: "+exception.getMessage()+" User Id: "+profileRequestDTO.getUserId());
			exception.printStackTrace();
			throw new Exception(exception);
		}
		
		return listRespProfileDTO;
	}

}