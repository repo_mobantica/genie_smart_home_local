package com.gsmarthome.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.RoomAndSwitchResponseDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Command;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.NotificationRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.SwitchTypeRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttlConnection;

@Component
public class SwitchServiceImpl implements SwitchService {
	
	/* Get actual class name to be printed on */
	static Logger logger = Logger.getLogger(SwitchServiceImpl.class.getName());
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:00");

	public static int curtainCount = 1;
	
	public static final String TURN = " turn "; //purposely added the space before and after turn
	
	public static final String SET_DIMMER_VALUE_WHEN_ON = "38"; //0-75 is dimmer value to hub hence 50% of it is 37avg
	
	Panel panel =  null;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	RoomRepository roomRepository;
	
	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	SwitchTypeRepository switchTypeRepository;

	@Autowired
	IOTProductRepository iOTProductRepository;

	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	CommandRepository commandRepository;

	@Autowired
	ActivityRepository activityRepository;

	@Value("${SWITCH_NAME_ALREADY_PRESENT}")
	private String SWITCH_NAME_ALREADY_PRESENT;

	@Value("${IMAGE_LOCATION}")
	private String IMAGE_LOCATION;
	

	@SuppressWarnings("deprecation")
	@Override
	public Room addEditSwitch(AddUpdateSwitchDTO switchDTO) {

		Activity activity = new Activity();
		Activity addactivity = new Activity();
		
		Date date = new Date();		
		
		try {
			if (switchDTO.getSwitchId() != null) {
				Switch fetchedSwitch = switchRepository.findOne(switchDTO.getSwitchId());

				logger.info("Add Edit Switch method, fetched Switch Details: "+"Switch ID: "+fetchedSwitch.getId());
				
				String oldswitchname = fetchedSwitch.getSwitchName();
				long userId = switchDTO.getUserId();
				
				UserDetails userDetails = userDetailsRepository.findOne(userId);
				
				logger.info("User details fetched "+"User ID: "+userId);
				
				if (switchDTO.getSwitchName() != null)
					fetchedSwitch.setSwitchName(switchDTO.getSwitchName());
				if (switchDTO.getSwitchType() != null)
					fetchedSwitch.setSwitchType(switchDTO.getSwitchType());
				/*if (switchDTO.getDimmerStatus() != null)
					fetchedSwitch.setDimmerStatus(switchDTO.getDimmerStatus());*/
				if (switchDTO.getDimmerValue() != null)
					fetchedSwitch.setDimmerValue(switchDTO.getDimmerValue());
				if (switchDTO.getSwitchStatus() != null)
					fetchedSwitch.setSwitchStatus(switchDTO.getSwitchStatus());
				if (switchDTO.getSwitchidentifier() != null)
					fetchedSwitch.setSwitchIdentifier(switchDTO.getSwitchidentifier());
				if (switchDTO.getSwitchImageId() != null)
					fetchedSwitch.setSwitchImageId(switchDTO.getSwitchImageId());

				switchRepository.save(fetchedSwitch);
				
				logger.debug("addEditSwitch() - Saved Switch Details. "+"Switch ID: "+fetchedSwitch.getId()
				+"Switch Name: "+fetchedSwitch.getSwitchName());
				
				if (userDetails.getImage() == null) {
					activity.setImage("");
				} else {
					activity.setImage(userDetails.getId()+ ".png");
					//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
				}
				activity.setMessage(userDetails.getFirstName().toString() + " updated switch name from " + oldswitchname
						+ " to " + fetchedSwitch.getSwitchName());
				activity.setMessageType(Constants.SWITCH_NAME_UPDATE_MESSAGE_TYPE);
				activity.setRoomname(fetchedSwitch.getIotProduct().getRoom().getRoomName().toString());
				activity.setCreated_date(dateFormat.format(new Date()).toString());
				activity.setMessageFrom(switchDTO.getMessageFrom().toString());
				
				addactivity = activityRepository.save(activity);
				
				logger.debug("Add Edit Switch method, Saved Activity Details. "+"Activity ID: "+addactivity.getId()
				+"Room Name: "+addactivity.getRoomname());
				
				JSONObject jMessage = new JSONObject();
				jMessage.put(Constants.STATUS, Constants.SWITCH_NAME_UPDATE_MESSAGE_TYPE);
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
				String message = userDetails.getFirstName().toString() + " updated switch name from " + oldswitchname
						+ " to " + fetchedSwitch.getSwitchName();
				jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
				jMessage.put(Constants.SWITCH_ID, fetchedSwitch.getId().toString());
				jMessage.put(Constants.SWITCH_NUMBER, fetchedSwitch.getSwitchNumber().toString());
				jMessage.put(Constants.SWITCH_NAME, fetchedSwitch.getSwitchName().toString());
				jMessage.put(Constants.SWITCH_STATUS, fetchedSwitch.getSwitchStatus().toString());
				jMessage.put(Constants.DIMMER_STATUS, fetchedSwitch.getDimmerStatus().toString());
				jMessage.put(Constants.DIMMER_VALUE, fetchedSwitch.getDimmerValue().toString());
				jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
				jMessage.put(Constants.SWITCH_IMAGE_ID, switchDTO.getSwitchImageId().toString());

				if (userDetails.getImage() == null) {
					jMessage.put(Constants.USER_IMAGE, "");
				} else {
					jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");
				}
				
				jMessage.put(Constants.ROOM_ID, fetchedSwitch.getIotProduct().getRoom().getId().toString());
				jMessage.put(Constants.ROOM_NAME, fetchedSwitch.getIotProduct().getRoom().getRoomName().toString());

				jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
						+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

				jMessage.put(Constants.MESSAGE_FROM, switchDTO.getMessageFrom());
				
				System.out.print("JSON Message for add Edit Switch Method"+jMessage);
				
				MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
				refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				
				// refreshBackMessage to mobile or to other thin end clients
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
				
				logger.debug("Add Edit Switch method, MQTT Local Publish Called. "+"Activity ID: "+jMessage.getInt(Constants.ROOM_ID)
				+"Room Name: "+jMessage.get(Constants.ROOM_NAME));
				
				
				String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
				
				
				//child thread to publish on Internet 
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				});
				
				logger.debug("Add Edit Switch method, MQTT Internet Publish Called. "+"JSON Message Room ID: "+jMessage.getInt(Constants.ROOM_ID)
				+"JSON Message Room Name: "+jMessage.get(Constants.ROOM_NAME));
				
				
				return fetchedSwitch.getIotProduct().getRoom();
				
			} else {
				if (switchDTO.getIotProductId() != null) {
					
					IOTProduct iotProduct = iOTProductRepository.findOne(switchDTO.getIotProductId());

					Switch newSwitch = new Switch(switchDTO.getSwitchidentifier(), switchDTO.getSwitchType(),
							switchDTO.getDimmerStatus(), switchDTO.getDimmerValue(), switchDTO.getSwitchStatus(),
							switchDTO.getSwitchName());

					newSwitch.setIotProduct(iotProduct);

					iotProduct.getSwitchList().add(newSwitch);

					switchRepository.save(newSwitch);

					logger.debug("Add Edit Switch method, Saved Switch Details. "+"Switch ID: "+newSwitch.getId()
					+"Switch Name: "+newSwitch.getSwitchName());
					
					return iotProduct.getRoom();
				} else {
					Room room = roomRepository.findOne(switchDTO.getRoomId());

					Boolean ifSwitchAlreadyPresent = false;

					for (IOTProduct iotProduct : room.getProductList()) {
						for (Switch switch1 : iotProduct.getSwitchList()) {
							if (switch1.getSwitchName().equals(switchDTO.getSwitchName()))
								ifSwitchAlreadyPresent = true;
						}
					}
					if (ifSwitchAlreadyPresent)
						throw new RuntimeException(SWITCH_NAME_ALREADY_PRESENT);
					IOTProduct newIOTProduct = new IOTProduct(room.getRoomType() + "Prod");
					newIOTProduct.setRoom(room);
					room.getProductList().add(newIOTProduct);
					Switch newSwitch = new Switch(switchDTO.getSwitchidentifier(), switchDTO.getSwitchType(),
							switchDTO.getDimmerStatus(), switchDTO.getDimmerValue(), switchDTO.getSwitchStatus(),
							switchDTO.getSwitchName());
					newSwitch.setIotProduct(newIOTProduct);
					newIOTProduct.getSwitchList().add(newSwitch);
					iOTProductRepository.save(newIOTProduct);
					
					logger.debug("Add Edit Switch method, Saved IOT Product Details. "+"IOT Product ID: "+newIOTProduct.getId()
					+"IOT Product Name: "+ newIOTProduct.getProductName());
					
					return room;
				}
			}
		} catch (Exception ex) {
			logger.debug("Add Edit Switch method, Saved Switch Details. "+"Switch ID: "+ switchDTO.getSwitchId()
			+"Switch Name: "+ switchDTO.getSwitchName());
			
			ex.printStackTrace();
			
		}
		return null;

	}

	
	@Override
	public Boolean deleteSwitch(Long switchId) {
		switchRepository.delete(switchId);
		
		logger.debug("deleteSwitch(), Switch deleted Successfully. "+"Switch ID: "+switchId);
		
		return true;
	}

	/*
	 * this function returns list of switches against room id
	 */
	@Override
	public List<SwitchListByRoomDTO> getSwitchListByRoom(Long roomId) {
		
		int skipNextSwitchCount = 0;
		
		Room room = roomRepository.findOne(roomId);
		
		logger.info("fetched room details Room Id: "+roomId);
		List<Switch> switchList = new ArrayList<Switch>();
		
		List<SwitchListByRoomDTO> listSwitchByRoomDTO = new ArrayList<SwitchListByRoomDTO>();
		
		if(room != null){
			for (IOTProduct iotProduct : room.getProductList())
				switchList.addAll(iotProduct.getSwitchList());
		
		}else{
			return null;
		}
		
		for (Switch switch1 : switchList) {
			
			SwitchType switchType = switchTypeRepository.findOne(Long.parseLong(switch1.getSwitchType()));
			
			if(switch1.getDimmerStatus().equals("2")){
				skipNextSwitchCount++;
			}
			
			if(skipNextSwitchCount != 2){
				listSwitchByRoomDTO.add(new SwitchListByRoomDTO(switch1.getId().toString(), switch1.getSwitchName(),
					switch1.getSwitchType(), switch1.getDimmerStatus(), switch1.getDimmerValue(),
					switch1.getSwitchStatus(), switchType.getOnImage(), switchType.getOffImage(),
					switch1.getLockStatus(), switch1.getHideStatus(), switch1.getSwitchIdentifier(),
					switch1.getSwitchImageId().toString()));	
			}else{
				skipNextSwitchCount = 0;
			}
		}
		
		logger.debug("Get Switch List Room method, Fetched Switch By Room Details Successfully. "+"Room ID: "+roomId);
		
		return listSwitchByRoomDTO;
	}
	
	@Override
	public ChangeSwitchStatusResponseDTO changeCurtainStatus(ChangeCurtainStatusDTO changeCurtainStatusDTO) {
	
		String switchStatus;
		String message = "";
		
		Activity addActivity = new Activity();
		Activity activity = new Activity();
		
		JSONObject jMessage = new JSONObject();
		
		Panel panel = new Panel();
		
		Switch switchValidOrInvalid = switchRepository.findOne(changeCurtainStatusDTO.getSwitchId());
		Switch switchValidOrInvalid2 = switchRepository.findOne(changeCurtainStatusDTO.getSwitchId()+1);
		
		logger.info("Change Curtain Status Fetched switch Details Switch ID: "+changeCurtainStatusDTO.getSwitchId());
		
		
		UserDetails userDetails = userDetailsRepository.findOne(changeCurtainStatusDTO.getUserId());

		logger.info("Change Curtain Status Method, user Details Fetched. User Id: "+changeCurtainStatusDTO.getUserId());

		try {

			int panelId = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			
			panel = panelRepository.findByPanelId(panelId);

			logger.info("Change Curtain Status Method, Panel Details Fetched.");
			
			if (panel != null) {
				
				if(switchValidOrInvalid.getDimmerStatus().equals(Constants.DIMMER_STATUS_FOR_CURTAIN)){
					makeCurtainOpenCloseCommand(switchValidOrInvalid, changeCurtainStatusDTO, panel);
					switchValidOrInvalid.setSwitchStatus(changeCurtainStatusDTO.getSwitchStatus());
					switchValidOrInvalid2.setSwitchStatus(changeCurtainStatusDTO.getCurtainStatus());
				}
				
				if (switchValidOrInvalid.getSwitchStatus().equals(Constants.SWITCH_ON)) {
					switchStatus = Constants.SWITCH_STATUS_ON;
				} else {
					switchStatus = Constants.SWITCH_STATUS_OFF;
				}

				if (userDetails.getImage() == null) {
					activity.setImage("");
				} else {
					activity.setImage(userDetails.getId()+ ".png");
				}
				
				
				activity.setMessageType(Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
				activity.setRoomname(switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());
				activity.setCreated_date(dateFormat.format(new Date()).toString());
				activity.setMessageFrom(changeCurtainStatusDTO.getMessageFrom());
			
				if(switchStatus.equals(Constants.SWITCH_STATUS_ON)){
					 message =  userDetails.getFirstName().toString() + " opened the "
							+ switchValidOrInvalid.getSwitchName();
					
					 activity.setMessage(userDetails.getFirstName().toString() +  " opened the "
								+ switchValidOrInvalid.getSwitchName());
				
				}else{
					message = userDetails.getFirstName().toString() + " closed the "
							+ switchValidOrInvalid.getSwitchName();
					activity.setMessage(userDetails.getFirstName().toString() +  " closed the "
							+ switchValidOrInvalid.getSwitchName());					
				}

				addActivity = activityRepository.save(activity);
				logger.info("Change Curtain Status, Activity added successfully. Activity message: "+activity.getMessage());
				
				jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
				jMessage.put(Constants.STATUS, Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
				jMessage.put(Constants.SWITCH_ID, switchValidOrInvalid.getId().toString());
				jMessage.put(Constants.SWITCH_NUMBER, switchValidOrInvalid.getSwitchNumber().toString());
				jMessage.put(Constants.SWITCH_NAME, switchValidOrInvalid.getSwitchName().toString());
				jMessage.put(Constants.SWITCH_STATUS, switchValidOrInvalid.getSwitchStatus().toString());
				jMessage.put(Constants.DIMMER_STATUS, switchValidOrInvalid.getDimmerStatus().toString());
				jMessage.put(Constants.DIMMER_VALUE, switchValidOrInvalid.getDimmerValue().toString());
				jMessage.put(Constants.ACTIVITY_ID, addActivity.getId().toString());
				jMessage.put(Constants.LOCK_STATUS, switchValidOrInvalid.getLockStatus().toString());
				jMessage.put(Constants.MESSAGE_FROM, changeCurtainStatusDTO.getMessageFrom());

				if (userDetails.getImage() == null) {
					jMessage.put(Constants.USER_IMAGE, "");
				} else {
					jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
				}
				
				jMessage.put(Constants.ROOM_ID, switchValidOrInvalid.getIotProduct().getRoom().getHome().toString());
				jMessage.put(Constants.ROOM_NAME, switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());

				Date date = new Date();

				jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
						+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

				logger.debug("Curtain Open and Close JSON Message :"+jMessage);
				
				//refreshBackMessage to mobile or to other thin end clients
				MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
			    
				refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				
			    MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
				String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
				
				//child thread to publish on Internet
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
						} catch (MqttException e) {							
							e.printStackTrace();
						}
					}
				});
				
				logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
				
				
				switchRepository.save(switchValidOrInvalid);
				switchRepository.save(switchValidOrInvalid2);
				
				logger.debug("Curtain Open and Close Success for Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
						+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
				
				
				return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
						switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
						switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
						switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
						Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),				
						switchValidOrInvalid.getHideStatus(), Constants.SUCCESS_MSG);
			}
			else {

				logger.error(Constants.FAILURE_MSG + "Panel not found Panel ID: " + switchValidOrInvalid.getIotProduct().getIotProductNumber().toString()+
						"Switch Number: " + switchValidOrInvalid.getSwitchNumber().toString());
				
				return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
						switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
						switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
						switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
						"lockbit", switchValidOrInvalid.getLockStatus(),
						switchValidOrInvalid.getHideStatus(), Constants.FAILURE_MSG);
			}
		} catch (Exception e) {

			logger.error(Constants.EXCEPTION_MSG + "Panel not found Panel ID: " + switchValidOrInvalid.getIotProduct().getIotProductNumber().toString()+
					"Switch Number: " + switchValidOrInvalid.getSwitchNumber().toString());
			
			e.printStackTrace();
			
			return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
					switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
					switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
					switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
					Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
					switchValidOrInvalid.getHideStatus(), Constants.FAILURE);
		}

	}

	/*
	 * this function turn on/off
	 */
	@SuppressWarnings("deprecation")
	@Override
	public ChangeSwitchStatusResponseDTO switchOnOff(ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		
<<<<<<< HEAD
		System.out.println("changeSwitchStatusDTO =useid= "+changeSwitchStatusDTO.getUserid());
		System.out.println("changeSwitchStatusDTO =getSwitchId= "+changeSwitchStatusDTO.getSwitchId());
		System.out.println("changeSwitchStatusDTO =getSwitchStatus= "+changeSwitchStatusDTO.getSwitchStatus());
		System.out.println("changeSwitchStatusDTO =getMessageFrom= "+changeSwitchStatusDTO.getMessageFrom());
=======
>>>>>>> stage
		String switchStatus;
		Switch switchValidOrInvalid =  null;
		
		Activity activity = new Activity();
		
		JSONObject jMessage = new JSONObject();
		
		Panel panel = new Panel();

		UserDetails userDetails = userDetailsRepository.findOne(changeSwitchStatusDTO.getUserid());
		
		if(userDetails != null){
			switchValidOrInvalid = switchRepository.findOne(changeSwitchStatusDTO.getSwitchId());
			logger.info("Switch On Off Method Fetched switch Details Switch ID: "+changeSwitchStatusDTO.getSwitchId());
		}

		logger.info("Switch On Off Method, fetched user details User Id: "+changeSwitchStatusDTO.getUserid());

		try {

			int panelId = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(panelId);

			logger.info("Switch On Off Command Fetched Panel Details Panel Id: "+panelId);
			
			if (panel != null) {
				
				if(switchValidOrInvalid.getHideStatus().equals("0") && switchValidOrInvalid.getIotProduct().getRoom().getHideStatus().equals("0")){
					
					fireOnOffCommand(switchValidOrInvalid, changeSwitchStatusDTO, panel);
					logger.info("switchOnOff Method, Switch ON/OFF occured ");
					
					switchValidOrInvalid.setSwitchStatus(changeSwitchStatusDTO.getSwitchStatus());
					switchValidOrInvalid.setDimmerValue(changeSwitchStatusDTO.getDimmerValue());
				
				
					if (switchValidOrInvalid.getSwitchStatus().equals("1")) {
						switchStatus = Constants.SWITCH_STATUS_ON;
					} else {
						switchStatus = Constants.SWITCH_STATUS_OFF;
					}
	
					if (userDetails.getImage() == null) {
						activity.setImage("");
					} else {
						activity.setImage(userDetails.getId()+ ".png");
					}
					activity.setMessage(userDetails.getFirstName().toString() +  TURN + switchStatus + " "
							+ switchValidOrInvalid.getSwitchName());
					activity.setMessageType(Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
					activity.setRoomname(switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());
					activity.setCreated_date(dateFormat.format(new Date()).toString());
					activity.setMessageFrom(changeSwitchStatusDTO.getMessageFrom());
					
					Activity addactivity = new Activity();
					
					addactivity = activityRepository.save(activity);
					logger.info("activity added Successfully. Activity Object: "+activity);
					
					
					if(changeSwitchStatusDTO.getMessageFrom().equals("Schedular"))
					{				
						jMessage.put(Constants.STATUS, Constants.SCHEDULAR_SWITCH_ON_OFF_MESSAGE_TYPE);
					}else{
						jMessage.put(Constants.STATUS, Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
					}
					
					jMessage.put(Constants.USER_ID, userDetails.getId().toString());
					jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
	
					if (switchValidOrInvalid.getSwitchNumber() >= 5) {
						String message = "";
						int dimmerValue = Integer.parseInt(switchValidOrInvalid.getDimmerValue());
						dimmerValue = (dimmerValue * 100)/75;
						
						message = userDetails.getFirstName().toString() +  TURN + switchStatus + " "
								+ switchValidOrInvalid.getSwitchName() + " (" + dimmerValue + ")";
						jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
						
					} else {
						String message = userDetails.getFirstName().toString() + TURN + switchStatus + " "
								+ switchValidOrInvalid.getSwitchName();
						jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
					}
	
					jMessage.put(Constants.SWITCH_ID, switchValidOrInvalid.getId().toString());
					jMessage.put(Constants.SWITCH_NUMBER, switchValidOrInvalid.getSwitchNumber().toString());
					jMessage.put(Constants.SWITCH_NAME, switchValidOrInvalid.getSwitchName().toString());
					jMessage.put(Constants.SWITCH_STATUS, switchValidOrInvalid.getSwitchStatus().toString());
					jMessage.put(Constants.DIMMER_STATUS, switchValidOrInvalid.getDimmerStatus().toString());
					jMessage.put(Constants.DIMMER_VALUE, switchValidOrInvalid.getDimmerValue().toString());
					jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
					jMessage.put(Constants.LOCK_STATUS, switchValidOrInvalid.getLockStatus().toString());
					jMessage.put(Constants.MESSAGE_FROM, changeSwitchStatusDTO.getMessageFrom().toString());
					
					if (userDetails.getImage() == null) {
						jMessage.put(Constants.USER_IMAGE, "");
					} else {
						jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
	
					}
					jMessage.put(Constants.ROOM_ID, switchValidOrInvalid.getIotProduct().getRoom().getId().toString());
					jMessage.put(Constants.ROOM_NAME, switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());
	
					Date date = new Date();
					jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
							+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
	
					logger.debug("Switch ON or OFF JSON Message :"+jMessage);
					
					// refreshBackMessage to mobile or to other thin end clients
					MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
				    
					refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
					
				    MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
					String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic:\t"+publicTopic);
					
					//child thread to publish on Internet
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t"+publicTopic);
								MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
							} catch (MqttException e) {
								System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+publicTopic);
								e.printStackTrace();
							}
						}
					});
					
					logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
					
					
					switchRepository.save(switchValidOrInvalid);
					
					logger.debug("Switch ON or OFF Success for Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
							+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
					
					
					return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
							switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
							switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
							switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
							changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),				
							switchValidOrInvalid.getHideStatus(), Constants.SUCCESS_MSG);
				
				}else{
					
					logger.info("SwitchOnOFF Method, No action will be performed since room or Switch is hidden");
					return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + userDetails.getHome().getId().toString(),
							switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
							switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
							switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
							changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),				
							switchValidOrInvalid.getHideStatus(), Constants.SUCCESS_MSG);
				}
			}
			else {

				logger.error(Constants.FAILURE_MSG + "Panel not found Panel ID: " + switchValidOrInvalid.getIotProduct().getIotProductNumber().toString()+
						"Switch Number: " + switchValidOrInvalid.getSwitchNumber().toString());
				
				return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
						switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
						switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
						switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
						changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
						switchValidOrInvalid.getHideStatus(), Constants.FAILURE_MSG);
			}
		} catch (Exception e) {

			logger.error(Constants.EXCEPTION_MSG + "Panel not found Panel ID: " + switchValidOrInvalid.getIotProduct().getIotProductNumber().toString()+
					"Switch Number: " + switchValidOrInvalid.getSwitchNumber().toString());
			
			e.printStackTrace();
			
			return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
					switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
					switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
					switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
					changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
					switchValidOrInvalid.getHideStatus(), Constants.FAILURE);
		}

	}
	
	void fireOnOffCommand(Switch switchValidOrInvalid, ChangeSwitchStatusDTO changeSwitchStatusDTO, Panel panel){
		
		try {
			String topic = panel.getPanelName() + "/out";
			
			StringBuffer switchOnOffCommand = new StringBuffer("$[");
			switchOnOffCommand.append(switchValidOrInvalid.getSwitchNumber());
			// command formation
			// switch number dimmer value lock
			if (switchValidOrInvalid.getSwitchNumber() < 5) {
				switchOnOffCommand.append("0");
				switchOnOffCommand.append(changeSwitchStatusDTO.getSwitchStatus());
				switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());

			} else {
				if (changeSwitchStatusDTO.getSwitchStatus().equals("0")) {
					switchOnOffCommand.append("00");
				} else {
					int dimmerValue = Integer.parseInt(changeSwitchStatusDTO.getDimmerValue());
					if (dimmerValue < 10) {
						switchOnOffCommand.append("0");
						switchOnOffCommand.append(changeSwitchStatusDTO.getDimmerValue());

					} else {
						switchOnOffCommand.append(changeSwitchStatusDTO.getDimmerValue());
					}

				}
				switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());
			}

			switchOnOffCommand.append("]");

			logger.info("switchOnOff() Command\t" + switchOnOffCommand.toString());
			logger.info(switchOnOffCommand);
			
			MqttMessage messageOff = new MqttMessage(switchOnOffCommand.toString().getBytes());
			
			messageOff.setQos(Constants.QoS_EXACTLY_ONCE);
			System.out.println("messageOff lovcal = "+messageOff);
			
			MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageOff);
			
			logger.debug("MQTT Message Published Locally Successfully.");
			
			Command commandSave = new Command(String.valueOf(switchOnOffCommand),
					dateFormat.format(new Date()).toString());
			
			saveSwitchCommand(commandSave, panel);

		} catch (Exception me) {
			logger.error("Exception in switchOnOff(). "+"Switch ID: "+changeSwitchStatusDTO.getSwitchId());
			me.printStackTrace();
		}
	}
	
	void makeCurtainOpenCloseCommand(Switch switchValidOrInvalid, ChangeCurtainStatusDTO changeCurtainStatusDTO, Panel panel){
		
		try {
			String topic = panel.getPanelName() + "/out";
			
			StringBuffer switchOnOffFirstCommand = new StringBuffer("$[");
			
			//command formation 
			switchOnOffFirstCommand.append(switchValidOrInvalid.getSwitchNumber());
			
			if (switchValidOrInvalid.getSwitchNumber() < 5) {
				switchOnOffFirstCommand.append("0");
				switchOnOffFirstCommand.append(changeCurtainStatusDTO.getSwitchStatus());
				switchOnOffFirstCommand.append(switchValidOrInvalid.getLockStatus());
			}

			switchOnOffFirstCommand.append("]");

			logger.info("Curtain Open Close First Command Created");
			
			if(changeCurtainStatusDTO.getCurtainStatus().equals("1")){
				MqttMessage messageFirstOnOff = new MqttMessage(switchOnOffFirstCommand.toString().getBytes());
				messageFirstOnOff.setQos(Constants.QoS_EXACTLY_ONCE);
				MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageFirstOnOff);
			}
			
			StringBuffer switchOnOffSecondCommand = new StringBuffer("$[");
			if(changeCurtainStatusDTO.getCurtainStatus().equals("0")){
				
				// command formation 
				switchOnOffSecondCommand.append(switchValidOrInvalid.getSwitchNumber()+1);
				if (switchValidOrInvalid.getSwitchNumber() < 5) {
					switchOnOffSecondCommand.append("0");
					switchOnOffSecondCommand.append("0");	// Curtain stop switch is 0 hence Stopped
					switchOnOffSecondCommand.append(switchValidOrInvalid.getLockStatus());
				}

				switchOnOffSecondCommand.append("]");
	
				logger.info("Curtain Stopped/Paused Second Command Created");
				
				MqttMessage messageSecondOnOff = new MqttMessage(switchOnOffSecondCommand.toString().getBytes());
				
				messageSecondOnOff.setQos(Constants.QoS_EXACTLY_ONCE);
				
				MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageSecondOnOff);
			}else{
				// command formation 
				switchOnOffSecondCommand.append(switchValidOrInvalid.getSwitchNumber()+1);
				if (switchValidOrInvalid.getSwitchNumber() < 5) {
					switchOnOffSecondCommand.append("0");
					switchOnOffSecondCommand.append("1");	//Curtain stop switch is 1 hence working
					switchOnOffSecondCommand.append(switchValidOrInvalid.getLockStatus());
				}
	
				switchOnOffSecondCommand.append("]");
	
				logger.info("Curtain Open Close Second Command Created");
				
				MqttMessage messageSecondOnOff = new MqttMessage(switchOnOffSecondCommand.toString().getBytes());
				
				messageSecondOnOff.setQos(Constants.QoS_EXACTLY_ONCE);
				
				MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageSecondOnOff);
			}
			
			logger.debug("MQTT Message Published Locally Successfully.");
			
			Command commandFirstSave = new Command(String.valueOf(switchOnOffFirstCommand),
					dateFormat.format(new Date()).toString());
			
			Command commandSecondSave = new Command(String.valueOf(switchOnOffSecondCommand),
					dateFormat.format(new Date()).toString());
			
			saveSwitchCommand(commandFirstSave, panel);
			
			saveSwitchCommand(commandSecondSave, panel);

		} catch (Exception me) {
			logger.error("Exception in switchOnOff(). "+"Switch ID: "+changeCurtainStatusDTO.getSwitchId());
			me.printStackTrace();
		}
	}
	
	void unlockHomeCommand(Switch homeLockSwitch, Panel panel){
		
		try {
			String topic = panel.getPanelName() + "/out";
			
			StringBuffer switchLockFirstCommand = new StringBuffer("$[");
			
			switchLockFirstCommand.append(homeLockSwitch.getSwitchNumber());
			switchLockFirstCommand.append("010]");

			logger.info("unlockHomeCommand() First Command\t" + switchLockFirstCommand.toString());
			logger.info(switchLockFirstCommand);
			
			MqttMessage firstLockmessage = new MqttMessage(switchLockFirstCommand.toString().getBytes());
			
			firstLockmessage.setQos(Constants.QoS_EXACTLY_ONCE);
			
			MqttlConnection.getObj().getLocalMqttConnection().publish(topic, firstLockmessage);

			Thread.sleep(3000);
			
			StringBuffer switchLockSecondCommand = new StringBuffer("$[");
			
			switchLockSecondCommand.append(homeLockSwitch.getSwitchNumber());
			switchLockSecondCommand.append("000]");
			
			logger.info("unlockHomeCommand() Second Command\t" + switchLockSecondCommand.toString());
			logger.info(switchLockSecondCommand);
			
			MqttMessage secondLockMessage = new MqttMessage(switchLockSecondCommand.toString().getBytes());
			
			secondLockMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			
			MqttlConnection.getObj().getLocalMqttConnection().publish(topic, secondLockMessage);

			logger.debug("MQTT Message Published Locally Successfully.");
			
			Command commandSave = new Command(String.valueOf(switchLockSecondCommand),
					dateFormat.format(new Date()).toString());
			
			saveSwitchCommand(commandSave, panel);

		} catch (Exception me) {
			logger.error("Exception in unlockHomeCommand(). "+"Switch ID: "+homeLockSwitch.getId());
			me.printStackTrace();
		}
	}

	@Override
	public ChangeSwitchStatusResponseDTO changeSwitchStatusByRoom(ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		/*
		 * This function turn off whole room off 
		 */
		Panel panel = new Panel();
		Switch switchValidOrInvalid = switchRepository.findOne(changeSwitchStatusDTO.getSwitchId());
		switchValidOrInvalid.setSwitchStatus(changeSwitchStatusDTO.getSwitchStatus());
		switchValidOrInvalid.setDimmerValue(changeSwitchStatusDTO.getDimmerValue());

		try {
			int panelId = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(panelId);
			if (panel != null) {
				try {

					StringBuffer switchOnOffCommand = new StringBuffer("$[");
					
					switchOnOffCommand.append(switchValidOrInvalid.getSwitchNumber());
					String topic = panel.getPanelName() + "/out";

					/*
					 * command formation switch number dimmer value lock
					 * 
					 */
					if (switchValidOrInvalid.getSwitchNumber() < 5) {
						switchOnOffCommand.append("0");
						/*if(switchValidOrInvalid.getDimmerStatus().equals("2") && curtainCount != 2)
						{	
							curtainCount++;
							switchOnOffCommand.append(changeSwitchStatusDTO.getSwitchStatus());
						}else if(switchValidOrInvalid.getDimmerStatus().equals("2") && curtainCount == 2){
							switchOnOffCommand.append("1");
							switchValidOrInvalid.setSwitchStatus("1");
							curtainCount = 1;
						}
						else{*/
							switchOnOffCommand.append(changeSwitchStatusDTO.getSwitchStatus());	
						//}
						switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());

					} else {
						if (changeSwitchStatusDTO.getSwitchStatus().equals("0")) {
								switchOnOffCommand.append("00");
						} else {
							int dimmerValue = Integer.parseInt(changeSwitchStatusDTO.getDimmerValue());
							
							//if dimmer value is zero set to 50 i.e. 38
							if(dimmerValue == 0 && changeSwitchStatusDTO.getSwitchStatus().equals("1")){
								switchValidOrInvalid.setDimmerValue(SET_DIMMER_VALUE_WHEN_ON);
								switchOnOffCommand.append(SET_DIMMER_VALUE_WHEN_ON);
							}else if (dimmerValue >= 1 && dimmerValue < 10) {
								switchOnOffCommand.append("0");
								switchOnOffCommand.append(changeSwitchStatusDTO.getDimmerValue());

							}else {
								//ASCII character of 36 is "$" hence command fails so changing to 37
								if(changeSwitchStatusDTO.getDimmerValue().equals("36")){ 
									switchOnOffCommand.append("37");	
								}else{
									switchOnOffCommand.append(changeSwitchStatusDTO.getDimmerValue());
								}
							}
						}
						switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());
					}

					switchOnOffCommand.append("]");
					logger.info("changeSwitchStatusByRoom() Command\t" + switchOnOffCommand.toString());
					MqttMessage messageOff = new MqttMessage(switchOnOffCommand.toString().getBytes());
					messageOff.setQos(Constants.QoS_EXACTLY_ONCE);
					MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageOff);
				
					Command commandSave = new Command(String.valueOf(switchOnOffCommand),
							dateFormat.format(new Date()).toString());
					saveSwitchCommand(commandSave, panel);
					
					logger.debug("changeSwitchStatusByRoom(), Switch Command Saved Successfully. Switch Number: "+ switchValidOrInvalid.getSwitchNumber().toString()
							+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
					
				} catch (Exception me) {

					logger.error("Exception in Change Switch Status by Room method. Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
							+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
					
					me.printStackTrace();
				}

				/*
				 * refreshBackMessage to mobile or to other thin end clients
				 */
				switchRepository.save(switchValidOrInvalid);
				
				logger.debug("Change Switch Status by Room method, Switch Details Saved Successfully. Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
						+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
				
				return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId().toString() + "",
						switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
						switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
						switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
						changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
						switchValidOrInvalid.getHideStatus(), Constants.SUCCESS_MSG);
			} else {
				logger.error("Panel not found");
				
				logger.error("Exception in Change Switch Status by Room method. Panel not found Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
						+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
				
				return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId().toString() + "",
						switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
						switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
						switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
						changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
						switchValidOrInvalid.getHideStatus(), Constants.FAILURE_MSG);
			}
		} catch (Exception e) {
			
			logger.error("Exception in changeSwitchStatusByRoom(). Switch Number: "+switchValidOrInvalid.getSwitchNumber().toString()
					+"Panel ID: "+switchValidOrInvalid.getIotProduct().getIotProductNumber());
			
			e.printStackTrace();
			
			return new ChangeSwitchStatusResponseDTO(switchValidOrInvalid.getId() + "",
					switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
					switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
					switchValidOrInvalid.getSwitchNumber().toString(), switchValidOrInvalid.getSwitchStatus(),
					changeSwitchStatusDTO.getDimmerValue(), Constants.LOCKBIT, switchValidOrInvalid.getLockStatus(),
					switchValidOrInvalid.getHideStatus(), Constants.FAILURE_MSG);
		}

	}

	/* Used to lock status */
	@SuppressWarnings("deprecation")
	@Override
	public ChangeLockStatusResponseDTO changeLockStatus(ChangeLockStatusDTO changeLockStatusDTO) {

		
		try {
			Panel panel = new Panel();
			StringBuffer switchOnOffCommand;
			Switch switchValidOrInvalid = switchRepository.findOne(changeLockStatusDTO.getSwitchId());
			
			logger.info("changeLockStatus(), Switch Details Fetched Switch Id: "+changeLockStatusDTO.getSwitchId());
			
			
			switchValidOrInvalid.setLockStatus(changeLockStatusDTO.getLockStatus());
			
			UserDetails userDetails = userDetailsRepository.findOne(changeLockStatusDTO.getUserId());
			logger.info("changeLockStatus(), User Details Fetched User Id: "+changeLockStatusDTO.getUserId());
			
			String lockStatus;
			if (changeLockStatusDTO.getLockStatus().equals("1")) {
				lockStatus = Constants.LOCK;
			} else {
				lockStatus = Constants.UNLOCK;
			}

			int Panelid = switchValidOrInvalid.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(Panelid);
			logger.info("changeLockStatus(), Penal Details Fetched Panel Id: "+changeLockStatusDTO.getUserId());
			
			if (panel != null) {
				String topic = panel.getPanelName() + "/out";
				try {
					switchOnOffCommand = new StringBuffer("$[");
					switchOnOffCommand.append(switchValidOrInvalid.getSwitchNumber());
					/*
					 * command formation switch number dimmer value lock
					 * $[switchnodimmervalue/switchstatus(00/01)/lockbit]
					 */
					if (switchValidOrInvalid.getSwitchNumber() < 5) {
						switchOnOffCommand.append("0");
						switchOnOffCommand.append(switchValidOrInvalid.getSwitchStatus());
						switchOnOffCommand.append(changeLockStatusDTO.getLockStatus());

					} else {
						if (switchValidOrInvalid.getDimmerStatus().toString().equals("1")) {

							int dimmerValue = Integer.parseInt(switchValidOrInvalid.getDimmerValue());
							if (dimmerValue < 10) {
								switchOnOffCommand.append("0");
								switchOnOffCommand.append(switchValidOrInvalid.getDimmerValue());

							} else {
								switchOnOffCommand.append(switchValidOrInvalid.getDimmerValue());
							}

						} else {
							switchOnOffCommand.append("00");
						}
						switchOnOffCommand.append(switchValidOrInvalid.getLockStatus());
					}
					switchOnOffCommand.append("]");
					MqttMessage message = new MqttMessage(switchOnOffCommand.toString().getBytes());
					message.setQos(Constants.QoS_EXACTLY_ONCE);
					MqttlConnection.getObj().getLocalMqttConnection().publish(topic, message);

					Command commandSave = new Command(String.valueOf(switchOnOffCommand),
							dateFormat.format(new Date()).toString());
					
					saveSwitchCommand(commandSave, panel);
					
					switchRepository.save(switchValidOrInvalid);

					logger.info("changeLockStatus(), Switch Details Saved Successfully Switch Id:"+changeLockStatusDTO.getSwitchId());
					
					Activity activity = new Activity();
					if (userDetails.getImage() == null) {
						activity.setImage("");
					} else {
						activity.setImage(userDetails.getId()+ ".png");
						//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");

					}
					activity.setMessage(userDetails.getFirstName().toString() + " " + lockStatus + " "
							+ switchValidOrInvalid.getSwitchName());
					activity.setMessageType(Constants.SWITCH_LOCK_UNLOCK_MESSAGE_TYPE);
					activity.setRoomname(switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());
					activity.setCreated_date(dateFormat.format(new Date()).toString());
					activity.setMessageFrom(changeLockStatusDTO.getMessageFrom().toString());
					Activity addactivity = new Activity();
					addactivity = activityRepository.save(activity);
					logger.info("changeLockStatus(), Activity Details Saved Successfully"+changeLockStatusDTO.getUserId());
					

					JSONObject jMessage = new JSONObject();
					jMessage.put(Constants.STATUS, Constants.SWITCH_LOCK_UNLOCK_MESSAGE_TYPE);
					jMessage.put(Constants.USER_ID, userDetails.getId().toString());
					jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
					String message2 = userDetails.getFirstName().toString() + " " + lockStatus + " "
							+ switchValidOrInvalid.getSwitchName();
					jMessage.put(Constants.MESSAGE, message2.substring(0, 1).toUpperCase() + message2.substring(1).toLowerCase());
					jMessage.put(Constants.SWITCH_ID, switchValidOrInvalid.getId().toString());
					jMessage.put(Constants.SWITCH_NUMBER, switchValidOrInvalid.getSwitchNumber().toString());
					jMessage.put(Constants.SWITCH_NAME, switchValidOrInvalid.getSwitchName().toString());
					jMessage.put(Constants.SWITCH_STATUS, switchValidOrInvalid.getSwitchStatus().toString());
					jMessage.put(Constants.DIMMER_STATUS, switchValidOrInvalid.getDimmerStatus().toString());
					jMessage.put(Constants.DIMMER_VALUE, switchValidOrInvalid.getDimmerValue().toString());
					jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
					jMessage.put(Constants.LOCK_STATUS, switchValidOrInvalid.getLockStatus().toString());
					jMessage.put(Constants.MESSAGE_FROM, changeLockStatusDTO.getMessageFrom().toString());
					if (userDetails.getImage() == null) {
						jMessage.put(Constants.USER_IMAGE, "");
					} else {
						jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");

					}
					jMessage.put(Constants.ROOM_ID, switchValidOrInvalid.getIotProduct().getRoom().getId().toString());
					jMessage.put(Constants.ROOM_NAME, switchValidOrInvalid.getIotProduct().getRoom().getRoomName().toString());

					Date date = new Date();
					jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
							+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

					logger.info(jMessage);
					//AESencrp.encrypt(jMessage.toString());
					
					MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
					refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
					// refreshBackMessage to mobile or to other thin end clients
					MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
					String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
					
					//child thread to publish on Internet 
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
							} catch (MqttException e) {
								e.printStackTrace();
							}
						}
					});

					return new ChangeLockStatusResponseDTO(
							switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
							switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
							switchValidOrInvalid.getSwitchNumber().toString(), changeLockStatusDTO.getLockStatus());

				} catch (MqttException me) {
					
					
					logger.error("Exception in Change Lock Status method. Switch Number: "+changeLockStatusDTO.getSwitchId()
							+"User ID: "+changeLockStatusDTO.getUserId());
			
					me.printStackTrace();
					
					switchRepository.save(switchValidOrInvalid);
					logger.info("Switch Details Saved "+switchValidOrInvalid);
					
					return new ChangeLockStatusResponseDTO(
							switchValidOrInvalid.getIotProduct().getRoom().getHome().getHomeName(),
							switchValidOrInvalid.getIotProduct().getIotProductNumber().toString(),
							switchValidOrInvalid.getSwitchNumber().toString(), changeLockStatusDTO.getLockStatus());

				}

			} else {
				
				logger.error("Exception in Change Lock Status method. Panel Not Found. Switch Number: "+changeLockStatusDTO.getSwitchId()
				+"User ID: "+changeLockStatusDTO.getUserId());

				logger.error("changeLockStatus(), Panel not found");
			}
		} catch (Exception e) {
			logger.error("Exception in Change Lock Status method. Switch Number: "+changeLockStatusDTO.getSwitchId()
			+"User ID: "+changeLockStatusDTO.getUserId());
			
			e.printStackTrace();
			
			return null;
		}
		
		return null;
	}

	// to hide status
	@SuppressWarnings("deprecation")
	@Override
	public ChangeHideStatusResponseDTO changeHidestatus(ChangeHideStatusDTO changeHideStatusDTO) {
		Activity addactivity = new Activity();
		JSONObject jMessage = new JSONObject();
		Panel panel = new Panel();
		
		try {

			String topic = "";
			Switch switch1 = switchRepository.findOne(changeHideStatusDTO.getSwitchId());
			logger.info("changeHidestatus(), Switch Details Fetched Switch Id: "+changeHideStatusDTO.getSwitchId());
			
			UserDetails userDetails = userDetailsRepository.findOne(changeHideStatusDTO.getUserId());
			logger.info("changeHidestatus(), User Details Fetched Switch Id: "+changeHideStatusDTO.getSwitchId());
			
			String hideStatus;
			if (changeHideStatusDTO.getHideStatus().equals("1")) {
				hideStatus = Constants.HIDE;
			} else {
				hideStatus = Constants.UNHIDE;
			}

			if (switch1 == null)
				return null;
			
			int Panelid = switch1.getIotProduct().getIotProductNumber();
			panel = panelRepository.findByPanelId(Panelid);
			if (panel != null) {
				topic = panel.getPanelName() + "/out";
			}

			if (changeHideStatusDTO.getHideStatus() != null){
				switch1.setHideStatus(changeHideStatusDTO.getHideStatus());
				if(changeHideStatusDTO.getHideStatus().equals("1")){
					switch1.setSwitchStatus("0");
				}
			}

			StringBuffer switchOnOffCommand = new StringBuffer("$[");
			switchOnOffCommand.append(switch1.getSwitchNumber());

			// command formation
			// switch number dimmer value lock
			if (switch1.getSwitchNumber() < 5) {
				switchOnOffCommand.append("0");
				switchOnOffCommand.append(switch1.getSwitchStatus());
				switchOnOffCommand.append(switch1.getLockStatus());

			} else {
				if (switch1.getSwitchStatus().equals("0")) {

					switchOnOffCommand.append("00");
				} else {

					int dimmerValue = Integer.parseInt(switch1.getDimmerValue());
					if (dimmerValue < 10) {
						switchOnOffCommand.append("0");
						switchOnOffCommand.append(switch1.getDimmerValue());

					} else {
						switchOnOffCommand.append(switch1.getDimmerValue());
					}

				}
				switchOnOffCommand.append(switch1.getLockStatus());
			}

			switchOnOffCommand.append("]");

			logger.info("Switch ON OFF While changeHidestatus Command\t" + switchOnOffCommand.toString());
			
			MqttMessage messageOff = new MqttMessage(switchOnOffCommand.toString().getBytes());
			messageOff.setQos(Constants.QoS_EXACTLY_ONCE);
			MqttlConnection.getObj().getLocalMqttConnection().publish(topic, messageOff);

			switchRepository.save(switch1);
			logger.info("Switch Details Saved. Switch ID: "+switch1.getId());
			
			Activity activity = new Activity();
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
				//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
			}
			
			activity.setMessage(
					userDetails.getFirstName().toString() + " " + hideStatus + " " + switch1.getSwitchName());
			activity.setMessageType("Switch_Hide_Unhide");
			activity.setRoomname(switch1.getIotProduct().getRoom().getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(changeHideStatusDTO.getMessageFrom().toString());
			addactivity = activityRepository.save(activity);

			jMessage.put(Constants.STATUS, "Switch_Hide_Unhide");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			String message = userDetails.getFirstName().toString() + " " + hideStatus + " "
					+ switch1.getSwitchName();
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
			jMessage.put(Constants.SWITCH_ID, switch1.getId().toString());
			jMessage.put(Constants.SWITCH_NUMBER, switch1.getSwitchNumber().toString());
			jMessage.put(Constants.SWITCH_NAME, switch1.getSwitchName().toString());
			jMessage.put(Constants.SWITCH_STATUS, switch1.getSwitchStatus().toString());
			jMessage.put(Constants.DIMMER_STATUS, switch1.getDimmerStatus().toString());
			jMessage.put(Constants.DIMMER_VALUE, switch1.getDimmerValue().toString());
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.HIDE_STATUS, switch1.getHideStatus().toString());
			jMessage.put(Constants.MESSAGE_FROM, changeHideStatusDTO.getMessageFrom().toString());
			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");

			}
			jMessage.put(Constants.ROOM_ID, switch1.getIotProduct().getRoom().getId().toString());
			jMessage.put(Constants.ROOM_NAME, switch1.getIotProduct().getRoom().getRoomName().toString());

			Date date = new Date();
			jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
					+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

			logger.info(jMessage);
			String jsonMessageEnc = jMessage.toString();//AESencrp.encrypt(jMessage.toString());
			
			MqttMessage refreshBackMessage = new MqttMessage(jsonMessageEnc.getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();

			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});
			
			return new ChangeHideStatusResponseDTO(switch1.getIotProduct().getRoom().getHome().getHomeName(),
					switch1.getIotProduct().getIotProductNumber().toString(), switch1.getSwitchNumber().toString(),
					changeHideStatusDTO.getHideStatus());
		} catch (Exception e) {
			logger.error("Exception in Change Hide Status method. Switch Number: "+changeHideStatusDTO.getSwitchId()
			+"User ID: "+changeHideStatusDTO.getUserId());
			e.printStackTrace();
			logger.error("Change Hide Status, Error Message: "+e.getStackTrace());
			return null;
		}

	}

	

	// Switch on/off all switch from selected room
	@SuppressWarnings("deprecation")
	@Override
	public List<ChangeStatusByRoomResponseDTO> changeStatusByRoom(ChangeStatusByRoomDTO changeStatusByRoomDTO) {

		String message = "";
		Activity activity = new Activity();
		Activity addactivity = new Activity();
		ChangeSwitchStatusResponseDTO changeSwitchStatusResponseDTO = null;
		
		JSONObject jMessage = new JSONObject();
		
		try {
			String switchStatus = "";
			UserDetails userDetails = userDetailsRepository.findOne(changeStatusByRoomDTO.getUserId());
			
			logger.info("changeStatusByRoom(), User Details Fetched User Id:"+changeStatusByRoomDTO.getUserId());
			
			Room room = roomRepository.findOne(changeStatusByRoomDTO.getRoomId());
			
			logger.info("changeStatusByRoom(), Room Details Fetched Room Id:"+changeStatusByRoomDTO.getRoomId());
			
			String fetchSwitchStatus = "0";
			List<ChangeStatusByRoomResponseDTO> list = new ArrayList<ChangeStatusByRoomResponseDTO>();
			
			if (changeStatusByRoomDTO.getOnoffstatus().equals("1")) {
				fetchSwitchStatus = "0";
			} else {
				fetchSwitchStatus = "1";
			}
			
			Switch arrRoomSwitch[] = switchRepository.getSwichbyRoomId(changeStatusByRoomDTO.getRoomId(),
					fetchSwitchStatus);
			
			logger.info("changeStatusByRoom(), Switch Details Fetched by Room Id:"+changeStatusByRoomDTO.getRoomId());
			
			for (int i = 0; i < arrRoomSwitch.length; i++) {
				
				if(arrRoomSwitch[i].getHideStatus() == "0" || arrRoomSwitch[i].getHideStatus().equals("0")){
					ChangeSwitchStatusDTO changeSwitchStatusdto = new ChangeSwitchStatusDTO();
					changeSwitchStatusdto.setSwitchId(arrRoomSwitch[i].getId());
					changeSwitchStatusdto.setSwitchStatus(changeStatusByRoomDTO.getOnoffstatus());
					changeSwitchStatusdto.setUserid(changeStatusByRoomDTO.getUserId());
					changeSwitchStatusdto.setDimmerValue(arrRoomSwitch[i].getDimmerValue());
					
					//do not do action on curtain
					if(arrRoomSwitch[i].getDimmerStatus().equals(Constants.DIMMER_STATUS_FOR_CURTAIN)){
						continue;
					}else{
						changeSwitchStatusResponseDTO = changeSwitchStatusByRoom(changeSwitchStatusdto);
					}
					
					String changeStatus = "0";
					
					if (changeSwitchStatusResponseDTO.getState().equalsIgnoreCase("Success")) {
						changeStatus = "1";
						switchStatus = Constants.SWITCH_STATUS_ON;		
					}else{
						switchStatus = Constants.SWITCH_STATUS_OFF;
					}
					
					list.add(new ChangeStatusByRoomResponseDTO(arrRoomSwitch[i].getId().toString(), changeStatus));
				}
			} // for
			if (userDetails.getImage() == null) {
				activity.setImage("");
			} else {
				activity.setImage(userDetails.getId()+ ".png");
			}
			if(changeStatusByRoomDTO.getOnoffstatus().equals("1"))
			{	
				activity.setMessage(userDetails.getFirstName().toString() + TURN + Constants.SWITCH_STATUS_ON
					+ " all appliances");
			}
			else{
				activity.setMessage(userDetails.getFirstName().toString() + TURN + Constants.SWITCH_STATUS_OFF
						+ " all appliances");				
			}
			activity.setMessageType("changeStatusByRoom");
			activity.setRoomname(room.getRoomName().toString());
			activity.setCreated_date(dateFormat.format(new Date()).toString());
			activity.setMessageFrom(changeStatusByRoomDTO.getMessageFrom());
			
			addactivity = activityRepository.save(activity);
			logger.info("Change Status by Room method, Switch Details Fetched by Room Id: "+changeStatusByRoomDTO.getRoomId());
			
			jMessage.put(Constants.STATUS, "changeStatusByRoom");
			jMessage.put(Constants.USER_ID, userDetails.getId().toString());
			jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
			
			if(changeStatusByRoomDTO.getOnoffstatus().equals("1"))
			{
				message = userDetails.getFirstName().toString() + TURN + Constants.SWITCH_STATUS_ON
					+ " all appliances";
			}else{
				message = userDetails.getFirstName().toString() + TURN + Constants.SWITCH_STATUS_OFF
						+ " all appliances";
			}
			
			jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1));
			jMessage.put(Constants.SWITCH_ID, " ");
			jMessage.put(Constants.SWITCH_NUMBER, " ");
			jMessage.put(Constants.SWITCH_NAME, " ");
			jMessage.put(Constants.SWITCH_STATUS, changeStatusByRoomDTO.getOnoffstatus());
			jMessage.put(Constants.DIMMER_STATUS, " ");
			jMessage.put(Constants.DIMMER_VALUE, " ");
			jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
			jMessage.put(Constants.LOCK_STATUS, " ");
			jMessage.put(Constants.MESSAGE_FROM, fetchSwitchStatus);

			if (userDetails.getImage() == null) {
				jMessage.put(Constants.USER_IMAGE, "");
			} else {
				jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
			}
			jMessage.put(Constants.ROOM_ID, room.getId().toString());
			jMessage.put(Constants.ROOM_NAME, room.getRoomName().toString());

			Date date = new Date();
			jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
					+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());

			logger.info(jMessage);
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage to mobile or to other thin end clients
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
			String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
			
			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
						logger.info("changeStatusByRoom() Internet MQTT Connection Successful");
						
					} catch (MqttException e) {
						logger.error("changeStatusByRoom() Exception in Internet Mqtt Connection"+e.getMessage());
						e.printStackTrace();
					}
				}
			});

			return list;
		} catch (Exception e) {
			logger.error("Exception in changeStatusByRoom() for "+e.getStackTrace());
			return null;
		}
	}

	

	public boolean saveSwitchCommand(Command command, Panel panel) {
		commandRepository.save(command);
		return false;
	}
	

	@SuppressWarnings("deprecation")
	@Override
	public void processPhysicalSwitch(byte[] b) {
		try {
			StringBuffer topicName = new StringBuffer();
			String switchStatus;
			String dimmerValue = "";
			for (int i = 3; i < b.length; i++)
				topicName.append((char) b[i]);

			if (b[1] == 42) {
				if (b[0] == 5 || b[0] == 6) {
					System.out.println("Dimmer Switch no " + b[0] + " is off (" + topicName + ")");
					switchStatus = "0";
				} else {
					System.out.println("Switch no " + b[0] + " is off (" + topicName + ")");
					switchStatus = "0";
				}

			} else if (b[1] == 1) {
				switchStatus = "1";
				System.out.println("Switch no " + b[0] + " is on (" + topicName + ")");
			} else {
				switchStatus = "1";
				System.out.println("Dimmer Switch no " + b[0] + " is on and value is " + b[1] + " (" + topicName + ")");
				dimmerValue = b[1] + "";

			}

			// First get switch id

			int switch_no = b[0];
			System.out.println(topicName.toString());

			Panel panel = new Panel();

			// list<Panel> panel=panelRepository1.getBypanelName("panel1");
			Panel allPanel[] = panelRepository.getPanel();
			for (int k = 0; k < allPanel.length; k++) {

				if (allPanel[k].getPanelName().equals(topicName.toString())) {
					panel = allPanel[k];
					break;
				}
			}
			
			logger.info("Panel ID: "+panel.getPanelId());

			IOTProduct iotPRODUCTID = iOTProductRepository.findByiotProductNumber(panel.getPanelId());
			Switch getSwitch = switchRepository.checkSwitchAlreadyExist(switch_no, iotPRODUCTID.getId());

			// Publish Data to refreshBack
			JSONObject jMessage = new JSONObject();

			jMessage.put(Constants.STATUS, "Switch_ON_OFF");
			jMessage.put(Constants.USER_ID, "");
			jMessage.put(Constants.USER_NAME, "");
			jMessage.put(Constants.MESSAGE, "Physically switch on");
			jMessage.put(Constants.SWITCH_ID, getSwitch.getId());
			jMessage.put("switchnumbere", getSwitch.getSwitchNumber());
			jMessage.put(Constants.SWITCH_STATUS, getSwitch.getSwitchStatus());
			jMessage.put(Constants.DIMMER_STATUS, getSwitch.getDimmerStatus());
			jMessage.put(Constants.DIMMER_VALUE, getSwitch.getDimmerValue());
			jMessage.put(Constants.USER_IMAGE, "");
			jMessage.put(Constants.ROOM_ID, iotPRODUCTID.getRoom().getId());

			Date dateCurrent = new Date();
			jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth()) + " "
					+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());

			logger.info(jMessage);

			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			// refreshBackMessage.se message
			MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);

			//child thread to publish on Internet 
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish("refreshBackInt", refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});

			// Insert db operation
			getSwitch.setSwitchStatus(switchStatus);
			//getSwitch.setDimmerStatus(switchStatus);
			getSwitch.setDimmerValue(dimmerValue);
			switchRepository.save(getSwitch);
			logger.info("Switch Details Saved Successfully"+getSwitch);
			
		} catch (Exception ex) {
			logger.error("processPhysicalSwitch(), "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	
	@Override
	public List<RoomAndSwitchResponseDTO> getSwitchListByUserId(Long userId,String messageFrom) {
		JSONObject jMessage = new JSONObject();		
		
		try
		{
			
		List<SwitchListByRoomDTO> switchList = null;
		
		List<RoomAndSwitchResponseDTO> roomAndSwitchResponseDTOList = new ArrayList<RoomAndSwitchResponseDTO>();
		
		UserDetails userDetails = userDetailsRepository.findOne(userId);
		
		List<RoomListByUserDTO> roomList = new ArrayList<RoomListByUserDTO>();
		
		if(userDetails != null)
		{
			for(Room room : userDetails.getHome().getRoomList())
			{
				
				RoomType roomType = roomTypeRepository.findOne(Long.parseLong(room.getRoomType()));
				
				roomList.add(new RoomListByUserDTO(room.getId(), room.getRoomName(), room.getRoomType(), roomType.getRoomImage(),room.getRoomImageId().toString(),room.getHideStatus()));
			
				switchList = new ArrayList<SwitchListByRoomDTO>();
				
				RoomAndSwitchResponseDTO roomAndSwitchResponseDTO = new RoomAndSwitchResponseDTO();
				
				for(RoomListByUserDTO roomListDTO : roomList ){
					switchList = getSwitchListByRoom(roomListDTO.getId());
					
					roomAndSwitchResponseDTO.setRoomId(roomListDTO.getId().toString());
					roomAndSwitchResponseDTO.setRoomName(roomListDTO.getRoomName());
					roomAndSwitchResponseDTO.setRoomImageId(roomListDTO.getRoomImageId());
					roomAndSwitchResponseDTO.setRoomHideStatus(roomListDTO.getHideStatus());
					roomAndSwitchResponseDTO.setSwitchList(switchList);	
				}
				
				roomAndSwitchResponseDTOList.add(roomAndSwitchResponseDTO);
			}
			if(messageFrom.equalsIgnoreCase("Internet"))
			{
				
				jMessage.put(Constants.STATUS, "Room_Switch_List");
				jMessage.put(Constants.USER_ID, userDetails.getId().toString());
				jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
				jMessage.put(Constants.MESSAGE, " ");
				
				if(userDetails.getImage() == null)
				{
					jMessage.put(Constants.USER_IMAGE, "");
				}
				
				else
				{
					jMessage.put(Constants.USER_IMAGE, userDetails.getId()+".png");
				}
				
				jMessage.put(Constants.SWITCH_AND_ROOM_LIST, roomAndSwitchResponseDTOList);
				jMessage.put(Constants.IS_ARMED, userDetails.getHome().getIsArmed());
				
				
				Date dateCurrent = new Date();
				jMessage.put(Constants.TIME, dateCurrent.getDate() + " " + Utility.theMonth(dateCurrent.getMonth())+" "+ Utility.getTime(dateCurrent.getHours() + ":" + dateCurrent.getMinutes()).toString());
				jMessage.put(Constants.MESSAGE_FROM, "Internet");
		
				logger.info("JSON Message"+jMessage);
				
				MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
				refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
				// refreshBackMessage to mobile or to other thin end clients
				// MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
				String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
				
				//child thread to publish on Internet 
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				});
			}
		}else{
			
			logger.info("getSwitchListByUserId Method User Details not Found");
		}
		
		return roomAndSwitchResponseDTOList;
		
		}
		catch(Exception ex)
		{
			logger.error("Exception Message: "+ex.getMessage()+" Cause: "+ex.getStackTrace());
			ex.printStackTrace();
			return null;
		}
	}
	

	@Override
	public Switch homeUnlock(SwitchIdDTO switchIdDTO) {

	//	String switchStatus = null;
		
		JSONObject jMessage = new JSONObject();

		Activity addactivity = new Activity();		
		
		Activity activity = new Activity();
		
		UserDetails userDetails = userDetailsRepository.findOne(switchIdDTO.getUserId());
		
		Switch switchById = switchRepository.findOne(switchIdDTO.getSwitchId());
		
		Switch switchLockByDimmerStatus[] = switchRepository.getHomeLock(Constants.DIMMER_STATUS_FOR_HOME_LOCK, Constants.DIMMER_STATUS_FOR_LOCK);
	
		//Switch switchNormalLockByDimmerStatus[] = switchRepository.getHomeLock(Constants.DIMMER_STATUS_FOR_LOCK);
		
		for(int i = 0; i < switchLockByDimmerStatus.length; i++){
			
			if(switchLockByDimmerStatus[i].getDimmerStatus().equals(switchById.getDimmerStatus()) || switchLockByDimmerStatus[i].getDimmerStatus().equals(switchById.getDimmerStatus()))
			{
				int panelId = switchById.getIotProduct().getIotProductNumber();
				panel = panelRepository.findByPanelId(panelId);
	
				logger.info("homeUnlock(), switch On Off Command Fetched Panel Details Panel Id: "+ panelId);
				
				if (panel != null) {
					
					//child thread to publish on Internet 
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							unlockHomeCommand(switchById, panel);			
						}
					});
	
					if (userDetails.getImage() == null) {
						activity.setImage("");
					} else {
						activity.setImage(userDetails.getId()+ ".png");
					}
					
					activity.setMessage(userDetails.getFirstName().toString() +" unlocked the"+ switchById.getSwitchName());
					activity.setMessageType(Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
					activity.setRoomname("Home");
					activity.setCreated_date(dateFormat.format(new Date()).toString());
					activity.setMessageFrom(switchIdDTO.getMessageFrom());
					
					
					addactivity = activityRepository.save(activity);
					logger.info("homeUnlock(), Activity added successfully. Activity Object: "+activity);
					
					try{
					jMessage.put(Constants.STATUS, Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
					jMessage.put(Constants.USER_ID, userDetails.getId().toString());
					jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
	
					String message = userDetails.getFirstName().toString() + " unlocked the "+ switchById.getSwitchName();
					
					jMessage.put(Constants.MESSAGE, message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase());
					jMessage.put(Constants.SWITCH_ID, switchById.getId().toString());
					jMessage.put(Constants.SWITCH_NUMBER, switchById.getSwitchNumber().toString());
					jMessage.put(Constants.SWITCH_NAME, switchById.getSwitchName().toString());
					jMessage.put(Constants.SWITCH_STATUS, switchById.getSwitchStatus().toString());
					jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
					jMessage.put(Constants.LOCK_STATUS, switchById.getLockStatus().toString());
					jMessage.put(Constants.LOCK_CODE, switchById.getLockCode());				
					jMessage.put(Constants.MESSAGE_FROM, switchIdDTO.getMessageFrom());
					
					if (userDetails.getImage() == null) {
						jMessage.put(Constants.USER_IMAGE, "");
					} else {
						jMessage.put(Constants.USER_IMAGE, userDetails.getId() + ".png");
	
					}
					jMessage.put(Constants.ROOM_ID, switchById.getIotProduct().getRoom().getId().toString());
					jMessage.put(Constants.ROOM_NAME, switchById.getIotProduct().getRoom().getRoomName().toString());
	
					Date date = new Date();
					jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
							+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
	
					logger.debug("Switch ON or OFF JSON Message :"+jMessage);
					
					// refreshBackMessage to mobile or to other thin end clients
					MqttMessage refreshMessage = new MqttMessage(jMessage.toString().getBytes());
				    
					refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
					
				    MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshMessage);
					
				    String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
					
					//child thread to publish on Internet
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshMessage);
							} catch (MqttException e) {
								
								e.printStackTrace();
							}
						}
					});
					
					logger.debug("MQTT Message Local and Internet Published Successfully. Topic: "+publicTopic);
					
					
					switchRepository.save(switchById);
					
					logger.debug("Switch ON or OFF Success for Switch Number: "+ switchById.getSwitchNumber().toString()
							+"Panel ID: "+switchById.getIotProduct().getIotProductNumber());
					
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Exception Message: "+e.getMessage()+" Cause:"+e.getCause());
					}
					
					return new Switch(switchById.getId(), switchById.getSwitchIdentifier(), switchById.getSwitchNumber(),
									  switchById.getSwitchType(), switchById.getDimmerStatus(), switchById.getDimmerValue(),
									  switchById.getSwitchStatus(), switchById.getIotProduct(), switchById.getLockStatus(),				
									  switchById.getHideStatus(), switchById.getLockCode());
				}
			}	
		}
		
		return null;
	}


	@Override
	public ChangeSwitchStatusResponseDTO changeGenieColor(ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		Activity addactivity = new Activity();
		Activity activity = new Activity();
		
		UserDetails userDetails = userDetailsRepository.findOne(changeSwitchStatusDTO.getUserid());
		
		StringBuffer switchChangeColorCommand;
		
		if(userDetails != null){
			Switch switchObj = switchRepository.findOne(changeSwitchStatusDTO.getSwitchId());
			
			if(switchObj.getHideStatus().equals(Constants.NOT_HIDDEN)){
				
				switchObj.setSwitchStatus(changeSwitchStatusDTO.getSwitchStatus());
				
				int iotProductId = Integer.parseInt(switchObj.getIotProduct().getIotProductNumber().toString());
				
				if(iotProductId > 0 ){
					Panel panelObj = panelRepository.findByPanelId(iotProductId);
					
					try{
					String panelName = panelObj.getPanelName();
					
						if(panelName != null){
							
								String topic = panelName + "/out";
							
								//switchChangeColorCommand = new StringBuffer("$"+"0,0,255,");
								switchChangeColorCommand = new StringBuffer(changeSwitchStatusDTO.getGenieColor());
	//							switchChangeColorCommand.append();
								MqttMessage message = new MqttMessage(switchChangeColorCommand.toString().getBytes());
								message.setQos(Constants.QoS_EXACTLY_ONCE);
								MqttlConnection.getObj().getLocalMqttConnection().publish(topic, message);
		
								Command commandSave = new Command(String.valueOf(switchChangeColorCommand),
										dateFormat.format(new Date()).toString());
								
								//saveSwitchCommand(commandSave, panel);
								
								switchRepository.save(switchObj);
		
								logger.info("Change Genie Color Method, Switch Details Saved Successfully Switch Id: "+changeSwitchStatusDTO.getSwitchId());
								
								if (userDetails.getImage() == null) {
									activity.setImage("");
								} else {
									activity.setImage(userDetails.getId()+ ".png");
									//activity.setImage(IMAGE_LOCATION + "/" + userDetails.getId() + ".png");
		
								}
								activity.setMessage(userDetails.getFirstName().toString() + " changed color "
										+ switchObj.getSwitchName());
								activity.setMessageType(Constants.GENIE_COLOR_STATUS);
								activity.setRoomname(switchObj.getIotProduct().getRoom().getRoomName().toString());
								activity.setCreated_date(dateFormat.format(new Date()).toString());
								activity.setMessageFrom(changeSwitchStatusDTO.getMessageFrom().toString());
						
								addactivity = activityRepository.save(activity);
								logger.info("Change Genie Color Method, Activity Details Saved Successfully. Switch Id: "+changeSwitchStatusDTO.getUserid());
								
		
								JSONObject jMessage = new JSONObject();
								jMessage.put(Constants.STATUS, Constants.SWITCH_ON_OFF_MESSAGE_TYPE);
								jMessage.put(Constants.USER_ID, userDetails.getId().toString());
								jMessage.put(Constants.USER_NAME, userDetails.getFirstName().toString());
								
								String message2 = userDetails.getFirstName().toString() + " changed color "
										+ switchObj.getSwitchName();
								
								jMessage.put(Constants.MESSAGE, message2.substring(0, 1).toUpperCase() + message2.substring(1).toLowerCase());
								jMessage.put(Constants.SWITCH_ID, switchObj.getId().toString());
								jMessage.put(Constants.SWITCH_NUMBER, switchObj.getSwitchNumber().toString());
								jMessage.put(Constants.SWITCH_NAME, switchObj.getSwitchName().toString());
								jMessage.put(Constants.SWITCH_STATUS, changeSwitchStatusDTO.getSwitchStatus().toString());
								jMessage.put(Constants.DIMMER_STATUS, switchObj.getDimmerStatus().toString());
								jMessage.put(Constants.DIMMER_VALUE, switchObj.getDimmerValue().toString());	
								jMessage.put(Constants.ACTIVITY_ID, addactivity.getId().toString());
								jMessage.put(Constants.LOCK_STATUS, switchObj.getLockStatus().toString());
								jMessage.put(Constants.GENIE_COLOR, changeSwitchStatusDTO.getGenieColor().toString());
								
								jMessage.put(Constants.MESSAGE_FROM, changeSwitchStatusDTO.getMessageFrom().toString());
								if (userDetails.getImage() == null) {
									jMessage.put(Constants.USER_IMAGE, "");
								} else {
									jMessage.put(Constants.USER_IMAGE,  userDetails.getId() + ".png");
		
								}
								jMessage.put(Constants.ROOM_ID, switchObj.getIotProduct().getRoom().getId().toString());
								jMessage.put(Constants.ROOM_NAME, switchObj.getIotProduct().getRoom().getRoomName().toString());
		
								Date date = new Date();
								jMessage.put(Constants.TIME, date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
										+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString());
		
								logger.info("Change Genie Color Method, JSON Message Created: "+jMessage);
								//AESencrp.encrypt(jMessage.toString());
								
								MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
								refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
								// refreshBackMessage to mobile or to other thin end clients
								MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
								String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
								
								//child thread to publish on Internet 
								SwingUtilities.invokeLater(new Runnable() {
									@Override
									public void run() {
										try {
											MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
										} catch (MqttException e) {
											e.printStackTrace();
										}
									}
								});
		
								return new ChangeSwitchStatusResponseDTO(switchObj.getId().toString(), userDetails.getHome().getId().toString(), 
										switchObj.getIotProduct().getIotProductNumber().toString(), 
										switchObj.getSwitchNumber().toString(), 
										switchObj.getLockStatus(),
										switchObj.getHideStatus(),
										changeSwitchStatusDTO.getSwitchStatus());
						
						}
					}catch(Exception e){
						logger.error("Change Genie Color Method, Exception meesage: "+e.getMessage());
						e.printStackTrace();
					}
					
					}
				}
			}else{
				//TODO userDetails not found
				logger.error("Change Genie Color Method, UserDetails not found exception message: ");
				return null;
			}
		
		return null;
	}


	@SuppressWarnings("deprecation")
	@Override
	public Boolean updateSwitchStatus(String msg1) {
		// TODO Auto-generated method stub
		
		Long id=Long (""+msg1.charAt(2));
		System.out.println("+msg1.charAt(3)"+msg1.charAt(3));

		System.out.println("+msg1.charAt(2)"+msg1.charAt(2));

		Switch switchValidOrInvalid =  null;
		switchValidOrInvalid.setId(id);
		switchValidOrInvalid.setSwitchStatus(""+msg1.charAt(3));
		switchValidOrInvalid.setDimmerValue(""+msg1.charAt(4)+""+msg1.charAt(5));
		switchRepository.save(switchValidOrInvalid);

		return true;
	}


	private Long Long(String string) {
		// TODO Auto-generated method stub
		return null;
	}
}