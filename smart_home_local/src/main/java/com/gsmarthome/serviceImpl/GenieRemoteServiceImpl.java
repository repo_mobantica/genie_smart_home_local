package com.gsmarthome.serviceImpl;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.RemoteIRRequestDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.IGenieRemoteService;

import MqttReadDataPaho.MqttlConnection;


@Component
public class GenieRemoteServiceImpl implements IGenieRemoteService {

	static Logger logger = Logger.getLogger(GenieRemoteServiceImpl.class.getName());

	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Override
	public void commandIRFire(RemoteIRRequestDTO remoteIRReqDTO) {
		// TODO Auto-generated method stub
		UserDetails userDetails = userDetailsRepository.findOne(remoteIRReqDTO.getUserId());
		
		if(userDetails != null){
		
			String topicId = remoteIRReqDTO.getTowerId();
			try {
			
				String message = "#"+remoteIRReqDTO.getIrRemoteinfo()+"^";
			
				logger.info("command IR Fire method, JSON Message :"+message);
				
				MqttMessage refreshMessage = new MqttMessage(message.toString().getBytes());
			    
				refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.TOPIC_TOWER+topicId+"/out", refreshMessage);
			} catch (MqttPersistenceException e1) {
				logger.error("command IR Fire method, Mqtt Persistence Exception User Id: "+remoteIRReqDTO.getUserId());
				e1.printStackTrace();
			} catch (MqttException e1) {
				logger.error("command IR Fire method, MqttException User Id: "+remoteIRReqDTO.getUserId());
				e1.printStackTrace();
			}
			
		}else{
			logger.error("learnIRCode method, user Details not found");
		}
	}

	@Override
	public void learnIRCode(RemoteIRRequestDTO remoteIRReqDTO) {
		
		UserDetails userDetails = userDetailsRepository.findOne(remoteIRReqDTO.getUserId());
		
		if(userDetails != null){
		
			String topicId = remoteIRReqDTO.getTowerId();
			try {
			
				String message = "&"+remoteIRReqDTO.getButtonId();
			
				logger.info("learn IR Code method, JSON Message :"+message);
				
				MqttMessage refreshMessage = new MqttMessage(message.toString().getBytes());
			    
				refreshMessage.setQos(Constants.QoS_EXACTLY_ONCE);
			
				MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.TOPIC_TOWER+topicId+"/out", refreshMessage);
			} catch (MqttPersistenceException e1) {
				logger.error("learn ir Code method, Mqtt Persistence Exception User Id: "+remoteIRReqDTO.getUserId());
				e1.printStackTrace();
			} catch (MqttException e1) {
				logger.error("learn ir Code method, MqttException User Id: "+remoteIRReqDTO.getUserId());
				e1.printStackTrace();
			}
			
		}else{
			logger.error("learnIRCode method, user Details not found");
		}
		
	}
	
}
