package com.gsmarthome.serviceImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.dto.NotificationHistoryDTO;
import com.gsmarthome.dto.NotificationHistoryResponseDTO;
import com.gsmarthome.entity.Activity;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.Notification;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.NotificationRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.util.AESencrp;
import com.gsmarthome.util.FCMNotifier;
import com.gsmarthome.util.Utility;

import MqttReadDataPaho.MqttlConnection;

@Component
public class NotificationServiceImpl implements NotificationService {

	static Logger logger = Logger.getLogger(NotificationServiceImpl.class.getName());

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	ActivityRepository activityRepository;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Override
	public List<NotificationHistoryResponseDTO> getNotificationHistory(NotificationHistoryDTO notificationHistoryDTO) {
		List<NotificationHistoryResponseDTO> listNotificationHistresoDTO = new ArrayList<NotificationHistoryResponseDTO>();
		Notification arrNotification[] = notificationRepository
				.findfityUnreadNotification(notificationHistoryDTO.getUserId());

		for (int i = 0; i < arrNotification.length; i++) {
			if (i == 50)
				break;
			listNotificationHistresoDTO.add(new NotificationHistoryResponseDTO(arrNotification[i].getMessage()));
			arrNotification[i].setIsRead(1);
			notificationRepository.save(arrNotification[i]);
		}

		return listNotificationHistresoDTO;
	}

	// to send push notification
	@Override
	public boolean sendNotificationMessage(UserDetails[] arrUserDetails, String notifyMessage) {

		Notification notification = new Notification();
		Date todaydate = new Date();
		FCMNotifier fcmNotifier = null;

		Boolean status = false;

		try {

			String serverKey = Constants.NOTIFICATION_SERVER_KEY;

			for (int i = 0; i < arrUserDetails.length; i++) {

				// check if user is verified user details
				if (arrUserDetails[i].getIsEmailVerified()) {
					fcmNotifier = new FCMNotifier(serverKey, arrUserDetails[i].getDeviceId());
				}

				String title = null;
				String body = "";

				title = "Notify";
				body += notifyMessage;

				if (fcmNotifier != null) {
					fcmNotifier.setNotification(title, body);
					fcmNotifier.setMessage(title, "Something");

					status = fcmNotifier.sendNotification();
				} else {
					logger.info("sendNotificationMessage Method, fcmNotifier in null");
				}

				logger.info("Notification" + status);
				notification.setMessage(notifyMessage);
				notification.setSwitchId(1l);
				notification.setRoomId(11l);
				notification.setStatus(status + "");
				notification.setUserId(arrUserDetails[i].getId());
				notification.setIsRead(0);
				notification.setSenderImage(arrUserDetails[i].getId() + ".png");
				notification.setCreated_date(todaydate);
			}

			notificationRepository.save(notification);

			return status;

		} catch (Exception e) {
			logger.error("Exception in Send Notification Message Method, Exception Message: " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean sendNotificationMessage(UserDetails userDetails, String notifyMessage) {
		Notification notification = new Notification();
		Date todaydate = new Date();
		FCMNotifier fcmNotifier = null;

		Boolean status = false;

		try {

			String serverKey = Constants.NOTIFICATION_SERVER_KEY;

			if (userDetails != null) {
				fcmNotifier = new FCMNotifier(serverKey, userDetails.getDeviceId());
			} else {

				System.out.println(
						"in here DANGER __________________________________________________________________________________________________");

				String title = null;
				String body = "";

				title = "Notify";
				body += "jkdn c ckjndsc ncsdcsdn cls____________________________________";

				if (fcmNotifier != null) {
					fcmNotifier.setNotification(title, body);
					fcmNotifier.setMessage(title, "Something");

					status = fcmNotifier.sendNotification();
				}

				logger.error("User Details not found");
			}

			String title = null;
			String body = "";

			title = "Notify";
			body += notifyMessage;

			if (fcmNotifier != null) {
				fcmNotifier.setNotification(title, body);
				fcmNotifier.setMessage(title, "Something");

				status = fcmNotifier.sendNotification();
			}

			logger.info("sendNotificationMessage Notification Status: " + status);
			notification.setMessage(notifyMessage);
			notification.setSwitchId(1l);
			notification.setRoomId(11l);
			notification.setStatus(status + "");
			notification.setUserId(userDetails.getId());
			notification.setIsRead(0);
			notification.setSenderImage(userDetails.getId() + ".png");
			notification.setCreated_date(todaydate);

			notificationRepository.save(notification);

			return status;

		} catch (Exception e) {
			logger.error("Exception in Send Notification Message Method, Exception Message: " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<ActivityResponseDTO> getActivityList(ActivityDTO activityDTO) {

		JSONObject jMessage = new JSONObject();

		final long homeid = activityDTO.getHomeId();
		Home home = homeRepository.findOne(homeid);

		UserDetails userDetails[] = userDetailsRepository.findByHome(home);

		if (home != null) {
			Page<Integer> page = activityRepository.findLatestId(new PageRequest(0, 20));

			Activity activity[] = activityRepository.selectByExcludedId(page.getContent());
			// Activity activityByHome[] =
			// activityRepository.selectByHomeId(homeid);

			List<ActivityResponseDTO> listActivityResp = new ArrayList<ActivityResponseDTO>();
			for (int i = 0; i < activity.length; i++) {
				if (i == 20)
					break;
				ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();
				try {
					Date date;
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
					date = format.parse(activity[i].getCreated_date().toString());
					activityResponseDTO.setCreated_date((date.getDate() + " " + Utility.theMonth(date.getMonth()) + " "
							+ Utility.getTime(date.getHours() + ":" + date.getMinutes()).toString()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				/*
				 * if(activity[i].getImage().length() > 0){
				 * System.out.println("Image Length"+activity[i].getImage().
				 * length()+"Image Sub String"+activity[i].getImage().substring(
				 * 40,47));
				 * activityResponseDTO.setImage(activity[i].getImage().substring
				 * (40, 47)); }else{
				 */
				activityResponseDTO.setImage(activity[i].getImage());
				// }

				activityResponseDTO.setId(activity[i].getId());

				activityResponseDTO.setMessage(activity[i].getMessage());
				activityResponseDTO.setMessageType(activity[i].getMessageType());
				activityResponseDTO.setRoomname(activity[i].getRoomname());
				listActivityResp.add(activityResponseDTO);

			}

			try {
				jMessage.put(Constants.ACTIVITY_LIST, listActivityResp);
				jMessage.put(Constants.STATUS, "ACTIVITY_LIST");
				jMessage.put(Constants.USER_ID, userDetails[0].getId());
				jMessage.put(Constants.MESSAGE_FROM, Constants.MESSAGE_FROM_INTERNET);

			} catch (JSONException e2) {
				logger.error("Exception in get activity list method, json message creation");
				e2.printStackTrace();
			}

			logger.info("get activity list method, JSON Message: " + jMessage);
			// refreshBackMessage to mobile or to other thin end clients
			MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
			refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);

			// MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK,
			// refreshBackMessage);

			logger.info("get activity list method, MQTT Local Publish Called");

			String publicTopic = "refreshIntBackGenieHomeId_" + activityDTO.getHomeId().toString();

			// child thread to publish on Internet
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}
			});

			return listActivityResp;
		}

		return null;
	}

	@Override
	public void sendNotificationMessage(String notifyMessage) {
		FCMNotifier fcmNotifier = null;
		Boolean status = false;
		try {
			String serverKey = Constants.NOTIFICATION_SERVER_KEY;
			fcmNotifier = new FCMNotifier(serverKey, "4");
			String title = null;
			String body = "";

			title = "Notify";
			body += notifyMessage;

			if (fcmNotifier != null) {
				fcmNotifier.setNotification(title, body);
				fcmNotifier.setMessage(title, "Something");

				status = fcmNotifier.sendNotification();
			}

		} catch (Exception e) {
			logger.error("Exception in Send Notification Message Method, Exception Message: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
