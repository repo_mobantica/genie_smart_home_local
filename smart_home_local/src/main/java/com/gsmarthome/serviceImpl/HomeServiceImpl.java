package com.gsmarthome.serviceImpl;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.dto.HomeLockCodeResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.HomeService;
import com.gsmarthome.service.NotificationService;

import MqttReadDataPaho.MqttlConnection;

@Component
public class HomeServiceImpl implements HomeService {

	static Logger logger = Logger.getLogger(HomeServiceImpl.class.getName());

	@Autowired
	HomeRepository homeRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	NotificationService notificationService;
	
	@Override
	public HomeArmANDBlockResponseDTO armHome(HomeDTO homeDTO) throws Exception{
		
		int result = 0;
		
		JSONObject jMessage = new JSONObject();
		
		HomeArmANDBlockResponseDTO homeArmResponseDTO = new HomeArmANDBlockResponseDTO();

		UserDetails userDetails = userDetailsRepository.findOne(homeDTO.getUserId());
		
		if(userDetails == null)
			throw new Exception("User Details not found");
		
		UserDetails arrUserDetails[] = userDetailsRepository.findByHome(userDetails.getHome());
		
		userDetails.setDeviceType("Android");
		
		if(homeDTO.getIsArmed().equals(Constants.HOME_ARMED)){
			result = homeRepository.updateHomeArmed(userDetails.getHome().getId(), Constants.HOME_ARMED);
			logger.info("Home is Armed now");
			if(result == 1){ //1 equals successfully saved
				
				notificationService.sendNotificationMessage(arrUserDetails, userDetails.getFirstName() + " " + userDetails.getLastName() +Constants.HOME_ARMED_MESSAGE);
				
				homeArmResponseDTO.setHomeId(userDetails.getHome().getId().toString());
				homeArmResponseDTO.setHomeName(userDetails.getHome().getHomeName());
				homeArmResponseDTO.setIsArm(homeDTO.getIsArmed().toString());
				homeArmResponseDTO.setStatus(Constants.SUCCESS_MSG);

			}
		}else{
			result = homeRepository.updateHomeArmed(userDetails.getHome().getId(), Constants.HOME_NOT_ARMED);	
			logger.info("Home is UnArmed now");
			
			if(result == 1){ //1 equals successfully saved
				
				notificationService.sendNotificationMessage(arrUserDetails,userDetails.getFirstName() + " " + userDetails.getLastName() +Constants.HOME_DISARMED_MESSAGE);
				
				homeArmResponseDTO.setHomeId(userDetails.getHome().getId().toString());
				homeArmResponseDTO.setHomeName(userDetails.getHome().getHomeName());
				homeArmResponseDTO.setIsArm(homeDTO.getIsArmed().toString());
				homeArmResponseDTO.setStatus(Constants.SUCCESS_MSG);

			}
		}
		
		jMessage.put(Constants.STATUS, "ARMED");
		jMessage.put(Constants.IS_ARMED, homeDTO.getIsArmed().toString());
		jMessage.put(Constants.USER_ID, homeDTO.getUserId().toString());
		jMessage.put(Constants.MESSAGE_FROM, homeDTO.getMessageFrom());
		
		MqttMessage refreshBackMessage = new MqttMessage(jMessage.toString().getBytes());
		refreshBackMessage.setQos(Constants.QoS_EXACTLY_ONCE);
		
		// refreshBackMessage to mobile or to other thin end clients
		MqttlConnection.getObj().getLocalMqttConnection().publish(Constants.REFRESH_BACK, refreshBackMessage);
		
		logger.info("Home Armed or Disarmed, MQTT Local Publish Called");
		
		String publicTopic = "refreshIntBackGenieHomeId_" + userDetails.getHome().getId().toString();
		
		
		//child thread to publish on Internet 
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MqttlConnection.getObj().getInternetMqttConnection().publish(publicTopic, refreshBackMessage);
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
		});
		
		
		return homeArmResponseDTO;
	}

	@Override
	public HomeLockCodeResponseDTO setLockCode(SwitchIdDTO switchIdDTO) throws Exception {
		
		HomeLockCodeResponseDTO homeLockCodeResponseDTO = new HomeLockCodeResponseDTO();
		
		Switch switchHomeLock[] = switchRepository.getHomeLock(Constants.DIMMER_STATUS_FOR_HOME_LOCK, Constants.DIMMER_STATUS_FOR_LOCK);
		
		for(int i = 0; i < switchHomeLock.length; i++){
			
				logger.info("setLockCode(), switch Details fetched successfully. Switch Id: "+ switchHomeLock[i].getId());
				
				switchHomeLock[i].setLockCode(switchIdDTO.getLockCode());
				
				switchRepository.save(switchHomeLock[i]);
				
				logger.info("setLockCode(), Switch Details saved successfully. Switch Id: "+ switchHomeLock[i].getId());
				
				homeLockCodeResponseDTO.setLockCode(switchIdDTO.getLockCode());
				homeLockCodeResponseDTO.setObjSwitch(switchHomeLock[i]);
				
		}
		
		return homeLockCodeResponseDTO;
	}

}
