package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.entity.Room;

@Service
public interface RoomService {

	Room addUpdateRoom(AddUpdateRoomDTO dto);
	
	List<RoomListByUserDTO> getRoomListByVendorId(Long vendorId,String messageFrom);
	
	Boolean deleteRoom(Long roomId);

	Boolean changeRoomHideStatus(UpdateHideRoomDTO updateHideRoomDTO) throws Exception;
	
}
