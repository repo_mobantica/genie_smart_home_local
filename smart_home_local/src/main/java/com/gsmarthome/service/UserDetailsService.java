package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.gsmarthome.dto.BackupRequestConfigDTO;
import com.gsmarthome.dto.BackupResponseConfigDTO;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdatePasswordDTO;
import com.gsmarthome.dto.UpdatePasswordOTPDTO;
import com.gsmarthome.dto.UpdateProfileDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UpdateUserTypeDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserShareControlDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.ValidateUserDTO;
import com.gsmarthome.dto.VerifyEmailOTP;
import com.gsmarthome.dto.VerifyOTPDTO;
import com.gsmarthome.dto.VerifyOTPResponse;
import com.gsmarthome.entity.UserDetails;

@Service
public interface UserDetailsService {
	
	public ResponseDTO<UserDetails> save(UserDetails userDetails,Boolean shareControl,BindingResult bindingResult);
	
	public ResponseDTO<Boolean> userShareControl(UserShareControlDTO shareControlDTO,BindingResult bindingResult);
	
	public Boolean userDeleteShareControl(UserShareControlDTO shareControlDTO,BindingResult bindingResult);
	
	public ResponseDTO<Boolean> updatePassword(UpdatePasswordDTO passwordDTO); 
	
	public ResponseDTO<UserIdDTO> sendOTP(EmailDTO emailDTO);
	
	public ResponseDTO<VerifyOTPResponse> verifyOTP(VerifyOTPDTO emailDTO);
	
	public ResponseDTO<Boolean> verifyEmailOTP(VerifyEmailOTP emailDTO);
	
	public ResponseDTO<Integer> validateUser(ValidateUserDTO validateUserDTO);
	
	public ResponseDTO<List<UserListDTO>> getByUser(UserIdDTO userIdDTO);
	
	ResponseDTO<Boolean> updatePasswordOTP(UpdatePasswordOTPDTO passwordDTO);
	
	public ResponseDTO<Boolean> updateUserType(UpdateUserTypeDTO updateUserTypeDTO);

	public ResponseDTO<Boolean> updateProfile(UpdateProfileDTO updateProfileDTO);
	
	public Boolean updateUserDevicetype(UpdateUserDeviceDTO updateUserDeviceDTO); 
	
	public String sendHomeVerificationOTP(EmailDTO emailDTO);
	
	public ResponseDTO<BackupResponseConfigDTO> backupConfig(BackupRequestConfigDTO backupRequestConfigDTO);
	
	public ResponseDTO<UserDetails> syncShareControlData(UserdetailsResponseDTO userDetails, Boolean shareControl, BindingResult bindingResult);

	
}
