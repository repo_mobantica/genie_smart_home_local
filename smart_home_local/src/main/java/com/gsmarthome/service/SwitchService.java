package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.RoomAndSwitchResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;

@Service
public interface SwitchService {

	Room addEditSwitch(AddUpdateSwitchDTO switchDTO);
	
	Boolean deleteSwitch(Long switchId);
	
	ChangeSwitchStatusResponseDTO switchOnOff(ChangeSwitchStatusDTO changeSwitchStatusDTO);
	
	ChangeSwitchStatusResponseDTO changeGenieColor(ChangeSwitchStatusDTO changeSwitchStatusDTO);
	
	List<SwitchListByRoomDTO> getSwitchListByRoom(Long roomId);
	
	List<RoomAndSwitchResponseDTO> getSwitchListByUserId(Long userId, String messageFrom);
	
	ChangeLockStatusResponseDTO changeLockStatus(ChangeLockStatusDTO changeLockStatusDTO);

	ChangeHideStatusResponseDTO changeHidestatus(ChangeHideStatusDTO changeHideStatusDTO);
	
	ChangeSwitchStatusResponseDTO changeCurtainStatus(ChangeCurtainStatusDTO changeCurtainStatusDTO);
	
	List<ChangeStatusByRoomResponseDTO>  changeStatusByRoom(ChangeStatusByRoomDTO changeStatusByRoomDTO );
	
	Switch homeUnlock(SwitchIdDTO switchid);
	
	void processPhysicalSwitch(	byte[] b);

	ChangeSwitchStatusResponseDTO changeSwitchStatusByRoom(ChangeSwitchStatusDTO changeSwitchStatusDTO);
	
	public Boolean  updateSwitchStatus(String msg1);
}
