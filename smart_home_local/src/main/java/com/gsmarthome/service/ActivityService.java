package com.gsmarthome.service;

import java.util.List;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.entity.Activity;

public interface ActivityService {
	
	Activity addActivity(Activity dto);
	
	List<ActivityResponseDTO> getActivityList();
	
}
