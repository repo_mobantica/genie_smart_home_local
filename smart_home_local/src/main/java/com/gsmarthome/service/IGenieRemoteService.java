package com.gsmarthome.service;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.RemoteIRRequestDTO;

@Service
public interface IGenieRemoteService {
	
	public void commandIRFire(RemoteIRRequestDTO remoteIRReqDTO);
	
	public void learnIRCode(RemoteIRRequestDTO remoteIRReqDTO);
	
}
