package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.NotificationHistoryResponseDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.dto.NotificationHistoryDTO;

@Service
public interface NotificationService {
	 
	 List<NotificationHistoryResponseDTO> getNotificationHistory(NotificationHistoryDTO notificationHistoryDTO );
	
	 List<ActivityResponseDTO> getActivityList(ActivityDTO activityDTO);

	 boolean sendNotificationMessage(UserDetails[] deviceDetails, String notifyMessage);

	 boolean sendNotificationMessage(UserDetails deviceDetails, String notifyMessage);

	 void sendNotificationMessage(String backToOnlineMessage);
}
