package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.GenieCamRequestDTO;
import com.gsmarthome.dto.GenieCamResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.entity.GenieCam;

@Service
public interface GenieCamService {
	
	ResponseDTO<List<GenieCam>> getAllGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO);
	
	ResponseDTO<GenieCamResponseDTO> addGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO);
	
	ResponseDTO<GenieCamResponseDTO> deleteGenieCamDetails(GenieCamRequestDTO genieCamRequestDTO);
	
}
