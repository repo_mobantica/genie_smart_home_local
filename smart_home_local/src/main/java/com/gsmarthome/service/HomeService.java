package com.gsmarthome.service;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.dto.HomeLockCodeResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;

@Service
public interface HomeService {
	
	HomeArmANDBlockResponseDTO armHome(HomeDTO homeDTO) throws Exception;
	
	HomeLockCodeResponseDTO setLockCode(SwitchIdDTO switchIdDTO) throws Exception;
	
}
