package com.gsmarthome.service;

import java.util.List;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.TowerRequestDTO;
import com.gsmarthome.entity.Tower;

public interface TowerService {
	
	public ResponseDTO<List<Tower>> getAllTower(TowerRequestDTO towerReqDTO);
}
