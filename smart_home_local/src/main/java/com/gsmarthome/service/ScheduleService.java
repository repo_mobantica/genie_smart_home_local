package com.gsmarthome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.entity.ScheduleSwitch;

@Service
public interface ScheduleService {

	ScheduleSwitchResponseDTO scheduleSwitch(ScheduleSwitchDTO dto) throws Exception;
	
	ScheduleProfileResponseDTO scheduleProfile(ScheduleProfileDTO dto) throws Exception;
	
	ScheduleSwitchResponseDTO editScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception;
	
	ScheduleProfileResponseDTO editScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception;

	ScheduleSwitchResponseDTO deleteScheduleSwitch(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception;
	
	ScheduleProfileResponseDTO deleteScheduleProfile(ScheduleProfileDTO scheduleProfileDTO) throws Exception;
	
	List<ScheduleSwitch> getScheduleSwitchList(ScheduleSwitchDTO scheduleSwitchDTO) throws Exception;

}

