package com.gsmarthome.SendData;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;

import com.amazonaws.util.json.JSONObject;

public class PostCollection {


	  public static String postCommanRestApi(String url, JSONObject jsonData)
	    throws java.io.IOException
	  {
		  System.out.println("url = "+url);
		  System.out.println("jsonData = "+jsonData);
	    StringBuffer response = null;
	    java.net.URL obj = new java.net.URL(url);
	    HttpURLConnection con = (HttpURLConnection)obj.openConnection();
	    

	    con.setRequestMethod("POST");
	    con.setRequestProperty("User-Agent", "Mozilla/5.0");
	    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	    con.setRequestProperty("Content-Type", "application/json");
	//    con.setRequestProperty("X-AUTH-TOKEN", tockenAuth);
	    

	    con.setDoOutput(true);
	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    wr.writeBytes(jsonData.toString());
	    wr.flush();
	    wr.close();
	    



	    BufferedReader in = new BufferedReader(new java.io.InputStreamReader(con.getInputStream()));
	    
	    response = new StringBuffer();
	    String output; while ((output = in.readLine()) != null) { 
	    	//String output;
	      response.append(output);
	    }
	    in.close();
	    return response.toString();
	  }
	  
}
