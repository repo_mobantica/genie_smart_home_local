package com.gsmarthome;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.entity.EventHolderBean;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.service.SwitchService;

@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent>{

	static Logger logger = Logger.getLogger(ContextRefreshedListener.class.getName());
	
	private static Long userId = 0l;
	
	private EventHolderBean eventHolderBean;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	NotificationService notificationService;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	SwitchService switchService;
	 
	@Autowired
    public void setEventHolderBean(EventHolderBean eventHolderBean) {
        this.eventHolderBean = eventHolderBean;
    }
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		logger.info("genie Pyramid Restart Notification Sending notification to all users...");
		
		eventHolderBean.setEventFired(true);
		
		Iterable<UserDetails> userDetailsList = userDetailsRepository.findAll();
	
		Iterator<UserDetails> itr = userDetailsList.iterator();
		
		while(itr.hasNext()){
			UserDetails user =  itr.next();
			
			userId = user.getId();
			
			if(user.getDeviceId() != null)
			{
				try{
					notificationService.sendNotificationMessage(user, user.getHome().getHomeName()+Constants.BACK_TO_ONLINE_MESSAGE);
					logger.info("Notification sent to all users regarding pyramid is back to online");
					
				}catch(Exception e){	
					e.printStackTrace();
					logger.error("Exception Message while sending Notification on pyramid restart: "+e.getMessage()+" Cause Exception:"+e.getCause());
				}
			}	
		}
		
		//turn the home switch Status according to database switch status
		turnHomeSwitchWhenBoots();
		
	}
	
	/********************************************************************************
	* Method Name: turnHomeSwitchWhenBoots
	* 
	* Parameters: void 
	* 
	* Description: whenever home get reboots this method will turn the home switch 
	* status according to database switch status.
	*
	* Response: void
	*********************************************************************************/
	 private void turnHomeSwitchWhenBoots(){
	
		Iterable<Switch> itSwitch = switchRepository.findAll();

		logger.info("turnHomeSwitchWhenBoots method, switch retrieved");
		
		Iterator<Switch> itrSwitch = itSwitch.iterator();
		
		while(itrSwitch.hasNext()){
			
			Switch switchDetails = itrSwitch.next();
			
			ChangeSwitchStatusDTO changeSwitchStatusDTO = new ChangeSwitchStatusDTO();
			changeSwitchStatusDTO.setSwitchId(switchDetails.getId());
			changeSwitchStatusDTO.setSwitchStatus(switchDetails.getSwitchStatus());
			changeSwitchStatusDTO.setDimmerValue(switchDetails.getDimmerValue());
			changeSwitchStatusDTO.setUserid(userId);
			changeSwitchStatusDTO.setMessageFrom("HubBoot");
			try {
				ChangeSwitchStatusResponseDTO changeSwitchStatusRespDTO = switchService.switchOnOff(changeSwitchStatusDTO);
			}catch (Exception e) {
				logger.error("Exception in turnHomeSwitchWhenBoots() Hub Boot Task "+e.getMessage());
				e.printStackTrace();
			}
		}
		
	 }

}
