package com.gsmarthome.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.gsmarthome.constants.Constants;

public class FCMNotifier {

	static Logger logger = Logger.getLogger(FCMNotifier.class.getName());
	
	URL url;
	String deviceID;
	HttpURLConnection conn;
	String serverKey;
	HashMap<String, Object> message;
	
	JsonElement jsonMessage;
	
	HashMap<String, Object> notification = new HashMap<>();

	boolean success;

	public FCMNotifier(String serverKey, String deviceID) throws Exception {
		url = new URL(Constants.FCM_NOTIFIER_URL);
		conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty(Constants.CONTENT_TYPE, "application/json");
		this.serverKey = serverKey;
		conn.setRequestProperty(Constants.AUTHORISATION, "key=" + serverKey);
		message = new HashMap<>();
		this.deviceID = deviceID;
	}

	public void setMessage(String title, Object body) throws Exception {
		HashMap<String, Object> data = new HashMap<>();

		HashMap<String, Object> innerMessage = new HashMap<>();
		
		innerMessage.put(Constants.FCM_NOTIFIER_TITLE, title);
		innerMessage.put(Constants.FCM_NOTIFIER_BODY, body);

		data.put(Constants.MESSAGE, innerMessage);

		// message.put("priority","normal");
		message.put(Constants.FCM_NOTIFIER_TO, deviceID);
		message.put(Constants.FCM_NOTIFIER_DATA, data);
		message.put(Constants.FCM_NOTIFIER_CONTENT_AVAILABLE, true);
		
		jsonMessage = new Gson().toJsonTree(message);

	}

	public void setNotification(String title, Object body) throws Exception {
		notification = new HashMap<>();
		// notification.put("title", title);
		notification.put(Constants.FCM_NOTIFIER_BODY, body);
		notification.put(Constants.FCM_NOTIFIER_SOUND, "default");
		// notification.put("badge", count);
		message.put(Constants.FCM_NOTIFIER_NOTIFICATION, notification);
	}

	public boolean sendNotification() throws Exception {
		Thread sendNotificationThread = new Thread() {
			public void run() {
				String response = "Default";

				String output;
				try {

					String input = jsonMessage.toString();

					logger.info(jsonMessage.toString());

					OutputStream os = conn.getOutputStream();
					os.write(input.getBytes());
					os.flush();

					if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
						logger.info("ResponseCode::" + conn.getResponseCode());
					}

					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));


					logger.info("Output from Server .... \n");
					
					while ((output = br.readLine()) != null) {
						response = response + "\n" + output;
					}
					conn.disconnect();

					if (response.contains("\"success\":1")) {
						success = true;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("Exception Message: "+e.getMessage()+" Cause:"+e.getCause());
					response = e.getMessage();
				}
			};
		};
		
		sendNotificationThread.start();
		
		try {
			sendNotificationThread.join();			
		} catch (InterruptedException iex) {
			iex.printStackTrace();
		}

		return success;
	}

}
