package com.gsmarthome.util;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.aop.aspectj.SingletonAspectInstanceFactory;

import com.gsmarthome.Application;

public class JdbcGetConnection {
	private static java.sql.Connection connection = null;

	public static java.sql.Connection getConnection() {

		if (connection == null) {
			try {
				Properties prop = new Properties();
				// load a properties file from class path, inside static method
				prop.load(Application.class.getClassLoader().getResourceAsStream("application.properties"));
				String host = prop.getProperty("spring.datasource.url");
				String username = prop.getProperty("spring.datasource.username");
				String password = prop.getProperty("spring.datasource.password");
				connection = DriverManager.getConnection(host, username, password);
			} catch (SQLException exception) {
				System.out.println(exception);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return connection;
	}

}
