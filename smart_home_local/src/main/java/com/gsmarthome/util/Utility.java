package com.gsmarthome.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.service.SwitchService;

public class Utility {
/*	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:00");
	private static final SimpleDateFormat dateFormatByDate = new SimpleDateFormat("yyyy-MM-dd");*/

	@Autowired
	SwitchService switchService;
	
	public static String theMonth(int month) {
		String[] monthNames = { Constants.JAN, Constants.FEB, Constants.MAR, Constants.APR, Constants.MAY, Constants.JUN, 
				Constants.JUL, Constants.AUG, Constants.SEP, Constants.OCT, Constants.NOV, Constants.DEC };
		return monthNames[month];
	}

	public static String getTime(String time) {
		String time1 = "";

		try {
			String _24HourTime = time;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			Date _24HourDt = _24HourSDF.parse(_24HourTime);
			System.out.println(_24HourDt);
			System.out.println(_12HourSDF.format(_24HourDt));
			time1 = _12HourSDF.format(_24HourDt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time1;
	}
	
	

	public static void main(String args[]){
	
/*
		Date shareControlDate = null;
		Date currDate = null;
		try {
			currDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateFormatByDate.format(new Date()));
			shareControlDate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-05-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(shareControlDate.before(currDate) || shareControlDate.equals(currDate)){
	
			System.out.println("Something");
			
		}else{
			System.out.println("Not");
		}
		*/
	/*	String str;
        Process processRefresh;
        try {
            processRefresh = Runtime.getRuntime().exec("sudo reboot");
            
        	BufferedReader br = new BufferedReader(
                new InputStreamReader(processRefresh.getInputStream()));
            while ((str = br.readLine()) != null)
                System.out.println("line: " + str);
            processRefresh.waitFor();
            
            System.out.println("exit: " + processRefresh.exitValue());
            processRefresh.destroy();
        } catch (Exception e) {
        	
        }*/
		
		//switchService.sendNotificationMessage("kapil's Home", "Bulb", "1", 11, 4,
		//		null, "Chnage by Name", "0", "0", 7);
		
        
	/*	String strCurrDate = dateFormat.format(new Date());
		
		System.out.println(strCurrDate.substring(0,10));*/
		
		/* final String GCM_API_KEY = "yourKey";
	    final int retries = 3;
	    final String notificationToken = "deviceNotificationToken";
	    Sender sender = new Sender(GCM_API_KEY);
	    Message msg = new Message.Builder().build();

	    try {
	                Result result = sender.send(msg, notificationToken, retries);

	                if (StringUtils.isEmpty(result.getErrorCodeName())) {
	                    logger.debug("GCM Notification is sent successfully");
	                    return true;
	                }

	                logger.error("Error occurred while sending push notification :" + result.getErrorCodeName());
	    } catch (InvalidRequestException e) {
	                logger.error("Invalid Request", e);
	    } catch (IOException e) {
	                logger.error("IO Exception", e);
	    }*/
		
		 	String shutdownCommand;
		    String operatingSystem = System.getProperty("os.name");

		    if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
		        shutdownCommand = "sudo reboot";
		    	//shutdownCommand = "sudo shutdown -r 10";
		    	
		    }
		    else {
		        throw new RuntimeException("Unsupported operating system.");
		    }

		    try {
				Runtime.getRuntime().exec(shutdownCommand);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.exit(0);
		
	}
	
	
	
}
