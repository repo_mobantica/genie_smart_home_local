package com.gsmarthome.dto;


public class UserListDTO {
	
	private Long id;

	private String firstName;
	
	private String lastName;
    
	private String email;
    
    private String phoneNumber;
    
    private Integer userType;
    
    private String image;
    
    private Boolean isEmailVerified;
    
    private Boolean isFirstLogin;

	public UserListDTO() {
	}

	public UserListDTO(Long id, String firstName, String lastName,
			String email, String phoneNumber, Integer userType, String image,
			Boolean isEmailVerified, Boolean isFirstLogin) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.userType = userType;
		this.image = image;
		this.isEmailVerified = isEmailVerified;
		this.isFirstLogin = isFirstLogin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public Boolean getIsFirstLogin() {
		return isFirstLogin;
	}

	public void setIsFirstLogin(Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}  
}
