package com.gsmarthome.dto;

public class TowerRequestDTO {

	private Long homeId;
	private Long userId;
	

	public TowerRequestDTO() {
	}

	public TowerRequestDTO(Long homeId) {
		this.homeId = homeId;
	}

	
	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "HomeDTO [userId=" + homeId + "]";
	}

}
