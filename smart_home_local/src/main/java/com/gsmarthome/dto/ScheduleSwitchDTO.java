package com.gsmarthome.dto;

public class ScheduleSwitchDTO {

	private Long userId;
	private Long switchId;
	private String scheduleSwitchId;
	private String switchStatus;
	private String lockStatus;
	private String dimmerValue;
	private String dimmerStatus;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	private String messageFrom;

	public ScheduleSwitchDTO() {
		super();
	}

	public ScheduleSwitchDTO(String scheduleSwitchId, Long switchId, Long userId, String switchStatus, String lockStatus,
			String scheduleDateTime) {
		this.scheduleSwitchId = scheduleSwitchId;
		this.switchId = switchId;
		this.userId = userId;
		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public ScheduleSwitchDTO(String scheduleSwitchId, Long switchId, Long userId, String switchStatus, String lockStatus,String  repeatStatus, String repeatWeek,
			String scheduleDateTime) {
		this.userId = userId;
		this.switchId = switchId;
		this.scheduleSwitchId = scheduleSwitchId;
		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.repeatWeek = repeatWeek;
		this.repeatStatus = repeatStatus;
		this.scheduleDateTime = scheduleDateTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}
	
	public String getScheduleSwitchId() {
		return scheduleSwitchId;
	}

	public void setScheduleSwitchId(String scheduleSwitchId) {
		this.scheduleSwitchId = scheduleSwitchId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}
	
	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}
	
	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	
	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}

	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
