package com.gsmarthome.dto;

public class RoomIdDTO {

	private Long roomId;

	public RoomIdDTO() {
	}

	public RoomIdDTO(Long roomId) {
		this.roomId = roomId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	@Override
	public String toString() {
		return "RoomIdDTO [roomId=" + roomId + "]";

	}

}
