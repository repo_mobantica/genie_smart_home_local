package com.gsmarthome.dto;

import javax.validation.constraints.NotNull;

public class ValidateUserDTO {

	@NotNull
	private String email;

	private String deviceId;

	private String shareControlOTP;

	public ValidateUserDTO() {
	}

	public ValidateUserDTO(String email, String deviceId, String shareControlOTP) {
		this.email = email;
		this.deviceId = deviceId;
		this.shareControlOTP = shareControlOTP;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getShareControlOTP() {
		return shareControlOTP;
	}

	public void setShareControlOTP(String shareControlOTP) {
		this.shareControlOTP = shareControlOTP;
	}

	@Override
	public String toString() {
		return "ValidateUserDTO [email=" + email + ", deviceId=" + deviceId + ", shareControlOTP=" + shareControlOTP
				+ "]";
	}

}
