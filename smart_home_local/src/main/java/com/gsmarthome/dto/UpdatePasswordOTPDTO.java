package com.gsmarthome.dto;

public class UpdatePasswordOTPDTO {

	private Long userId;

	private String newPassword;

	public UpdatePasswordOTPDTO() {
		super();
	}

	public UpdatePasswordOTPDTO(Long userId, String newPassword) {
		super();
		this.userId = userId;
		this.newPassword = newPassword;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
