package com.gsmarthome.dto;

public class HomeDTO {

	private Long userId;
	private Integer isArmed;
	private String homeName;
	private Integer isBlocked;	
	private String messageFrom;	
	
	public HomeDTO() {
	}

	public HomeDTO(Long userId, String homeName) {
		this.userId = userId;
		this.homeName = homeName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public Integer getIsArmed() {
		return isArmed;
	}

	public void setIsArmed(Integer isArmed) {
		this.isArmed = isArmed;
	}

	public Integer getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Integer isBlocked) {
		this.isBlocked = isBlocked;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	
}
