package com.gsmarthome.dto;

public class ProfileChangeStatusByHomeDTO {

	private Long userId;
	private String switchId;
	private String onoffstatus;
	private String profileSwitchId;
	private String profileOnOffStatus;
	private String messageFrom;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getSwitchId() {
		return switchId;
	}
	public void setSwitchId(String switchId) {
		this.switchId = switchId;
	}
	public String getOnoffstatus() {
		return onoffstatus;
	}
	public void setOnoffstatus(String onoffstatus) {
		this.onoffstatus = onoffstatus;
	}
	public String getProfileSwitchId() {
		return profileSwitchId;
	}
	public void setProfileSwitchId(String profileSwitchId) {
		this.profileSwitchId = profileSwitchId;
	}
	public String getProfileOnOffStatus() {
		return profileOnOffStatus;
	}
	public void setProfileOnOffStatus(String profileOnOffStatus) {
		this.profileOnOffStatus = profileOnOffStatus;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}
	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
