package com.gsmarthome.dto;

public class UpdateUserTypeDTO {
	
	private Long userId;
	
	private Integer newUserType;
	
	private Long adminUserID;

	public UpdateUserTypeDTO() {
	}

	public UpdateUserTypeDTO(Long userId, Integer newUserType) {
		this.userId = userId;
		this.newUserType = newUserType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(Integer newUserType) {
		this.newUserType = newUserType;
	}

	@Override
	public String toString() {
		return "UpdateUserTypeDTO [userId=" + userId + ", newUserType="
				+ newUserType + "]";
	}

	public Long getAdminUserID() {
		return adminUserID;
	}

	public void setAdminUserID(Long adminUserID) {
		this.adminUserID = adminUserID;
	}
	
	
	
	
	

}
