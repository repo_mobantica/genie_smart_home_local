package com.gsmarthome.dto;

public class ChangeStatusByHomeDTO {
	
	private Long userId;
	private String switchId;
	private Long profileId;
	private String onoffstatus;
	private String modeName;
	private String dimmerValues;
	private String messageFrom;

	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getSwitchId() {
		return switchId;
	}

	public void setSwitchId(String switchId) {
		this.switchId = switchId;
	}
	
	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getOnoffstatus() {
		return onoffstatus;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public void setOnoffstatus(String onoffstatus) {
		this.onoffstatus = onoffstatus;
	}
	
	public String getDimmerValues() {
		return dimmerValues;
	}

	public void setDimmerValues(String dimmerValues) {
		this.dimmerValues = dimmerValues;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
