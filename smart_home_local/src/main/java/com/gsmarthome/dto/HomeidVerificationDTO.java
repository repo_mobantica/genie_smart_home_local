package com.gsmarthome.dto;

public class HomeidVerificationDTO {
	private String homeId;

	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public HomeidVerificationDTO() {
		super();
	}

	public HomeidVerificationDTO(String homeId) {
		this.homeId = homeId;

	}

}
