package com.gsmarthome.dto;

import java.util.ArrayList;
import java.util.List;

import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;

public class HomeDetailsResponseDTO {
	private List<Room> roomList = new ArrayList<Room>();
	private List<Switch> switchList = new ArrayList<Switch>();
	//private List<RoomAndSwitchType> roomAndSwitchtypeList = new ArrayList<RoomAndSwitchType>();
	
	private String noofswitchs;

	public HomeDetailsResponseDTO() {

		super();
	}

	public HomeDetailsResponseDTO(String noofswitchs, List<Room> roomList, List<Switch> switchList) {
		this.noofswitchs = noofswitchs;
		this.switchList = switchList;
		this.roomList = roomList;
	}

	public List<Room> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<Room> roomList) {
		this.roomList = roomList;
	}

	public List<Switch> getSwitchList() {
		return switchList;
	}

	public void setSwitchList(List<Switch> switchList) {
		this.switchList = switchList;
	}

	public String getNoofswitchs() {
		return noofswitchs;
	}

	public void setNoofswitchs(String noofswitchs) {
		this.noofswitchs = noofswitchs;
	}

}
