package com.gsmarthome.dto;

public class RemoteIRRequestDTO {

	private Long userId;
	private Long homeId;
	private String towerId;
	private String buttonId;
	private String irRemoteinfo;
	private String brandName;
	private String remoteType;
	private String modelNumber;
	
	public RemoteIRRequestDTO() {
	}

	public String getTowerId() {
		return towerId;
	}


	public void setTowerId(String towerId) {
		this.towerId = towerId;
	}


	public String getIrRemoteinfo() {
		return irRemoteinfo;
	}

	public void setIrRemoteinfo(String irRemoteinfo) {
		this.irRemoteinfo = irRemoteinfo;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(String remoteType) {
		this.remoteType = remoteType;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getButtonId() {
		return buttonId;
	}

	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
	}

	@Override
	public String toString() {
		return "RemoteIRDTO [irRemoteinfo=" + irRemoteinfo + ", userId=" + userId + ", homeId=" + homeId
				+ ", brandName=" + brandName + ", remoteType=" + remoteType + ", modelNumber=" + modelNumber + "]";
	}

}
