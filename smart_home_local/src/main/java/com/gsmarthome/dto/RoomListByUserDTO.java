package com.gsmarthome.dto;

import com.gsmarthome.entity.Switch;

public class RoomListByUserDTO {

	private Long id;

	private String roomName;

	private String roomType;

	private byte[] roomImage;
	
	private String roomImageId;
	
	private String hideStatus;
	
	private Switch objSwitch;

	public RoomListByUserDTO() {
		super();
	}

	public RoomListByUserDTO(Long id, String roomName, String roomType, byte[] roomImage,String roomImageId, String hideStatus) {
		super();
		this.id = id;
		this.hideStatus = hideStatus;
		this.roomName = roomName;
		this.roomType = roomType;
		this.roomImage = roomImage;
		this.roomImageId=roomImageId;
	}
	
	public RoomListByUserDTO(Switch objSwitch) {
		super();
		this.objSwitch = objSwitch;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public byte[] getRoomImagroomImageIde() {
		return roomImage;
	}

	public void setRoomImage(byte[] roomImage) {
		this.roomImage = roomImage;
	}

	public String getRoomImageId() {
		return roomImageId;
	}

	public void setRoomImageId(String roomImageId) {
		this.roomImageId = roomImageId;
	}

	public byte[] getRoomImage() {
		return roomImage;
	}
	
	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	public Switch getObjSwitch() {
		return objSwitch;
	}

	public void setObjSwitch(Switch objSwitch) {
		this.objSwitch = objSwitch;
	}
}
