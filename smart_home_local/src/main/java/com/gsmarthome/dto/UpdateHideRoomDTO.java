package com.gsmarthome.dto;

public class UpdateHideRoomDTO {

	private Long roomId;
	private Long userId;
	private String hideStatus;
	private String messageFrom;
	
	public UpdateHideRoomDTO(){
		
	}
	
	public UpdateHideRoomDTO(Long roomId, String hideStatus){
		this.roomId = roomId;
		this.hideStatus = hideStatus;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

	@Override
	public String toString() {
		return "UpdateHideRoomDTO [roomId=" + roomId + ", userId=" + userId + ", hideStatus=" + hideStatus
				+ ", messageFrom=" + messageFrom + "]";
	}

	
}
