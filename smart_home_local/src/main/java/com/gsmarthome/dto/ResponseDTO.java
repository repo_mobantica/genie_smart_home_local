package com.gsmarthome.dto;

import java.util.List;

public class ResponseDTO<T> {

	String status;

	T result;

	String msg;
	
	Long userId;
	
	String isArmed;
	
	String lockCode;
	

	public ResponseDTO(String aPI_STATUS_SUCCESS, List<SwitchListByRoomDTO> switchAndRoomList, String string, Long long1) {
		super();
	}

	public ResponseDTO(String status, T result) {
		super();
		this.status = status;
		this.result = result;
	}

	public ResponseDTO(String status, T result, String msg) {
		super();
		this.status = status;
		this.result = result;
		this.msg = msg;
	}
	
	public ResponseDTO(String status, T result, String msg, String isArmed, String lockCode) {
		super();
		this.status = status;
		this.result = result;
		this.msg = msg;
		this.isArmed = isArmed;
		this.lockCode = lockCode;
	}
	
	public ResponseDTO(String status, T result, String msg, Long userId) {
		super();
		this.status = status;
		this.result = result;
		this.msg = msg;
		this.userId = userId;
		
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getIsArmed() {
		return isArmed;
	}

	public void setIsArmed(String isArmed) {
		this.isArmed = isArmed;
	}

	public String getLockCode() {
		return lockCode;
	}

	public void setLockCode(String lockCode) {
		this.lockCode = lockCode;
	}
	

}
