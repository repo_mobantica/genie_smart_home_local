package com.gsmarthome.dto;

import java.util.ArrayList;
import java.util.List;

import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;

public class RoomAndSwitchResponseDTO {
	
	private String roomId;
	private String roomName;
	private String roomImageId;
	private String roomHideStatus;

	private List<SwitchListByRoomDTO> switchList = new ArrayList<SwitchListByRoomDTO>();

	public RoomAndSwitchResponseDTO() {

		super();
	}
	
	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
	public String getRoomImageId() {
		return roomImageId;
	}

	public void setRoomImageId(String roomImageId) {
		this.roomImageId = roomImageId;
	}

	public RoomAndSwitchResponseDTO(String noofswitchs, List<Room> roomList, List<SwitchListByRoomDTO> switchList) {
		this.switchList = switchList;
	}
	
	public String getRoomHideStatus() {
		return roomHideStatus;
	}

	public void setRoomHideStatus(String roomHideStatus) {
		this.roomHideStatus = roomHideStatus;
	}
	
	public List<SwitchListByRoomDTO> getSwitchList() {
		return switchList;
	}

	public void setSwitchList(List<SwitchListByRoomDTO> switchList) {
		this.switchList = switchList;
	}

}
