package com.gsmarthome.dto;

public class ChangeStatusByRoomResponseDTO {
	
	private String switchid;
	private String status;
	/*private String profileSwitchid;
	private String profileStatus;*/
	private String dimmerValue;
	private String roomid;
	
	public ChangeStatusByRoomResponseDTO(String switchid, String status,String roomid ) {
		super();
		this.roomid=roomid;		
		this.switchid = switchid;
		this.status = status;
	}

	public ChangeStatusByRoomResponseDTO(String switchid, String status,String dimmerValue,String roomid ) {
		super();
		this.roomid=roomid;		
		this.dimmerValue = dimmerValue;
		this.switchid = switchid;
		this.status = status;
	}
	
	public ChangeStatusByRoomResponseDTO(String switchid, String status) {
		super();
		this.switchid = switchid;
		this.status = status;

	}

	public String getSiwtchid() {
		return switchid;
	}

	public void setSiwtchid(String switchid) {
		this.switchid = switchid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getRoomid() {
		return roomid;
	}

	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	
}
