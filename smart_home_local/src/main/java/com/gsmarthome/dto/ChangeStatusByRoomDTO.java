package com.gsmarthome.dto;

public class ChangeStatusByRoomDTO {
	
	private Long roomId;
	private String onoffstatus;
	private Long userId;
	private String messageFrom;

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getOnoffstatus() {
		return onoffstatus;
	}

	public void setOnoffstatus(String onoffstatus) {
		this.onoffstatus = onoffstatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
