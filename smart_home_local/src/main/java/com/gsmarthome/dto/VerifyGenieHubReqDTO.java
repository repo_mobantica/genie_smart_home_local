package com.gsmarthome.dto;

public class VerifyGenieHubReqDTO {

	private Long homeId;

	public VerifyGenieHubReqDTO() {
	}

	public VerifyGenieHubReqDTO(Long homeId) {
		this.homeId = homeId;
	}

	
	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	@Override
	public String toString() {
		return "HomeDTO [userId=" + homeId + "]";
	}

}
