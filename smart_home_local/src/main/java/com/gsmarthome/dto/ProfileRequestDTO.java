package com.gsmarthome.dto;

public class ProfileRequestDTO {

	private Long profileId;
	private String profileName;
	private Long userId;
	private String switchList;
	private String switchStatus;
	private String dimmerValues;
	private String messageFrom;
	
	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getSwitchList() {
		return switchList;
	}

	public void setSwitchList(String switchList) {
		this.switchList = switchList;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}
	
	public String getDimmerValues() {
		return dimmerValues;
	}

	public void setDimmerValues(String dimmerValues) {
		this.dimmerValues = dimmerValues;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
