package com.gsmarthome.dto;

public class ProfileChangeStatusByRoomResponseDTO {

	private String switchid;
	private String profileSwitchId;
	private String status;
	private String roomid;
	
	public ProfileChangeStatusByRoomResponseDTO(String switchid, String  profileSwitchId,String status,String roomid ) {
		super();
		this.roomid=roomid;		
		this.switchid = switchid;
		this.profileSwitchId = profileSwitchId;
		this.status = status;
	}
	
	public String getSwitchid() {
		return switchid;
	}
	public void setSwitchid(String switchid) {
		this.switchid = switchid;
	}
	public String getProfileSwitchId() {
		return profileSwitchId;
	}
	public void setProfileSwitchId(String profileSwitchId) {
		this.profileSwitchId = profileSwitchId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	
	
	
	
}
