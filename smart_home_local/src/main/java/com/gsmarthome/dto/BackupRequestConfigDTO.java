package com.gsmarthome.dto;

public class BackupRequestConfigDTO {

	private Long homeId;

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	
}
