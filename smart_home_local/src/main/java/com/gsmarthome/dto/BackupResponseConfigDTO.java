package com.gsmarthome.dto;

public class BackupResponseConfigDTO {

	private String secureUsername;
	private String securePassword;
	
	public String getSecureUsername() {
		return secureUsername;
	}
	public void setSecureUsername(String secureUsername) {
		this.secureUsername = secureUsername;
	}
	public String getSecurePassword() {
		return securePassword;
	}
	public void setSecurePassword(String securePassword) {
		this.securePassword = securePassword;
	}	

	

}
