package com.gsmarthome.dto;

public class ChangeCurtainStatusDTO {

	private Long switchId;

	private String switchStatus;

	private String curtainStatus;

	private Long userId;
	
	private String messageFrom;

	public ChangeCurtainStatusDTO() {
		super();
	}

	public ChangeCurtainStatusDTO(Long switchId, String switchStatus, String curtainStatus, Long userid) {
		super();
		this.switchId = switchId;
		this.switchStatus = switchStatus;
		this.curtainStatus = curtainStatus;
		this.userId = userid;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getCurtainStatus() {
		return curtainStatus;
	}

	public void setCurtainStatus(String curtainStatus) {
		this.curtainStatus = curtainStatus;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

}
