package com.gsmarthome.dto;

public class ScheduleProfileDTO {

	private Long userId;
	private Long profileId;
	private Long scheduleId;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	private String messageFrom;

	public ScheduleProfileDTO() {
		super();
	}

	public ScheduleProfileDTO(Long scheduleId, Long switchId, Long userId, String switchStatus, String lockStatus,
			String scheduleDateTime) {
		this.scheduleId = scheduleId;
		this.profileId = switchId;
		this.userId = userId;
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public ScheduleProfileDTO(Long scheduleId, Long switchId, Long userId, String switchStatus, String lockStatus,String  repeatStatus, String repeatWeek,
			String scheduleDateTime) {
		this.scheduleId = scheduleId;
		this.profileId = switchId;
		this.userId = userId;
		this.repeatWeek = repeatWeek;
		this.repeatStatus = repeatStatus;
		this.scheduleDateTime = scheduleDateTime;
	}
	


	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}

	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
}
