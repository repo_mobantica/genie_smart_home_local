package com.gsmarthome.dto;

public class HomeArmANDBlockResponseDTO {

	private String homeId;
	private String homeName;
	private String isArm;
	private String isBlocked;
	private String status;
	
	public HomeArmANDBlockResponseDTO() {
	}

	public HomeArmANDBlockResponseDTO(String homeId) {
		this.homeId = homeId;
	}
	
	public String getHomeId() {
		return homeId;
	}

	public void setHomeId(String homeId) {
		this.homeId = homeId;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public String getIsArm() {
		return isArm;
	}

	public void setIsArm(String isArm) {
		this.isArm = isArm;
	}
	
	public String getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(String isBlocked) {
		this.isBlocked = isBlocked;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
