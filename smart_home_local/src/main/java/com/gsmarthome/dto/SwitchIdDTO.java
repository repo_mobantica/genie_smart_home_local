package com.gsmarthome.dto;

public class SwitchIdDTO {

	private Long switchId;	
	private Long userId;
	private String messageFrom;
	private String lockCode;

	public SwitchIdDTO() {
	}

	public SwitchIdDTO(Long switchId) {
		this.switchId = switchId;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

	public String getLockCode() {
		return lockCode;
	}

	public void setLockCode(String lockCode) {
		this.lockCode = lockCode;
	}

}
