package com.gsmarthome.dto;

public class ScheduleSwitchResponseDTO {
	private Long switchId;
	private Long userId;
	private String switchStatus;
	private String lockStatus;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	private Long scheduleSwitchId;
	private String switchName;

	public ScheduleSwitchResponseDTO() {
		super();
	}

	public Long getScheduleSwitchId() {
		return scheduleSwitchId;
	}

	public void setScheduleSwitchId(Long scheduleSwitchId) {
		this.scheduleSwitchId = scheduleSwitchId;
	}

	public ScheduleSwitchResponseDTO(Long switchId, String switchStatus, String lockStatus, Long userId,
			String scheduleDateTime, Long scheduleSwitchId, String switchName) {
		this.scheduleSwitchId=scheduleSwitchId;
		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.userId = userId;
		this.scheduleDateTime = scheduleDateTime;
		this.switchId = switchId;
		this.switchName=switchName;
	}

	public ScheduleSwitchResponseDTO(Long switchId, String switchStatus, String lockStatus,String repeatStatus, String repeatWeek, Long userId,
			String scheduleDateTime, Long scheduleSwitchId, String switchName) {
		this.scheduleSwitchId = scheduleSwitchId;
		this.switchId = switchId;
		this.userId = userId;
		this.repeatStatus= repeatStatus;
		this.repeatWeek= repeatWeek;
		this.switchStatus = switchStatus;
		this.lockStatus = lockStatus;
		this.scheduleDateTime = scheduleDateTime;
		this.switchName = switchName;
	}
	
	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}
	
	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}

	@Override
	public String toString() {
		return "ScheduleSwitchResponseDTO [switchId=" + switchId + ", userId=" + userId + ", switchStatus="
				+ switchStatus + ", lockStatus=" + lockStatus + ", repeatStatus=" + repeatStatus + ", repeatWeek="
				+ repeatWeek + ", scheduleDateTime=" + scheduleDateTime + ", scheduleSwitchId=" + scheduleSwitchId
				+ ", switchName=" + switchName + "]";
	}

	
}
