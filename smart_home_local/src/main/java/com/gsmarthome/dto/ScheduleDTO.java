package com.gsmarthome.dto;

public class ScheduleDTO {

	private Long userId;

	private String homeName;

	public ScheduleDTO() {
	}

	public ScheduleDTO(Long userId, String homeName) {
		this.userId = userId;
		this.homeName = homeName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	@Override
	public String toString() {
		return "HomeDTO [userId=" + userId + ", homeName=" + homeName + "]";
	}

}
