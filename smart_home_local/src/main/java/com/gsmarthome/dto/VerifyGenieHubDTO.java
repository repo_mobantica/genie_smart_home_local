package com.gsmarthome.dto;

public class VerifyGenieHubDTO {
	
	private String response;
	private String panelDetails;
	private String routerSSID;
	private String routerPassword;
	private String secureSSID;
	private String securePassword;
	private String statusCode;	
	private String Status;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return Status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getRouterSSID() {
		return routerSSID;
	}

	public void setRouterSSID(String routerSSID) {
		this.routerSSID = routerSSID;
	}

	public String getRouterPassword() {
		return routerPassword;
	}

	public void setRouterPassword(String routerPassword) {
		this.routerPassword = routerPassword;
	}

	public String getSecureSSID() {
		return secureSSID;
	}

	public void setSecureSSID(String secureSSID) {
		this.secureSSID = secureSSID;
	}

	public String getSecurePassword() {
		return securePassword;
	}

	public void setSecurePassword(String securePassword) {
		this.securePassword = securePassword;
	}

	public String getPanelDetails() {
		return panelDetails;
	}

	public void setPanelDetails(String panelDetails) {
		this.panelDetails = panelDetails;
	}
	

}
