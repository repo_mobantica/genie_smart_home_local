package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateSwitchDTO;
import com.gsmarthome.dto.ChangeCurtainStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusDTO;
import com.gsmarthome.dto.ChangeHideStatusResponseDTO;
import com.gsmarthome.dto.ChangeLockStatusDTO;
import com.gsmarthome.dto.ChangeLockStatusResponseDTO;
import com.gsmarthome.dto.ChangeStatusByRoomDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomAndSwitchResponseDTO;
import com.gsmarthome.dto.RoomIdDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.dto.SwitchListByRoomDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.serviceImpl.SwitchServiceImpl;;

@RestController
@RequestMapping(value = "/switch")
public class SwitchController {
	
	static Logger logger = Logger.getLogger(SwitchServiceImpl.class.getName());
	
	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";
	
	
	@Autowired
	UserDetailsService usersDetailsService;
	
	@Autowired
	SwitchService switchService;
	
	@Autowired
	RoomService roomService;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	SwitchRepository switchRepository;
	
	@Autowired
	ActivityRepository activityRepository;

	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@RequestMapping(value = "/homeunlock", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Switch>> homeUnlock(@RequestBody SwitchIdDTO switchIdDTO) {
		
		Switch switchDetails =  switchService.homeUnlock(switchIdDTO);
		
		if(switchDetails != null){
			return new ResponseEntity<ResponseDTO<Switch>>(new ResponseDTO<Switch>(API_STATUS_SUCCESS, switchDetails),HttpStatus.OK);
		}else{
			return new ResponseEntity<ResponseDTO<Switch>>(new ResponseDTO<Switch>(API_STATUS_FAILURE, switchDetails),HttpStatus.BAD_REQUEST);			
		}
	}
	
	/*
	 * Purpose:To add new switch
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Room>> addSwitch(@RequestBody AddUpdateSwitchDTO dto) {
		try {
			Room room = switchService.addEditSwitch(dto);

			return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_SUCCESS, room),
					HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<ResponseDTO<Room>>(
					new ResponseDTO<Room>(API_STATUS_FAILURE, null, e.getMessage()), HttpStatus.OK);
		}
	}

	/*
	 * Purpose:To edit switch details
	 */

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Room>> editSwitch(@RequestBody AddUpdateSwitchDTO dto) {
		try {
			Room room = switchService.addEditSwitch(dto);

			return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_SUCCESS, room),
					HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<ResponseDTO<Room>>(
					new ResponseDTO<Room>(API_STATUS_FAILURE, null, e.getMessage()), HttpStatus.OK);
		}
	}

	/*
	 * Purpose:To delete switch details
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> delete(@RequestBody SwitchIdDTO dto) {
		Boolean status = switchService.deleteSwitch(dto.getSwitchId());

		return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_SUCCESS, status),
				HttpStatus.OK);
	}

	/*
	 * Purpose:To perform Switch ON/OFF activity
	 */

	@RequestMapping(value = "/changestatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>> changeStatus(
			@RequestBody ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		
		ChangeSwitchStatusResponseDTO changeSwitchStatusResponseDTO = switchService.switchOnOff(changeSwitchStatusDTO);
		
		if (changeSwitchStatusResponseDTO.getStatus().equals("Already lock")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Already Lock"),
					HttpStatus.OK);
		} else if (changeSwitchStatusResponseDTO.getStatus().equals("Hidden")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Already Hidden"),
					HttpStatus.OK);
		} else {
			if (changeSwitchStatusResponseDTO.getStatus().equals(Constants.SUCCESS_MSG))
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_SUCCESS, changeSwitchStatusResponseDTO, "Success"),
						HttpStatus.OK);
			else
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusResponseDTO, "Failure"),
						HttpStatus.OK);
		}
	}
	
	
	/***************************************************************************
	* Method Name: changeCurtainStatus
	* 
	* Parameters: ChangeCurtainStatusDTO POJO JSON request body
	* 
	* Description: This method will open and close the curtain from mobile and 
	* pause the curtain while closing and opening the curtain.
	*
	* Response: ChangeSwitchStatusResponseDTO POJO Response body
	*
	***************************************************************************/
	
	@RequestMapping(value = "/changecurtainstatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>> changeCurtainStatus(
			@RequestBody ChangeCurtainStatusDTO changeCurtainStatusDTO) {
		
		ChangeSwitchStatusResponseDTO changeSwitchStatusRespDTO = switchService.changeCurtainStatus(changeCurtainStatusDTO);
		
		if (changeSwitchStatusRespDTO.getStatus().equals("Already lock")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusRespDTO, "Already Lock"),
					HttpStatus.OK);
		} else if (changeSwitchStatusRespDTO.getStatus().equals("Already hidden")) {
			return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
					new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusRespDTO, "Already Hidden"),
					HttpStatus.OK);
		} else {
			if (changeSwitchStatusRespDTO.getStatus().equals("Success"))
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_SUCCESS, changeSwitchStatusRespDTO, "Success"),
						HttpStatus.OK);
			else
				return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
						new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_FAILURE, changeSwitchStatusRespDTO, "Failure"),
						HttpStatus.OK);
		}
	}

	// Change Lock Status
	@RequestMapping(value = "/changelockstatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeLockStatusResponseDTO>> changeLockstatus(
			@RequestBody ChangeLockStatusDTO dto) {
		ChangeLockStatusResponseDTO status = switchService.changeLockStatus(dto);
		return new ResponseEntity<ResponseDTO<ChangeLockStatusResponseDTO>>(
				new ResponseDTO<ChangeLockStatusResponseDTO>(API_STATUS_SUCCESS, status), HttpStatus.OK);
	}

	// Change Switch Status
	@RequestMapping(value = "/changehidestatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeHideStatusResponseDTO>> changeHidestatus(
			@RequestBody ChangeHideStatusDTO dtoChangeHideStatus) {
		ChangeHideStatusResponseDTO status = switchService.changeHidestatus(dtoChangeHideStatus);
		// if(status.get)
		return new ResponseEntity<ResponseDTO<ChangeHideStatusResponseDTO>>(
				new ResponseDTO<ChangeHideStatusResponseDTO>(API_STATUS_SUCCESS, status), HttpStatus.OK);
	}
	
	/***************************************************************************
	* Method Name: changeGenieColor
	* 
	* Parameters: ChangeSwitchStatusDTO POJO JSON request body
	* 
	* Description: This method will change the color of the bulb and make 
	* bulb ON or OFF as well.
	*
	* Response: ChangeSwitchStatusResponseDTO POJO Response body
	*
	***************************************************************************/
	// Change Switch Status
	@RequestMapping(value = "/changegeniecolor", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>> changeGenieColor(
			@RequestBody ChangeSwitchStatusDTO changeSwitchStatusDTO) {
		ChangeSwitchStatusResponseDTO status = switchService.changeGenieColor(changeSwitchStatusDTO);
		
		// if(status.get)
		return new ResponseEntity<ResponseDTO<ChangeSwitchStatusResponseDTO>>(
				new ResponseDTO<ChangeSwitchStatusResponseDTO>(API_STATUS_SUCCESS, status), HttpStatus.OK);
	}

	// get switch list by room
	@RequestMapping(value = "/getswitchbyroom", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>> getSwitchList(@RequestBody RoomIdDTO dto) {
		List<SwitchListByRoomDTO> switchList = switchService.getSwitchListByRoom(dto.getRoomId());
		return new ResponseEntity<ResponseDTO<List<SwitchListByRoomDTO>>>(
				new ResponseDTO<List<SwitchListByRoomDTO>>(API_STATUS_SUCCESS, switchList), HttpStatus.OK);
	}

	// To change switch status of all switch from selected Room
	@RequestMapping(value = "/changeStatusByRoom", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>> changeStatusByRoom(
			@RequestBody ChangeStatusByRoomDTO changeStatusByRoom) {
		
		List<ChangeStatusByRoomResponseDTO> switchList = switchService.changeStatusByRoom(changeStatusByRoom);
		
		return new ResponseEntity<ResponseDTO<List<ChangeStatusByRoomResponseDTO>>>(
				new ResponseDTO<List<ChangeStatusByRoomResponseDTO>>(API_STATUS_SUCCESS, switchList), HttpStatus.OK);
	}
	
	@RequestMapping(value="/getswitchbyuser", method=RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<RoomAndSwitchResponseDTO>>> getServiceAndRoomList(@RequestBody UserIdDTO dto,HttpServletRequest request)
	{
		List<RoomAndSwitchResponseDTO> switchList = switchService.getSwitchListByUserId(dto.getUserId() , dto.getMessageFrom());
		
		final String device_id = request.getHeader(DEVICE_ID_HEADER_NAME);
		final String device_type = request.getHeader(DEVICE_TYPE_HEADER_NAME);
		UpdateUserDeviceDTO updateUserDeviceDTO= new UpdateUserDeviceDTO();
		
		updateUserDeviceDTO.setUserId(dto.getUserId());
		updateUserDeviceDTO.setDeviceId(device_id);
		updateUserDeviceDTO.setDeviceType(device_type);
		
		boolean flag = usersDetailsService.updateUserDevicetype(updateUserDeviceDTO);
		
				
		return new ResponseEntity<ResponseDTO<List<RoomAndSwitchResponseDTO>>>(new ResponseDTO<List<RoomAndSwitchResponseDTO>>(API_STATUS_SUCCESS , switchList , "Switch and Room List By User Id", dto.getUserId()),HttpStatus.OK);
	}
	

	public void  physicalSwitch(byte[] b)
	{
				
		switchService.processPhysicalSwitch(b);
	}

	
	public boolean updateSwitchStatus(String msg1)
	{
	System.out.println("msg1 Inn  = "+msg1);
		 return switchService.updateSwitchStatus(msg1);
	}
}
