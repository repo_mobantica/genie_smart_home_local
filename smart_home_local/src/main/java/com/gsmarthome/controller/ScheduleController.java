package com.gsmarthome.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.ScheduleProfileDTO;
import com.gsmarthome.dto.ScheduleProfileResponseDTO;
import com.gsmarthome.dto.ScheduleSwitchDTO;
import com.gsmarthome.dto.ScheduleSwitchResponseDTO;
import com.gsmarthome.entity.ScheduleSwitch;
import com.gsmarthome.service.ScheduleService;

@RestController
@RequestMapping(value = "/schedule")
public class ScheduleController{

	static Logger logger = Logger.getLogger(ScheduleController.class.getName());
	
	@Autowired
	ScheduleService scheduleService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;


	// Schedule Switch
	@RequestMapping(value = "/scheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>> scheduleswitch(@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		ScheduleSwitchResponseDTO schedulSwitchDetails = null;
		
		try{
			schedulSwitchDetails = scheduleService.scheduleSwitch(scheduleSwitchDTO);
			 
			if(schedulSwitchDetails != null){
				logger.info("scheduleswitch(), Schedule Successfully applied for Switch ID: "+scheduleSwitchDTO.getSwitchId());  
				return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
							new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS, schedulSwitchDetails, "Schedule Successfully aapplied"), HttpStatus.OK);
			}else{
				logger.error("scheduleswitch(), Scheduler Failed to apply for Switch ID: "+scheduleSwitchDTO.getSwitchId());
				return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
						new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_FAILURE, schedulSwitchDetails,"Schedule creation failed"), HttpStatus.BAD_REQUEST);
			}
			
		}catch(Exception e){
			logger.error("Exception in scheduleswitch() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
					new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_FAILURE, schedulSwitchDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	
	}
	
	// Schedule Profile
	@RequestMapping(value = "/scheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> scheduleProfile(@RequestBody ScheduleProfileDTO reqScheduleProfileDTO) {
		ScheduleProfileResponseDTO respSchedulProfileDetails = null;
		
		try{
			respSchedulProfileDetails = scheduleService.scheduleProfile(reqScheduleProfileDTO);
			 
			if(respSchedulProfileDetails != null){
				logger.info("scheduleProfile(), Schedule Successfully applied for Switch ID: "+reqScheduleProfileDTO.getProfileId());  
				return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
							new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, respSchedulProfileDetails, "Profile Schedule Successfully applied"), HttpStatus.OK);
			}else{
				logger.error("scheduleProfile(), Scheduler Failed to apply for Switch ID: "+reqScheduleProfileDTO.getProfileId());
				return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
						new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, respSchedulProfileDetails,"Schedule creation failed"), HttpStatus.BAD_REQUEST);
			}
			
		}catch(Exception e){
			logger.error("Exception in scheduleProfile() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, respSchedulProfileDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	
	}

	@RequestMapping(value = "/editscheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>> editScheduleSwitch(
			@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		ScheduleSwitchResponseDTO schedulSwitchDetails = null;
		
		try{
			schedulSwitchDetails = scheduleService.editScheduleSwitch(scheduleSwitchDTO);
			
			if(schedulSwitchDetails != null){
				return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
						new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS, schedulSwitchDetails,"Edit Scheduler Succesfully edited the Scheduler"), HttpStatus.OK);
			}else{
				return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
						new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_FAILURE, schedulSwitchDetails,"Edit Scheduler Failed to edit the Scheduler"), HttpStatus.BAD_REQUEST);		
			}
		}catch(Exception e){
			logger.error("Exception in editScheduleswitch() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
					new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_FAILURE, schedulSwitchDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@RequestMapping(value = "/editscheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> editScheduleProfile(
			@RequestBody ScheduleProfileDTO scheduleProfileDTO) {
		ScheduleProfileResponseDTO schedulProfileDetails = null;
		
		try{
			schedulProfileDetails = scheduleService.editScheduleProfile(scheduleProfileDTO);
			
			if(schedulProfileDetails != null){
				return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
						new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, schedulProfileDetails,"Edit Scheduler Succesfully edited the Scheduler"), HttpStatus.OK);
			}else{
				return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
						new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, schedulProfileDetails,"Edit Scheduler Failed to edit the Scheduler"), HttpStatus.BAD_REQUEST);		
			}
		}catch(Exception e){
			logger.error("Exception in editScheduleswitch() Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, schedulProfileDetails,"Exception Message: "+e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	}

	@RequestMapping(value = "/deletescheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>> deleteScheduleSwitch(@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		
		ScheduleSwitchResponseDTO schedulswitchdetails = null;
		try {
			schedulswitchdetails = scheduleService.deleteScheduleSwitch(scheduleSwitchDTO);
		} catch (Exception e) {
			logger.error("deleteScheduleSwitch(), Exception Message: "+e.getMessage()+" Cause: "+e.getCause());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
					new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS, schedulswitchdetails,"Exception Message: "+e.getMessage()+" Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ResponseDTO<ScheduleSwitchResponseDTO>>(
				new ResponseDTO<ScheduleSwitchResponseDTO>(API_STATUS_SUCCESS, schedulswitchdetails), HttpStatus.OK);

	}
	
	@RequestMapping(value = "/deletescheduleprofile", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>> deleteScheduleProfile(@RequestBody ScheduleProfileDTO scheduleProfileDTO) {
		
		ScheduleProfileResponseDTO schedulProfileDetails = null;
		
		try {
			schedulProfileDetails = scheduleService.deleteScheduleProfile(scheduleProfileDTO);
		} catch (Exception e) {
			logger.error("Exception Message: "+e.getMessage()+" Cause: "+e.getCause());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
					new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_FAILURE, schedulProfileDetails,"Exception Message: "+e.getMessage()+"Cause: "+e.getCause()), HttpStatus.BAD_REQUEST);
			
		}
		
		return new ResponseEntity<ResponseDTO<ScheduleProfileResponseDTO>>(
				new ResponseDTO<ScheduleProfileResponseDTO>(API_STATUS_SUCCESS, schedulProfileDetails), HttpStatus.OK);

	}
	
	/**************************************************************************
	 * @methodName 
	 * getScheduleSwitch
	 * 
	 * @description 
	 * This API will get all future Schedule Switch List created by specific 
	 * user for his home. If Schedule Switch list is empty still give response 
	 * as success.
	 * 
	 * @param 
	 * scheduleSwitchDTO
	 * 
	 * @return 
	 * List<ScheduleSwitch>
	 ****************************************************************************/
	
	
	@RequestMapping(value = "/getscheduleswitch", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ScheduleSwitch>>> getScheduleSwitch(@RequestBody ScheduleSwitchDTO scheduleSwitchDTO) {
		
		List<ScheduleSwitch> listScheduleSwitch = null;
		try {
			listScheduleSwitch = scheduleService.getScheduleSwitchList(scheduleSwitchDTO);
		} catch (Exception e) {
			logger.error("Exception Message: "+e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<List<ScheduleSwitch>>>(
					new ResponseDTO<List<ScheduleSwitch>>(API_STATUS_FAILURE, listScheduleSwitch,"Exception Message: "+e.getMessage()), HttpStatus.OK);
		}
		
		if(listScheduleSwitch == null || listScheduleSwitch.isEmpty()){
			return new ResponseEntity<ResponseDTO<List<ScheduleSwitch>>>(
					new ResponseDTO<List<ScheduleSwitch>>(API_STATUS_SUCCESS, listScheduleSwitch,"No Future Schedule list Found"), HttpStatus.OK);
		}
		
		return new ResponseEntity<ResponseDTO<List<ScheduleSwitch>>>(
				new ResponseDTO<List<ScheduleSwitch>>(API_STATUS_SUCCESS, listScheduleSwitch,"Schedule list found Successfully"), HttpStatus.OK);

	}
	
	
}
