package com.gsmarthome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.service.UserDetailsService;


@RestController
@RequestMapping(value="/user")
public class Test {
	 @Autowired
	    UserDetailsService userDetailsService;   
	   
	    @Value("${API_STATUS_SUCCESS}")
		private String API_STATUS_SUCCESS;
	    
	    @Value("${API_STATUS_FAILURE}")
		private String API_STATUS_FAILURE;
		
	    @RequestMapping(value="/test",method=RequestMethod.POST)
	    public void test(){
	    	System.out.println("Test in here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	    }
}
