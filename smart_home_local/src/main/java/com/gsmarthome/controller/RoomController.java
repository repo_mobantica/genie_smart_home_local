package com.gsmarthome.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.AddUpdateRoomDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomIdDTO;
import com.gsmarthome.dto.RoomListByUserDTO;
import com.gsmarthome.dto.UpdateHideRoomDTO;
import com.gsmarthome.dto.UpdateUserDeviceDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.exception.InvalidParameterException;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.RoomService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.serviceImpl.RoomServiceImpl;

@RestController
@RequestMapping(value = "/room")
public class RoomController {

	static Logger logger = Logger.getLogger(RoomController.class.getName());

	private static final String DEVICE_ID_HEADER_NAME = "DEVICE-ID";
	private static final String DEVICE_TYPE_HEADER_NAME = "DEVICE-TYPE";

	@Autowired
	UserDetailsService usersDetailsService;

	@Autowired
	HomeRepository homeRepositoryService;

	@Autowired
	UserDetailsRepository userRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	RoomService roomService;

	@Autowired
	SwitchService switchService;

	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	private HttpServletRequest request;

	private UserIdDTO dto;

	private HttpServletRequest request2;

	private UserIdDTO dto2;

	private HttpServletRequest request3;

	private UserIdDTO dto3;

	private HttpServletRequest request4;

	private UserIdDTO dto4;

	private HttpServletRequest request5;

	/*
	 * Purpose : TO add/update Room details
	 * 
	 */

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Room>> addRoom(@RequestBody AddUpdateRoomDTO dto) {
		try {
			Room room = roomService.addUpdateRoom(dto);

			if (room == null) {
				return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_FAILURE, null,
						"Exception in updating room details, Home is NULL"), HttpStatus.OK);
			}

			return new ResponseEntity<ResponseDTO<Room>>(new ResponseDTO<Room>(API_STATUS_SUCCESS, room),
					HttpStatus.OK);

		} catch (RuntimeException e) {
			return new ResponseEntity<ResponseDTO<Room>>(
					new ResponseDTO<Room>(API_STATUS_FAILURE, null, e.getMessage()), HttpStatus.OK);
		}
	}

	/*
	 * Purpose: Get Room list by userId
	 */

	@RequestMapping(value = "/getlistbyuser", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<RoomListByUserDTO>>> getRoomList(@RequestBody UserIdDTO dto,
			HttpServletRequest request) {
		Home home = null;
		Switch switchLockCode[];
System.out.println("dto.getUserId() = "+dto.getUserId());
		List<RoomListByUserDTO> roomList = roomService.getRoomListByVendorId(dto.getUserId(), dto.getMessageFrom());

		final String device_id = request.getHeader(DEVICE_ID_HEADER_NAME);
		final String device_type = request.getHeader(DEVICE_TYPE_HEADER_NAME);
		UpdateUserDeviceDTO updateUserDeviceDTO = new UpdateUserDeviceDTO();

		updateUserDeviceDTO.setUserId(dto.getUserId());
		updateUserDeviceDTO.setDeviceId(device_id);
		updateUserDeviceDTO.setDeviceType(device_type);

		boolean flag = usersDetailsService.updateUserDevicetype(updateUserDeviceDTO);

		if (!roomList.isEmpty()) {
			home = homeRepositoryService.findByIsArmed(1);
			// switchLockCode =
			// switchRepository.getHomeLock(Constants.DIMMER_STATUS_FOR_HOME_LOCK,
			// Constants.DIMMER_STATUS_FOR_LOCK);

			/*
			 * for(int i = 0; i < switchLockCode.length; i++){
			 * 
			 * if(switchLockCode[i].getLockCode() != null ||
			 * !switchLockCode[i].getLockCode().isEmpty()){ lockCode =
			 * switchLockCode[i].getLockCode(); }
			 * 
			 * if(!lockCode.equals("") || lockCode != null ){ break; } }
			 */
			if (home == null) {
				home = new Home();
				home.setIsArmed(0);
			}
		}

		return new ResponseEntity<ResponseDTO<List<RoomListByUserDTO>>>(
				new ResponseDTO<List<RoomListByUserDTO>>(API_STATUS_SUCCESS, roomList, "", home.getIsArmed().toString(),
						RoomServiceImpl.lockCode),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/getlistbyuser2", method = RequestMethod.POST)
	public void test(){
		System.out.println("Hi");
	}

	/*
	 * Purpose: To delete Room
	 */

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> deleteRoom(@RequestBody RoomIdDTO entityIdDTO) {
		Boolean result = roomService.deleteRoom(entityIdDTO.getRoomId());

		return new ResponseEntity<ResponseDTO<Boolean>>(new ResponseDTO<Boolean>(API_STATUS_SUCCESS, true),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/hideroom", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Boolean>> changeHideRoomStatus(@RequestBody UpdateHideRoomDTO updateHideRoomDTO)
			throws InvalidParameterException {
		Boolean result = false;

		try {
			result = roomService.changeRoomHideStatus(updateHideRoomDTO);
		} catch (NumberFormatException | InvalidParameterException invalidParamEx) {
			logger.error("Exception in changeHideRoomStatus() Exception Message: " + invalidParamEx.getMessage());
			invalidParamEx.printStackTrace();
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_FAILURE, result, invalidParamEx.getMessage()),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error("Exception in changeHideRoomStatus() " + e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_FAILURE, result, e.getMessage()), HttpStatus.BAD_REQUEST);
		}

		if (result == false) {
			logger.error("Hide Room Status did not changed");
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_FAILURE, result, "Hide Room Status did not changed"),
					HttpStatus.OK);
		} else {
			logger.info("Hide Room Status Changed Successfully");
			return new ResponseEntity<ResponseDTO<Boolean>>(
					new ResponseDTO<Boolean>(API_STATUS_SUCCESS, result, "Hide Room Status Changed Successfully"),
					HttpStatus.OK);
		}
	}

}
