package com.gsmarthome.controller;

<<<<<<< HEAD
import java.io.IOException;
=======
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
>>>>>>> stage
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

<<<<<<< HEAD
=======
import com.amazonaws.util.json.JSONObject;
>>>>>>> stage
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.NotificationService;

@RestController
@RequestMapping(value = "/util")
public class UtilityController {
	
	static Logger logger = Logger.getLogger(UtilityController.class.getName());
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Autowired
	NotificationService notificationService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	

	/******************************************************************************************
	* Method Name: refreshHub
	* 
	* Parameters: UserIdDTO POJO request body
	* 
	* Description: This method would used to reboot Pyramid (HUB) if in case some restart require 
	* or devices or hub is not responding
	* 
	* Response: In actual the response would not go, since the hub gets rebooted and connection 
	* will be lost
	*
	******************************************************************************************/
	
	@RequestMapping(value = "/reboothub", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<String>> refreshHub(
			@RequestBody UserIdDTO userIdDTO) {
		
		UserDetails userDetails = userDetailsRepository.findOne(userIdDTO.getUserId());
		
		
		if(userDetails != null){

			UserDetails arrUserDetails[] = userDetailsRepository.findByHome(userDetails.getHome());
			
	        Process processRefresh;
	        try {
	
	            logger.info("*****************Process Will Reboot : "+new Date().getTimezoneOffset()+"************************");
	            
	            notificationService.sendNotificationMessage(arrUserDetails,"Hub Reboot request has taken place, Hub will restart in 5-10 minutes.");
	            
	            String shutdownCommand;
			    String operatingSystem = System.getProperty("os.name");

			    if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
			        shutdownCommand = "sudo reboot";
			    	//shutdownCommand = "sudo shutdown -r 10";
			    	
			    }
			    else {
			        throw new RuntimeException("Unsupported operating system.");
			    }

			    try {
					Runtime.getRuntime().exec(shutdownCommand);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    System.exit(0);
			    
	        } catch (Exception e) {
	        	logger.error("Exception Message: "+e.getStackTrace());
	        	e.printStackTrace();
	        }	
        }else{
        	logger.error("User ID not found"+userIdDTO.getUserId());
        	return new ResponseEntity<ResponseDTO<String>>(HttpStatus.BAD_REQUEST);
        }
        
		return null;
	}
	
	/******************************************************************************************
	* Method Name: rebootPanel
	* 
	* Parameters: UserIdDTO POJO request body
	* 
	* Description: This method would used to reboot panels of home if in case panels are not 
	* responding to the command.
	* 
	* Response: response is not 
	*
	******************************************************************************************/
	
	@RequestMapping(value = "/rebootpanel", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<String>> rebootPanel(
			@RequestBody UserIdDTO userIdDTO) {
		
		UserDetails userDetails = userDetailsRepository.findOne(userIdDTO.getUserId());
		
		if(userDetails != null){
	        Process processRefresh;
	       
			UserDetails arrUserDetails[] = userDetailsRepository.findByHome(userDetails.getHome());
			
	        try {
	        	
	            logger.info("*****************Process Will Reboot : "+new Date().getTimezoneOffset()+"************************");
	            
	            notificationService.sendNotificationMessage(arrUserDetails,"Panel Reboot request has taken place, panel will start working after 5 Minutes.");
	            
	            processRefresh = Runtime.getRuntime().exec("sudo rebootPanels.sh");

	            processRefresh.waitFor();
	            System.exit(0);
	            
	            logger.info("Exit value: "+processRefresh.exitValue());
	            processRefresh.destroy();

	        } catch (Exception e) {
	        	logger.error("Exception Message: "+e.getStackTrace());
	        	e.printStackTrace();
	        }	
		}else{
			logger.error("User ID not found"+userIdDTO.getUserId());
        	return new ResponseEntity<ResponseDTO<String>>(HttpStatus.BAD_REQUEST);
		}
		
		return null;
	}
	
<<<<<<< HEAD
=======

>>>>>>> stage
}
	
