package com.gsmarthome.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ActivityDTO;
import com.gsmarthome.dto.ActivityResponseDTO;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.HomeArmANDBlockResponseDTO;
import com.gsmarthome.dto.HomeDTO;
import com.gsmarthome.dto.HomeDetailsResponseDTO;
import com.gsmarthome.dto.HomeLockCodeResponseDTO;
import com.gsmarthome.dto.HomeVerificationResponseDTO;
import com.gsmarthome.dto.HomeidVerificationDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.RoomAndSwitchResponseDTO;
import com.gsmarthome.dto.SwitchIdDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.VerifyGenieHubDTO;
import com.gsmarthome.dto.VerifyGenieHubReqDTO;
import com.gsmarthome.dto.deviceDetailsDTO;
import com.gsmarthome.dto.panelListDTO;
import com.gsmarthome.entity.DeviceDetails;
import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.InstallationConfiguration;
import com.gsmarthome.entity.Panel;
import com.gsmarthome.entity.Room;
import com.gsmarthome.entity.RoomType;
import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.SwitchType;
import com.gsmarthome.entity.Tower;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.DeviceDetailsRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.InstallationConfigurationRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.SwitchTypeRepository;
import com.gsmarthome.repository.TowerRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.HomeService;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.service.SwitchService;
import com.gsmarthome.service.UserDetailsService;

@RestController
@RequestMapping(value = "/home")
public class HomeController {

	static Logger logger = Logger.getLogger(HomeController.class.getName());
	
	@Autowired
	NotificationService notificationService;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	HomeService homeService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	SwitchTypeRepository switchTypeRepository;

	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Autowired
	DeviceDetailsRepository deviceDetailsRepository;

	@Autowired
	CommandRepository commandRepository;

	@Autowired
	SwitchService switchService;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	IOTProductRepository iotProductRepository;
	
	@Autowired
	TowerRepository towerRepository;

	@Autowired
	InstallationConfigurationRepository installationConfigurationRepository;

	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;

	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;

	@Autowired
	UserDetailsService userDetailsService;

	@RequestMapping(value = "/setlockcode", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<HomeLockCodeResponseDTO>> setLockCode(@RequestBody SwitchIdDTO switchIdDTO) {
	
		HomeLockCodeResponseDTO homeLockCodeResponseDTO = null;
		
		try {
			
			homeLockCodeResponseDTO = homeService.setLockCode(switchIdDTO);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(homeLockCodeResponseDTO != null){
			return new ResponseEntity<ResponseDTO<HomeLockCodeResponseDTO>>(new ResponseDTO<HomeLockCodeResponseDTO>(API_STATUS_SUCCESS, homeLockCodeResponseDTO), HttpStatus.OK);
		}else{
			return new ResponseEntity<ResponseDTO<HomeLockCodeResponseDTO>>(new ResponseDTO<HomeLockCodeResponseDTO>(API_STATUS_FAILURE, homeLockCodeResponseDTO), HttpStatus.BAD_REQUEST);			
		}
	}
	
	/* Purpose TO add New home */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<Home> addRoom(@RequestBody HomeDTO dto) {
		UserDetails userDetails = userDetailsRepository.findOne(dto.getUserId());

		Home home = new Home(dto.getHomeName());

		home.getUserList().add(userDetails);

		Home savedHome = homeRepository.save(home);

		return new ResponseEntity<Home>(savedHome, HttpStatus.OK);
	}

	/*
	 * Purpose Get home details like switch type and Room type for that Home
	 * along with no of switches configures as we have configured 6 switches
	 * only so returning No of switch = 6
	 * 
	 */

	@RequestMapping(value = "/homedetails", method = RequestMethod.GET)
	public ResponseEntity<HomeDetailsResponseDTO> homedetails() {
		
		//List<Switch> switchList = (List<Switch>) switchRepository.findAll();
		//List<Room> roomList = (List<Room>) roomRepository.findAll();

		List<IOTProduct> iotProduct = 	(List<IOTProduct>)iotProductRepository.findAll();
		
		HomeDetailsResponseDTO homeDetailsResponseDTO = new HomeDetailsResponseDTO();

		List<RoomAndSwitchResponseDTO> roomAndSwitchResponseDTOList = new ArrayList<RoomAndSwitchResponseDTO>();
		List<Switch> switchArrayList = new ArrayList<Switch>();
		
		Switch switchList1[] = null;
		
		for(int i = 0; i < iotProduct.size(); i++){
		
			RoomAndSwitchResponseDTO roomAndSwitchResponseDTO = new RoomAndSwitchResponseDTO();
			
			//switchList1 = switchRepository.getSwitchByIOTProductId(iotProduct.get(i).getId());

			roomAndSwitchResponseDTO.setRoomId(iotProduct.get(i).getRoom().getId().toString());
			roomAndSwitchResponseDTO.setRoomName(iotProduct.get(i).getRoom().getRoomName());
			
			for(int j = 0; j < switchList1.length; j++){
				switchArrayList.add(switchList1[j]);
			}

		roomAndSwitchResponseDTOList.add(roomAndSwitchResponseDTO);

		}	
		//homeDetailsResponseDTO.setRoomList(roomList);
		homeDetailsResponseDTO.setSwitchList(switchArrayList);
		homeDetailsResponseDTO.setNoofswitchs("6");
		return new ResponseEntity<HomeDetailsResponseDTO>(homeDetailsResponseDTO, HttpStatus.OK);
	}


	/*
	 * Purpose : as per the deviceid parameter check the device exist or not and
	 * is device is allocated to respective user only.
	 * 
	 */

	@RequestMapping(value = "/homeverification", method = RequestMethod.POST)
	public ResponseEntity<HomeVerificationResponseDTO> homeIdVerification(
			@RequestBody HomeidVerificationDTO homeidVerificationDTO) {
		
		
		DeviceDetails deviceDetails = deviceDetailsRepository.findBydeviceId(homeidVerificationDTO.getHomeId());
		
		logger.info("homeIdVerification method, Device Details find by device id fetched");
		
		if (deviceDetails != null) {
			UserDetails userDetailsById = userDetailsRepository.findOne(deviceDetails.getAdminUser());
			HomeVerificationResponseDTO homeVerificationResponseDTO = new HomeVerificationResponseDTO();
		
			EmailDTO emailDTO = new EmailDTO();
			
			emailDTO.setEmail(userDetailsById.getEmail().toString());
			String Otp = userDetailsService.sendHomeVerificationOTP(emailDTO);
			homeVerificationResponseDTO.setDeviceDetails(deviceDetails);
			homeVerificationResponseDTO.setOtpcode(Otp);
			homeVerificationResponseDTO.setStatus(API_STATUS_SUCCESS);
			homeVerificationResponseDTO.setUsername(userDetailsById.getFirstName() + "  " + 
													userDetailsById.getLastName());
			homeVerificationResponseDTO.setUserDetails(userDetailsById);
			homeVerificationResponseDTO.setHomeId(userDetailsById.getHome().getId().toString());
			
			return new ResponseEntity<HomeVerificationResponseDTO>(homeVerificationResponseDTO, HttpStatus.OK);
		} else {
			
			HomeVerificationResponseDTO homeVerificationResponseDTO = new HomeVerificationResponseDTO();
			homeVerificationResponseDTO.setDeviceDetails(null);
			homeVerificationResponseDTO.setOtpcode(null);
			homeVerificationResponseDTO.setStatus(API_STATUS_FAILURE);
			homeVerificationResponseDTO.setUsername(null);

			return new ResponseEntity<HomeVerificationResponseDTO>(homeVerificationResponseDTO, HttpStatus.OK);
		}
	}

	/*****************************************************************************
	 * Method name: verifyGenieHub
	 * 
	 * Parameters: VerifyGenieHubReqDTO JSON POJO
	 * 
	 * Description: 
	 * 
	 * Response: VerifyGenieHubDTO Object
	 * 
	 * 
	 *****************************************************************************/

	@RequestMapping(value = "/verifygohub", method = RequestMethod.POST)
	public ResponseEntity<VerifyGenieHubDTO> verifyGenieHub(@RequestBody VerifyGenieHubReqDTO verifyGenieHubReqDTO) {
		try {
			System.out.println("INNN ");
			VerifyGenieHubDTO objverifygeniehubDTO= null;
			Home home = homeRepository.findOne(verifyGenieHubReqDTO.getHomeId());
			
			if(home != null){
				objverifygeniehubDTO = new VerifyGenieHubDTO();
				objverifygeniehubDTO.setResponse("GenieHub "+verifyGenieHubReqDTO.getHomeId()+" is now connected");
				objverifygeniehubDTO.setStatusCode("1");
				objverifygeniehubDTO.setStatus(API_STATUS_SUCCESS);
				return new ResponseEntity<VerifyGenieHubDTO>(objverifygeniehubDTO, HttpStatus.OK);
			}else{
				objverifygeniehubDTO = new VerifyGenieHubDTO();
				objverifygeniehubDTO.setResponse("GenieHub "+verifyGenieHubReqDTO.getHomeId()+" is not Present");
				objverifygeniehubDTO.setStatusCode("0");
				objverifygeniehubDTO.setStatus(API_STATUS_SUCCESS);
				return new ResponseEntity<VerifyGenieHubDTO>(objverifygeniehubDTO, HttpStatus.OK);
			}
			
		} catch (Exception ex) {
			VerifyGenieHubDTO objverifygeniehubDTO = new VerifyGenieHubDTO();
			objverifygeniehubDTO.setResponse("GenieHub "+verifyGenieHubReqDTO.getHomeId()+" is not connected");
			objverifygeniehubDTO.setStatusCode("0");
			objverifygeniehubDTO.setStatus(API_STATUS_FAILURE);
			return new ResponseEntity<VerifyGenieHubDTO>(objverifygeniehubDTO, HttpStatus.OK);
		}
	}

	/*
	 * 
	 * Purpose :To get User information from device details
	 * 
	 */

	@RequestMapping(value = "/findBydeviceId", method = RequestMethod.POST)
	public ResponseEntity<UserdetailsResponseDTO> findBydeviceId(@RequestBody deviceDetailsDTO d) {
		DeviceDetails devicedetails = deviceDetailsRepository.findBydeviceId(d.getDeviceId().toString());
		UserDetails userDetails = userDetailsRepository.findOne(devicedetails.getAdminUser());
		UserdetailsResponseDTO userDetailsResponseDTO = new UserdetailsResponseDTO();
		if (devicedetails != null) {
			userDetailsResponseDTO.setId(userDetails.getId());
			userDetailsResponseDTO.setFirstName(userDetails.getFirstName());
			userDetailsResponseDTO.setLastName(userDetails.getLastName());
			userDetailsResponseDTO.setEmail(userDetails.getEmail());
			userDetailsResponseDTO.setImage(userDetails.getImage());
			userDetailsResponseDTO.setPhoneNumber(userDetails.getPhoneNumber());
			userDetailsResponseDTO.setDeviceId(userDetails.getDeviceId());
			userDetailsResponseDTO.setDeviceType(userDetails.getDeviceType());
			userDetailsResponseDTO.setHomeid(userDetails.getHome().getId());
			userDetailsResponseDTO.setIsEmailVerified(userDetails.getIsEmailVerified());
			userDetailsResponseDTO.setIsFirstLogin(userDetails.getIsFirstLogin());
			userDetailsResponseDTO.setUserType(userDetails.getUserType());
			userDetailsResponseDTO.setPassword(userDetailsResponseDTO.getPassword());
		}
		return new ResponseEntity<UserdetailsResponseDTO>(userDetailsResponseDTO, HttpStatus.OK);
	}

	/*
	 * 
	 * Used to fill )tables sequentially as :- Panel -> Room Type->Switch Type
	 * ->Sync User_details and Home , Device_detail table with AWS database then
	 * iot product -> Room->Switch Here we provide a input parameters as follow :
	 * topic-room identifier-panel id-switch configuration-device
	 * id,topic-room identifier-panel id-switch configuration-device id,......
	 * 
	 * Parameter :{"panel list":r
	 * "panel1-BED1-11-123456-0kx1d,panel2-BED2-12-120056-0kx1d,panel3-HAL1-13-123456-0kx1d,panel4-KIT1-14-103456-0kx1d,panel5-BAL1-15-123400-0kx1d,panel6-BAT1-16-100000-0kx1d,panel7-WAI1-17-123000-0kx1d,panel8-STU1-18-103450-0kx1d,panel9-KID1-19-123450-0kx1d",
	 * "ipConfigration":
	 * "BED1-11-123456-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel1,panel1/in,panel1/out,192.168.0.119,BED2-12-120056-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel2,panel2/in,panel2/out,192.168.0.119,HAL1-13-123456-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel3,panel3/in,panel3/out,192.168.0.119,KIT1-14-103456-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel4,panel4/in,panel4/out,192.168.0.119,BAL1-15-123400-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel5,panel5/in,panel5/out,192.168.0.119,BAT1-16-100000-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel6,panel6/in,panel6/out,192.168.0.119,WAI1-17-123000-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel7,panel7/in,panel7/out,192.168.0.119,STU1-18-103450-GTX23,Genie SmartHome,w72i61m1,safeSSID,safePass,panel8,panel8/in,panel8/out,192.168.0.119,KID1-19-123450-0kx1d,Genie SmartHome,w72i61m1,safeSSID,safePass,panel9,panel9/in,panel9/out,192.168.0.119,"
	 * }
	 * 
	 * 
	 */
	
	@RequestMapping(value = "/fillPanel", method = RequestMethod.POST)
	public ResponseEntity<VerifyGenieHubDTO> fillPanel(@RequestBody panelListDTO getpanel) throws MqttException {

		Panel panel = new Panel();
		Panel newPanel = new Panel();
		
		UserDetails userDetails = new UserDetails();
		List<String> allPanel = new ArrayList<String>();
		String ipConfigration = getpanel.getIpConfigration().toString();
		ArrayList<String> ipConfigrationdetails = new ArrayList<String>(Arrays.asList(ipConfigration.split(",")));
		
		InstallationConfiguration installationConfiguration = new InstallationConfiguration(ipConfigration);
		
		installationConfiguration.setSecurePassword(ipConfigrationdetails.get(4));
		installationConfiguration.setSecureSSId(ipConfigrationdetails.get(3));
		
		installationConfigurationRepository.save(installationConfiguration);
		
		try {
			ArrayList<String> towerInfo = new ArrayList<String>(Arrays.asList(getpanel.getTowerInfo().split("-")));
			logger.info("fillPanel method, Tower info fetched from DTO. User ID:"+getpanel.getId());
			
			if(!towerInfo.equals(Constants.NOT_APPLICABLE)){
				for(int i = 0; i < towerInfo.size(); i++){
					Tower towerObj = new Tower();
					towerObj.setTowerinfo(towerInfo.get(i));
	
					towerRepository.save(towerObj);
					logger.info("Fill Panel method, Tower info saved in DB. User Id:"+getpanel.getId());
				}
			}
			
			
			String panelList = getpanel.getPanellist().toString();

			ArrayList<String> pList = new ArrayList<String>(Arrays.asList(panelList.split(",")));
			
			for (int i = 0; i < pList.size(); i++) {
				
				logger.info("Fill Panel Method: PANEL Command: " + pList.get(i));
				
				String[] panelRoomConfig = pList.get(i).toString().split("-");

				long roomTypeid = 1;
				allPanel.add(panelRoomConfig[0].toString());

				int panelId = Integer.parseInt(panelRoomConfig[2].toString());
				panel = panelRepository.findByPanelId(panelId);

				if (panel == null) {

					newPanel.setNotImplimented("NA");
					newPanel.setPanelCommand(pList.get(i).toString());
					newPanel.setPanelId(Integer.parseInt(panelRoomConfig[2].toString()));
					newPanel.setPanelName(panelRoomConfig[0].toString());
					newPanel.setPanelIp("");
					panelRepository.save(newPanel);
					panel = newPanel;
				} else {
					// panel already exist
					panel.setNotImplimented("NA");
					panel.setPanelCommand(pList.get(i).toString());
					panel.setPanelId(Integer.parseInt(panelRoomConfig[2].toString()));
					panel.setPanelName(panelRoomConfig[0].toString());
					panel.setPanelIp("");
					panelRepository.save(panel);
					
				}

				//check home already exist or not
				Home chkHome = new Home();
				Room room = new Room();

				DeviceDetails deviceDetails = new DeviceDetails();

				deviceDetails = deviceDetailsRepository.findBydeviceId(panelRoomConfig[4].toString());
				
				// device information needs to add in device details table
				if (deviceDetails == null) {
					deviceDetails = new DeviceDetails();
					deviceDetails.setDeviceId(panelRoomConfig[4].toString());
					deviceDetails = deviceDetailsRepository.save(deviceDetails);
				}
				
				if (deviceDetails.getAdminUser() == null) {
					/*RestTemplate restTemplate1 = new RestTemplate();
					deviceDetailsDTO deviceDetailsDTO = new deviceDetailsDTO();
					deviceDetailsDTO.setDeviceId(panelRoomConfig[4].toString());

					String strUserDetails = restTemplate1
							.postForObject("http://geniecentraltest.genieiot.com:8080/GSmart_geniecentraltest/home/findBydevice"
									+ "Id", deviceDetailsDTO, String.class);
					System.out.println(strUserDetails);
					// JSON from String to Object
					ObjectMapper objMapper = new ObjectMapper();
					UserdetailsResponseDTO userDetailsResponseDTO = objMapper.readValue(strUserDetails, UserdetailsResponseDTO.class);*/

					Home home = new Home();
					home.setId(getpanel.getHomeId());
					home.setHomeName(getpanel.getFirstName() + "s Home");
					home.setIsArmed(0);
					home.setIsBlocked(0);
					
					homeRepository.save(home);

					UserDetails newUser = new UserDetails();

					Home home1 = new Home();
					home1 = homeRepository.findOne(home.getId());

					try{
						
						newUser.setId(getpanel.getId());
						newUser.setFirstName(getpanel.getFirstName());
						newUser.setLastName(getpanel.getLastName());
						newUser.setEmail(getpanel.getEmail());
						newUser.setPhoneNumber(getpanel.getPhoneNumber());
						newUser.setDeviceId(getpanel.getDeviceId());
						newUser.setDeviceType(getpanel.getDeviceType());
						newUser.setHome(home1);
						newUser.setBirthDate(getpanel.getBirthDate());
						newUser.setIsEmailVerified(getpanel.getIsEmailVerified());
						newUser.setIsFirstLogin(getpanel.getIsFirstLogin());
						newUser.setUserType(getpanel.getUserType());
						newUser.setPassword(getpanel.getPassword());
						
						userDetailsRepository.save(newUser);

					}catch (Exception ex) {
						logger.error("Fill Panel Method, Exception Message: "+ex.getMessage()+" Cause: "+ex.getCause());
						ex.printStackTrace();	
					}
					
					DeviceDetails dnew = new DeviceDetails();
					dnew.setDeviceId(panelRoomConfig[4].toString());
					dnew.setAdminUser(getpanel.getId());
					deviceDetailsRepository.save(dnew);
					
					long roomId = 1;
					RoomType r = new RoomType();
					r.setId(roomId);
					r.setRoomTypeName("BedRoom");
					roomTypeRepository.save(r);

					RoomType r1 = new RoomType();
					r1.setId(++roomId);
					r1.setRoomTypeName("Hall");
					roomTypeRepository.save(r1);

					RoomType r2 = new RoomType();
					r2.setId(++roomId);
					r2.setRoomTypeName("Kitchen");
					roomTypeRepository.save(r2);

					RoomType r3 = new RoomType();
					r3.setId(++roomId);
					r3.setRoomTypeName("Room_A");
					roomTypeRepository.save(r3);

					RoomType r4 = new RoomType();
					r4.setId(++roomId);
					r4.setRoomTypeName("Room_B");
					roomTypeRepository.save(r4);

					RoomType r5 = new RoomType();
					r5.setId(++roomId);
					r5.setRoomTypeName("Room_C");
					roomTypeRepository.save(r5);

					long switchTypeId = 1;

					SwitchType switchType1 = new SwitchType();
					switchType1.setId(switchTypeId);
					switchType1.setSwitchTypeName("simple1");
					switchType1.setDimmerStatus("0");
					switchTypeRepository.save(switchType1);

					SwitchType switchType2 = new SwitchType();
					switchType2.setId(++switchTypeId);
					switchType2.setSwitchTypeName("power");
					switchType2.setDimmerStatus("0");
					switchTypeRepository.save(switchType2);

					SwitchType switchType3 = new SwitchType();
					switchType3.setId(++switchTypeId);
					switchType3.setSwitchTypeName("simple2");
					switchType3.setDimmerStatus("0");
					switchTypeRepository.save(switchType3);

					SwitchType switchType4 = new SwitchType();
					switchType4.setId(++switchTypeId);
					switchType4.setSwitchTypeName("socket");
					switchType4.setDimmerStatus("0");
					switchTypeRepository.save(switchType4);

					SwitchType switchType5 = new SwitchType();
					switchType5.setId(++switchTypeId);
					switchType5.setSwitchTypeName("Dimmer1");
					switchType5.setDimmerStatus("0");
					switchTypeRepository.save(switchType5);

					SwitchType switchType6 = new SwitchType();
					switchType6.setId(++switchTypeId);
					switchType6.setSwitchTypeName("Dimmer2");
					switchType6.setDimmerStatus("0");
					switchTypeRepository.save(switchType6);
				}
				
				long userid = deviceDetails.getAdminUser();

				userDetails = userDetailsRepository.findOne(userid);

				chkHome = homeRepository.findOne(userDetails.getHome().getId());

				// insert into Room table
				RoomType roomType = new RoomType();
				Room newRoom = new Room();

				roomType = roomTypeRepository.findOne(roomTypeid);

				room = roomRepository.findRoomByroomIdentifier(panelRoomConfig[1]);
				
				if (room == null) {
					
						String roomname = panelRoomConfig[1].toUpperCase();
						String mapRoomName = "";
						switch(roomname)
						{
							case "TO" :
								mapRoomName="Toilet";
								break;
							case "BA" :
								mapRoomName="Balcony";
								break;
							case "SR" :
								mapRoomName="Store room";
								break;
							case "RO" :
								mapRoomName="Rooms";
								break;
							case "OS" :
								mapRoomName="Outside space";
								break;
							case "TE" :
								mapRoomName="Terrace";
								break;
							case "ST" :
								mapRoomName="Stage";
								break;
							case "LF" :
								mapRoomName="Lift";
								break;
							case "GR" :
								mapRoomName="Guest room";
								break;
							case "PO" :
								mapRoomName="Porch";
								break;
							case "GA" :
								mapRoomName="Garden";
								break;
							case "PS" :
								mapRoomName="Parking space";
								break;
							case "GE" :
								mapRoomName="Garrage";
								break;
							case "BD1" :
								mapRoomName="Bedroom1";
								break;
							case "BD2" :
								mapRoomName="Bedroom2";
								break;
							case "BD3" :
								mapRoomName="Bedroom3";
								break;	
							case "HA1" :
								mapRoomName="Hall1";
								break;	
							case "HA2" :
								mapRoomName="Hall2";
								break;	
							case "HA3" :
								mapRoomName = "Hall3";
								break;	
							case "KT" :
								mapRoomName = "Kitchen";
								break;	
							case "ET":
								mapRoomName = "Entrance";
								break;
							case "RC":
								mapRoomName = "Reception";
								break;
							case "LB":
								mapRoomName = "Lobby";
								break;
							case "MD":
								mapRoomName = "Marketing Department";
								break;					
							case "OC":
								mapRoomName = "Office";
								break;
							case "SF":
								mapRoomName = "Second Floor";
								break;
							case "CH":
								mapRoomName = "Conference Hall";
								break;
							default: mapRoomName = "Other Room";						
						}	
						newRoom.setRoomName(mapRoomName);
						newRoom.setRoomType(roomType.getId().toString());
						newRoom.setRoomIdentifier(panelRoomConfig[1]);
						newRoom.setHome(chkHome);
						newRoom.setRoomImageId(0);
						newRoom.setHideStatus("0");
						
						room = roomRepository.save(newRoom);
					
				}
				// new room added and filled the room object with value
				// or taken room details from existing table
				// but room data with user
				if (room != null) {
					IOTProduct iotProduct = new IOTProduct();
					iotProduct.setProductName(panelRoomConfig[3].toString());
					iotProduct.setProductType(panelRoomConfig[6]);
					iotProduct.setIotProductNumber(Integer.parseInt(panelRoomConfig[2].toString()));
					iotProduct.setRoom(room);
					IOTProduct chkIOTProduct = new IOTProduct();
					
					Switch chkSwitch = new Switch();

					chkIOTProduct = iotProductRepository.findByiotProductNumber(Integer.parseInt(panelRoomConfig[2].toString()));
					if (chkIOTProduct == null) {
						chkIOTProduct = iotProductRepository.save(iotProduct);
					}
					
					// now add iotProduct details
					if (chkIOTProduct != null) {
						String switches = panelRoomConfig[3].toString();

						for (int index = 0; index < switches.length(); index++) {
							if (!Character.toString(switches.charAt(index)).equals("0")) {
								
								if (index < 4) {
									
									try {

										chkSwitch = switchRepository.checkSwitchAlreadyExist(
												Integer.parseInt(Character.toString(switches.charAt(index))),
												chkIOTProduct.getId());

										logger.info("********************Switch Character:*****************"+Character.toString(switches.charAt(index)));
										
									} catch (Exception ex) {
										ex.printStackTrace();
										logger.error("Exception Message: "+ex.getMessage().toString()+" Cause: "+ex.getCause());
									}
									if (chkSwitch == null) {
										int Switchtypecount = index + 1;
										Switch s1 = new Switch();
										
										s1.setSwitchName("Bulb");

										//C1- 12 are curtain Switch, C2- 23 are curtain Switch, C3 - 34 are Curtain switches
										//for 1 and 2 Switch only
										switch(panelRoomConfig[5].toString()){
										case "gc":
											s1.setDimmerStatus(Constants.GENIE_COLOUR_STATUS);
											s1.setSwitchIdentifier("gc");
											s1.setSwitchName("Genie color");
											break;
										case "c1":
											if(index < 2){
												s1.setDimmerStatus(Constants.GENIE_CURTAIN_STATUS);
												s1.setSwitchIdentifier("c1");
												s1.setSwitchName("Curtain");
											}else{
												s1.setDimmerStatus("0");
											}
											break;
										case "c2":	//for 2 and 3 switch 
											if(index >= 2 && index <= 3){
												s1.setDimmerStatus(Constants.GENIE_CURTAIN_STATUS);
												s1.setSwitchIdentifier("c2");
												s1.setSwitchName("Curtain");
											}else{
												s1.setDimmerStatus("0");
											}
											break;
										case "c3":	//for 34 switches
											if(index >= 2){
												s1.setDimmerStatus(Constants.GENIE_CURTAIN_STATUS);
												s1.setSwitchIdentifier("c3");
												s1.setSwitchName("Curtain");
											}else{
												s1.setDimmerStatus("0");
											}
											break;
										case "cc":	//for 1234 switches
											s1.setDimmerStatus(Constants.GENIE_CURTAIN_STATUS);
											s1.setSwitchIdentifier("cc");
											s1.setSwitchName("Curtain");
											break;
										case "L000":
											if(index < 1){
												if(panelRoomConfig[7].equals("1")){
													s1.setDimmerStatus(Constants.GENIE_MAIN_LOCK_STATUS);
													s1.setSwitchIdentifier("L000");
													s1.setSwitchName("Home Lock");
												}else{
													s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
													s1.setSwitchIdentifier("L000");	
													s1.setSwitchName("Lock");
												}
											}else{
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}
											break;
										case "LL00":
											if(index < 2){
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("LL00");
												s1.setSwitchName("Lock");
											}else{
												s1.setDimmerStatus("0");
											}											
											break;
										case "LLL0":
											if(index < 3){
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("LLL0");
												s1.setSwitchName("Lock");
											}else{
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}											
											break;
										case "LLLL":
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("LLLL");
												s1.setSwitchName("Lock");
											break;
										case "0LLL":
											if(index < 1){
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}else{
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("0LLL");
												s1.setSwitchName("Lock");
											}
											break;
										case "00LL":
											if(index < 2){
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}else{
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("00LL");
												s1.setSwitchName("Lock");
											}
											break;
										case "000L":
											if(index < 3){
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}else{
												s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
												s1.setSwitchIdentifier("000L");
												s1.setSwitchName("Lock");
											}
											break;
										case "00L0":
											if(index == 2){
												if(panelRoomConfig[7].equals("1")){
													s1.setDimmerStatus(Constants.GENIE_MAIN_LOCK_STATUS);
													s1.setSwitchIdentifier("00L0");	
													s1.setSwitchName("Home Lock");
												}else{
													s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
													s1.setSwitchIdentifier("00L0");
													s1.setSwitchName("Lock");
												}
											}else{
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);												
											}
											break;
										case "0L00":
											if(index == 1){
												if(panelRoomConfig[7].equals("1")){
													s1.setDimmerStatus(Constants.GENIE_MAIN_LOCK_STATUS);
													s1.setSwitchIdentifier("0L00");
													s1.setSwitchName("Home Lock");
												}else{
													s1.setDimmerStatus(Constants.GENIE_LOCK_STATUS);
													s1.setSwitchIdentifier("0L00");
													s1.setSwitchName("Lock");
												}

											}else{
												s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
											}
											break;
										default:
											s1.setDimmerStatus(Constants.GENIE_SWITCH_STATUS);
										}
										
										s1.setDimmerValue("0");
										s1.setLockStatus("0");
										s1.setSwitchStatus("0");
										s1.setHideStatus("0");
										s1.setSwitchImageId(0);
										s1.setSwitchType(Switchtypecount + "");

										s1.setSwitchNumber(
												Integer.parseInt(Character.toString(switches.charAt(index))));
										s1.setIotProduct(chkIOTProduct);
										switchRepository.save(s1);
										s1 = null;
									}
								} else {
									chkSwitch = switchRepository.checkSwitchAlreadyExist(
											Integer.parseInt(Character.toString(switches.charAt(index))),
											chkIOTProduct.getId());
									if (chkSwitch == null) {
										int Switchtypecount = index + 1;

										Switch s2 = new Switch();
										s2.setSwitchName("Fan");
										s2.setDimmerStatus("1");
										s2.setDimmerValue("20");
										s2.setLockStatus("0");
										s2.setSwitchNumber(
												Integer.parseInt(Character.toString(switches.charAt(index))));
										s2.setIotProduct(chkIOTProduct);
										s2.setSwitchStatus("0");
										s2.setHideStatus("0");
										s2.setSwitchType(Switchtypecount + "");
										s2.setSwitchImageId(0);
										switchRepository.save(s2);
										s2 = null;
									}
								}
							}
						}
					}
				}
				roomTypeid++;

			}

		} catch (Exception ex) {

			VerifyGenieHubDTO objverifygeniehubDTO = new VerifyGenieHubDTO();		 
			objverifygeniehubDTO.setResponse("Error while configuring GenieHub");
			objverifygeniehubDTO.setStatus(API_STATUS_SUCCESS);

			return new ResponseEntity<VerifyGenieHubDTO>(objverifygeniehubDTO, HttpStatus.OK);

		}
	
		VerifyGenieHubDTO objVerifyGenieHubDTO = new VerifyGenieHubDTO();
		objVerifyGenieHubDTO.setPanelDetails(getpanel.getPanellist());
		objVerifyGenieHubDTO.setRouterSSID(ipConfigrationdetails.get(1).toString());
		objVerifyGenieHubDTO.setRouterPassword(ipConfigrationdetails.get(2).toString());
		objVerifyGenieHubDTO.setSecureSSID(ipConfigrationdetails.get(3).toString());
		objVerifyGenieHubDTO.setSecurePassword(ipConfigrationdetails.get(4).toString());
		objVerifyGenieHubDTO.setResponse("GenieHub is now configured");
		objVerifyGenieHubDTO.setStatus(API_STATUS_SUCCESS);
		return new ResponseEntity<VerifyGenieHubDTO>(objVerifyGenieHubDTO, HttpStatus.OK);

	}

	@RequestMapping(value = "/getactivitylist", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<ActivityResponseDTO>>> getActivityList(@RequestBody ActivityDTO activityDTO) {
		List<ActivityResponseDTO> activityResponseDTO = notificationService.getActivityList(activityDTO);
		return new ResponseEntity<ResponseDTO<List<ActivityResponseDTO>>>(
				new ResponseDTO<List<ActivityResponseDTO>>(API_STATUS_SUCCESS, activityResponseDTO), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getinstallationinfo", method = RequestMethod.GET)
	public ResponseEntity<VerifyGenieHubDTO>  getInstallationInfo() {
		//InstallationConfiguration  installationConfiguration[]=installationConfigurationRepository.getInstallationConfiguration();
		
		List<InstallationConfiguration> installationConfiguration1 = (List<InstallationConfiguration>)installationConfigurationRepository.findAll();
		int total = installationConfiguration1.size();
		ArrayList<String> ipConfigrationdetails = new ArrayList<String>(Arrays.asList(installationConfiguration1.get(total-1).getConfigurationString().split(",")));

		//ArrayList ipConfigrationdetails=new ArrayList();
		
		VerifyGenieHubDTO objVerifyGenieHubDTO = new VerifyGenieHubDTO();
		
		List<Panel> panelist=(List<Panel>)panelRepository.findAll();
		String allpanel="";
		
		for(int i=0;i<panelist.size();i++)
		{
			allpanel+=panelist.get(i).getPanelCommand().toString() + ",";
		}
		
		objVerifyGenieHubDTO.setPanelDetails(allpanel.toString());
		objVerifyGenieHubDTO.setRouterSSID(ipConfigrationdetails.get(1));
		objVerifyGenieHubDTO.setRouterPassword(ipConfigrationdetails.get(2));
		objVerifyGenieHubDTO.setSecureSSID(ipConfigrationdetails.get(3));
		objVerifyGenieHubDTO.setSecurePassword(ipConfigrationdetails.get(4));
		objVerifyGenieHubDTO.setResponse("GenieHub is now configured");
		objVerifyGenieHubDTO.setStatus(API_STATUS_SUCCESS);
		
		//List<ActivityResponseDTO> activityResponseDTO = notificationService.getActivityList();
		
		return new ResponseEntity<VerifyGenieHubDTO>(objVerifyGenieHubDTO, HttpStatus.OK);
	}
	
	/******************************************************************************************
	 * Method Name: armHome
	 * 
	 * Parameter: JSON Object with HomeDTO
	 * 
	 * Description: this method will arm the home whenever user wants their home to be on safe 
	 * mode, by setting arm the user will start getting notification from whenever any user 
	 * tries to switch ON or OFF the lights or Fan physically at home.
	 * 
	 * Response: HomeArmANDBlockResponseDTO Object
	 * 
	 * ****************************************************************************************/
	
	@RequestMapping(value = "/armhome", method = RequestMethod.POST)
	public ResponseEntity<HomeArmANDBlockResponseDTO>  armHome(@RequestBody HomeDTO homeDTO) {
		
		HomeArmANDBlockResponseDTO homeArm  = null;
		
		try {
			homeArm = homeService.armHome(homeDTO);
		} catch (Exception e) {
			logger.info("Exception while home Arm update");
			e.printStackTrace();
			homeArm = new HomeArmANDBlockResponseDTO();
			homeArm.setStatus(Constants.FAILURE_MSG);
			return new ResponseEntity<HomeArmANDBlockResponseDTO>(homeArm, HttpStatus.BAD_REQUEST);
		}
		
		if(homeArm != null){
			return new ResponseEntity<HomeArmANDBlockResponseDTO>(homeArm, HttpStatus.OK);
		}else{
			homeArm = new HomeArmANDBlockResponseDTO();
			homeArm.setStatus(Constants.FAILURE_MSG);
			return new ResponseEntity<HomeArmANDBlockResponseDTO>(homeArm, HttpStatus.BAD_REQUEST);
		}
	}

}
