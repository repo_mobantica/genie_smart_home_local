package com.gsmarthome.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.BackupRequestConfigDTO;
import com.gsmarthome.dto.BackupResponseConfigDTO;
import com.gsmarthome.dto.EmailDTO;
import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.UpdatePasswordDTO;
import com.gsmarthome.dto.UpdatePasswordOTPDTO;
import com.gsmarthome.dto.UpdateUserTypeDTO;
import com.gsmarthome.dto.UserIdDTO;
import com.gsmarthome.dto.UserListDTO;
import com.gsmarthome.dto.UserShareControlDTO;
import com.gsmarthome.dto.UserdetailsResponseDTO;
import com.gsmarthome.dto.ValidateUserDTO;
import com.gsmarthome.dto.VerifyEmailOTP;
import com.gsmarthome.dto.VerifyOTPDTO;
import com.gsmarthome.dto.VerifyOTPResponse;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.service.UserDetailsService;
import com.gsmarthome.dto.UpdateProfileDTO;


@RestController
@RequestMapping(value="/user")
public class UserDetailsController {

    @Autowired
    UserDetailsService userDetailsService;   
	
	@Autowired
	NotificationService notificationService;
    
    @Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
    
    @Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
    
    //User details information will get saved in User_details table
    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserDetails>> save(@Valid @RequestBody UserDetails newUserDetails, BindingResult bindingResult) {
    	System.out.println("INNNNNNNNNN");
    	ResponseDTO<UserDetails> savedNewUser = userDetailsService.save(newUserDetails,false,bindingResult);
        return new ResponseEntity<ResponseDTO<UserDetails>>(savedNewUser,HttpStatus.OK);
    }
    
    
    @RequestMapping(value="/updatepassword",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updatePassword(@Valid @RequestBody UpdatePasswordDTO updatePasswordDTO,BindingResult bindingResult) {
    	ResponseDTO<Boolean> savedCustomer=userDetailsService.updatePassword(updatePasswordDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
        
    @RequestMapping(value="/updatepasswordOTP",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updatePasswordOTP(@Valid @RequestBody UpdatePasswordOTPDTO updatePasswordDTO,BindingResult bindingResult) {
    	
    	   	   	
    	ResponseDTO<Boolean> savedCustomer=userDetailsService.updatePasswordOTP(updatePasswordDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/sendOTP",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<UserIdDTO>> sendOTP(@Valid @RequestBody EmailDTO emailDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<UserIdDTO> savedCustomer = userDetailsService.sendOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<UserIdDTO>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/verifyOTP",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<VerifyOTPResponse>> verifyOTP(@Valid @RequestBody VerifyOTPDTO emailDTO,BindingResult bindingResult) {
    	
    	   	   	
    	ResponseDTO<VerifyOTPResponse> savedCustomer=userDetailsService.verifyOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<VerifyOTPResponse>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/verifyEmailOTP",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> verifyEmailOTP(@Valid @RequestBody VerifyEmailOTP emailDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<Boolean> savedCustomer=userDetailsService.verifyEmailOTP(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/validateUser",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Integer>> validateUser(@Valid @RequestBody ValidateUserDTO emailDTO,BindingResult bindingResult) {
    	   	   	
    	ResponseDTO<Integer> savedCustomer=userDetailsService.validateUser(emailDTO);
    	
        return new ResponseEntity<ResponseDTO<Integer>>(savedCustomer,HttpStatus.OK);
    }
    
    //add activity log for this
    @RequestMapping(value="/shareControl",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> shareControl(@Valid @RequestBody UserShareControlDTO shareControlDTO, BindingResult bindingResult) {
    	
    	   	   	
    	ResponseDTO<Boolean> savedCustomer = userDetailsService.userShareControl(shareControlDTO,bindingResult);
    	
    	return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
    @RequestMapping(value="/list",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<List<UserListDTO>>> userList(@Valid @RequestBody UserIdDTO userIdDTO,BindingResult bindingResult) {
        	ResponseDTO<List<UserListDTO>> savedCustomer = userDetailsService.getByUser(userIdDTO);
    	
    	return new ResponseEntity<ResponseDTO<List<UserListDTO>>>(savedCustomer,HttpStatus.OK);
    }
	
    //add activity log for this
    @RequestMapping(value="/updatetype",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updateType(@Valid @RequestBody UpdateUserTypeDTO userIdDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<Boolean> savedCustomer=userDetailsService.updateUserType(userIdDTO);
    	
    	return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    @RequestMapping(value="/updateprofile",method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO<Boolean>> updateProfile(@Valid @RequestBody UpdateProfileDTO updateProfileDTO,BindingResult bindingResult) {
    	
    	ResponseDTO<Boolean> savedCustomer=userDetailsService.updateProfile(updateProfileDTO);
    	
        return new ResponseEntity<ResponseDTO<Boolean>>(savedCustomer,HttpStatus.OK);
    }
    
   @RequestMapping(value="/syncShareControlData",method=RequestMethod.POST)
   public ResponseEntity<ResponseDTO<UserDetails>> syncShareControldata(@Valid @RequestBody UserdetailsResponseDTO customer,BindingResult bindingResult) {
	   	System.out.println("Hi in here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		ResponseDTO<UserDetails> savedCustomer=userDetailsService.syncShareControlData(customer,false,bindingResult);
    	
        return new ResponseEntity<ResponseDTO<UserDetails>>(savedCustomer,HttpStatus.OK);
    }
   
   @RequestMapping(value="/syncShareControlData2",method=RequestMethod.POST)
   public void syncShareControldata2() {
		
	   	notificationService.sendNotificationMessage("Swapnil testuing notification service");
	   	System.out.println("Hi in here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
   
   
   @RequestMapping(value="/backupconfig",method=RequestMethod.POST)
   public ResponseEntity<ResponseDTO<BackupResponseConfigDTO>> backupConfig(@Valid @RequestBody BackupRequestConfigDTO backupRequestConfigDTO,BindingResult bindingResult) {
  
	   ResponseDTO<BackupResponseConfigDTO> backupResponseConfigDTO = userDetailsService.backupConfig(backupRequestConfigDTO);

       return new ResponseEntity<ResponseDTO<BackupResponseConfigDTO>>(backupResponseConfigDTO, HttpStatus.OK);
       
   }
   
   @RequestMapping(value="/deletesharecontrol",method=RequestMethod.POST)
   public ResponseEntity<ResponseDTO<Boolean>> deleteShareControl(@Valid @RequestBody UserShareControlDTO shareControlDTO, BindingResult bindingResult) {
	   Boolean isUserDeleted = false;
	
	   try{
		isUserDeleted = userDetailsService.userDeleteShareControl(shareControlDTO,bindingResult);
	   
	   }catch(Exception e){
		   return new ResponseEntity<ResponseDTO<Boolean>>((new ResponseDTO<Boolean>(API_STATUS_FAILURE, isUserDeleted, "Exception in deleting User's Profile and Schedulers")),HttpStatus.OK);
	   }
   	
	   if(isUserDeleted)
		   return new ResponseEntity<ResponseDTO<Boolean>>((new ResponseDTO<Boolean>(API_STATUS_SUCCESS, isUserDeleted, "User's Profile and Schedulers deleted Successfully")),HttpStatus.OK);
	   else{
		   return new ResponseEntity<ResponseDTO<Boolean>>((new ResponseDTO<Boolean>(API_STATUS_FAILURE, isUserDeleted, "User's Profile and Schedulers is not Deleted")),HttpStatus.OK);
	   }
   }
   
}
