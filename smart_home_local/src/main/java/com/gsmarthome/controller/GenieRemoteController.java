package com.gsmarthome.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.RemoteIRRequestDTO;
import com.gsmarthome.service.IGenieRemoteService;
import com.gsmarthome.serviceImpl.GenieCamServiceImpl;

@RestController
@RequestMapping(value = "/remote")
public class GenieRemoteController {

	static Logger logger = Logger.getLogger(GenieCamServiceImpl.class.getName());

	@Autowired
	IGenieRemoteService genieRemoteService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	@RequestMapping(value="/irfire",method=RequestMethod.POST)
	public void commandIRFire(@RequestBody RemoteIRRequestDTO remoteIRReqDTO)
	{
		try{
			genieRemoteService.commandIRFire(remoteIRReqDTO); 
		}catch(RuntimeException e){
			logger.error("command ir fire method, User Id: "+remoteIRReqDTO.getUserId());
			e.printStackTrace();
		}
		catch (Exception e) {
			logger.error("command ir fire method, User Id: "+remoteIRReqDTO.getUserId());
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="/learnircode",method=RequestMethod.POST)
	public void learnIRCode(@RequestBody RemoteIRRequestDTO remoteIRReqDTO)
	{
		try{		
			genieRemoteService.learnIRCode(remoteIRReqDTO);			
			 
		}catch (RuntimeException e) {
			logger.error("learn ir Code method, runtime Exception User Id: "+remoteIRReqDTO.getUserId());			
			e.printStackTrace();
		}catch(Exception e){
			logger.error("learn IR Code method, Exception occured User Id: "+remoteIRReqDTO.getUserId());
			e.printStackTrace();
		}
	}
}


	
