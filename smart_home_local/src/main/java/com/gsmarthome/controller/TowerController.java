package com.gsmarthome.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsmarthome.dto.ResponseDTO;
import com.gsmarthome.dto.TowerRequestDTO;
import com.gsmarthome.entity.Tower;
import com.gsmarthome.service.TowerService;

@RestController
@RequestMapping(value="/tower")
public class TowerController {
	
	@Autowired
	TowerService towerService;
	
	@Value("${API_STATUS_SUCCESS}")
	private String API_STATUS_SUCCESS;
	
	@Value("${API_STATUS_FAILURE}")
	private String API_STATUS_FAILURE;
	
	
	@RequestMapping(value="/getalltower",method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<List<Tower>>> getAllTower(@RequestBody TowerRequestDTO towerReqDTO)
	{
	
		ResponseDTO<List<Tower>> towerList = towerService.getAllTower(towerReqDTO);		
		
		try{
			return new ResponseEntity<ResponseDTO<List<Tower>>>(towerList,HttpStatus.OK);
	
		}catch (RuntimeException e) {
			
			return new ResponseEntity<ResponseDTO<List<Tower>>>(towerList,HttpStatus.OK);

		}
	}
}
