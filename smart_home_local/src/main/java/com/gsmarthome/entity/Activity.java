package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Activity {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
/*	@ManyToOne
	@JoinColumn(name="homeId", nullable=false)
	@JsonIgnore
	private Home home;*/
	
	private String image;
	private String roomname;
	private String message;
	private String messageType;
	private String created_date;
	
	private String messageFrom;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String string) {
		this.created_date = string;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRoomname() {
		return roomname;
	}
	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}
	public String getMessageFrom() {
		return messageFrom;
	}
	
	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	@Override
	public String toString() {
		return "Activity [id=" + id + ", image=" + image + ", roomname=" + roomname + ", message=" + message
				+ ", messageType=" + messageType + ", created_date=" + created_date + ", messageFrom=" + messageFrom
				+ "]";
	}
	 
}
