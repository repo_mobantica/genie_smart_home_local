package com.gsmarthome.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDetails {
	
	@Id
	private Long id;
	
	@NotNull
	private String firstName;
	
	@NotNull
	private String lastName;

	@NotNull
	private String birthDate;
	
	@Email
	@NotNull
	@Column(name = "email", unique = true)
	private String email;

	private String password;

	private String phoneNumber;

	private Integer userType;

	private String image;

	private String deviceId;
	
	private String deviceType;
	
	private Boolean isEmailVerified;
	
	private Boolean isFirstLogin;
	
	@ManyToOne
	@JoinColumn(name = "HOMEID")
	private Home home;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public UserDetails() {
	
	}
	
	public UserDetails(Long id, String firstName, String lastName, String email, String password, Integer userType,
			String phoneNumber, Home home) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.userType = userType;
		this.phoneNumber = phoneNumber;
		this.home = home;
	}

	public UserDetails(Long id, String firstName, String lastName, String email, String password, String phoneNumber,
			Integer userType, String image, Home home) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.userType = userType;
		this.image = image;
		this.home = home;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public Boolean getIsFirstLogin() {
		return isFirstLogin;
	}

	public void setIsFirstLogin(Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	@Override
	public String toString() {
		return "UserDetails [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", birthDate="
				+ birthDate + ", email=" + email + ", password=" + password + ", phoneNumber=" + phoneNumber
				+ ", userType=" + userType + ", image=" + image + ", deviceId=" + deviceId + ", deviceType="
				+ deviceType + ", isEmailVerified=" + isEmailVerified + ", isFirstLogin=" + isFirstLogin + ", home="
				+ home + "]";
	}
	
}
