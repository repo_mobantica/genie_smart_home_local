package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RoomType {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String roomTypeName;

	@Lob
	private byte[] roomImage;

	public RoomType() {
		super();
	}

	public RoomType(Long id, String roomTypeName) {
		super();
		this.id = id;
		this.roomTypeName = roomTypeName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public byte[] getRoomImage() {
		return roomImage;
	}

	public void setRoomImage(byte[] roomImage) {
		this.roomImage = roomImage;
	}

}
