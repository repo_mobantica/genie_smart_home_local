package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ScheduleSwitch {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long userId;
	private Long switchId;
	private Long profileId;
	private String switchStatus;
	private String dimmerValue;
	private String dimmerStatus;
	private String lockStatus;
	private String repeatStatus;
	private String repeatWeek;
	private String scheduleDateTime;
	private String futureDateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSwitchId() {
		return switchId;
	}

	public void setSwitchId(Long switchId) {
		this.switchId = switchId;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}
	
	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}
	
	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRepeatStatus() {
		return repeatStatus;
	}

	public void setRepeatStatus(String repeatStatus) {
		this.repeatStatus = repeatStatus;
	}

	public String getRepeatWeek() {
		return repeatWeek;
	}

	public void setRepeatWeek(String repeatWeek) {
		this.repeatWeek = repeatWeek;
	}

	public String getScheduleDateTime() {
		return scheduleDateTime;
	}

	public void setScheduleDateTime(String scheduleDateTime) {
		this.scheduleDateTime = scheduleDateTime;
	}

	public String getFutureDateTime() {
		return futureDateTime;
	}

	public void setFutureDateTime(String futureDateTime) {
		this.futureDateTime = futureDateTime;
	}

	@Override
	public String toString() {
		return "ScheduleSwitch [id=" + id + ", switchId=" + switchId + ", profileId=" + profileId + ", switchStatus="
				+ switchStatus + ", dimmerValue=" + dimmerValue + ", dimmerStatus=" + dimmerStatus + ", lockStatus="
				+ lockStatus + ", repeatStatus=" + repeatStatus + ", repeatWeek=" + repeatWeek + ", userId=" + userId
				+ ", scheduleDateTime=" + scheduleDateTime + ", futureDateTime=" + futureDateTime + "]";
	}
	
}
