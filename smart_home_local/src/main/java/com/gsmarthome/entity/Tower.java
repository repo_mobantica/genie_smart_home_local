package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Tower {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	private String towerinfo;
	
	public Tower()
	{
		super();
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTowerinfo() {
		return towerinfo;
	}


	public void setTowerinfo(String towerinfo) {
		this.towerinfo = towerinfo;
	}

	@Override
	public String toString() {
		return "Tower [id=" + id + ", towerinfo=" + towerinfo + "]";
	}

}
