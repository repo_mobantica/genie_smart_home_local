package com.gsmarthome.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RemoteDetails {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;	
	private String brandName;
	private String modelNumber;
	private String remoteInfo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getBrandName() {
		return brandName;
	}
	
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	public String getModelNumber() {
		return modelNumber;
	}
	
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public String getRemoteInfo() {
		return remoteInfo;
	}
	
	public void setRemoteInfo(String remoteInfo) {
		this.remoteInfo = remoteInfo;
	}
	
	@Override
	public String toString() {
		return "RemoteDetails [id=" + id + ", brandName=" + brandName + ", modelNumber=" + modelNumber + ", remoteInfo="
				+ remoteInfo + "]";
	}
	
}
