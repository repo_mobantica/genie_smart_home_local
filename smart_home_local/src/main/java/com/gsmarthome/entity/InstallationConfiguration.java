package com.gsmarthome.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author Kapil S Shinde
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class InstallationConfiguration {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Column(length=1000000)
	private String configurationString;
	
	@Column(name="secure_ssid")
	private String secureSSId;
	
	@Column(name="secure_password")
	private String securePassword;
	
	@Column
	private Date created;
	
	@Column
	private Date updated;
	
	@PrePersist
	protected void onCreate() {
	    created = new Date();
	}
	
	@PreUpdate
	protected void onUpdate() {
	    updated = new Date();
	}

	public InstallationConfiguration() {
		
	}

	public String getConfigurationString() {
		return configurationString;
	}

	public void setConfigurationString(String configurationString) {
		this.configurationString = configurationString;
	}

	public InstallationConfiguration(String configurationString) {
		this.configurationString = configurationString;
	}
	
	public String getSecureSSId() {
		return secureSSId;
	}

	public void setSecureSSId(String secureSSId) {
		this.secureSSId = secureSSId;
	}

	public String getSecurePassword() {
		return securePassword;
	}

	public void setSecurePassword(String securePassword) {
		this.securePassword = securePassword;
	}

	@Override
	public String toString() {
		return "InstallationConfiguration [id=" + id + ", configurationString=" + configurationString + ", secureSSId="
				+ secureSSId + ", securePassword=" + securePassword + ", created=" + created + ", updated=" + updated
				+ "]";
	}

}