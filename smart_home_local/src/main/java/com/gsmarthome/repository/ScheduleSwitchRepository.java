package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.ScheduleSwitch;;

public interface ScheduleSwitchRepository extends CrudRepository<ScheduleSwitch, Long> {
	
	@Query("select s from ScheduleSwitch s where s.scheduleDateTime = ?#{[0]}")
	public ScheduleSwitch[] findSwitchByDate(String scheduleDateTime);
	
	@Query("SELECT s from ScheduleSwitch s WHERE s.profileId = ?#{[0]}")
	public ScheduleSwitch[] findScheduleByProfileId(Long profileId);
	
	@Query("UPDATE ScheduleSwitch s SET s.scheduleDateTime = ?#{[1]} WHERE s.userId = ?#{[0]}")
	public int updateSwitchDate(Long userId, String scheduleDateTime);
	
	@Query("DELETE FROM ScheduleSwitch s WHERE s.profileId= ?#{[0]}")
	public int deleteByProfileId(Long profileId);
	
	@Query("select s from ScheduleSwitch s where s.userId = ?#{[0]}")
	public ScheduleSwitch[] findScheduleByUserId(Long userId);

}
