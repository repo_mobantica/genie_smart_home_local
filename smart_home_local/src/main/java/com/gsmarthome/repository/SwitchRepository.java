package com.gsmarthome.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Switch;
import com.gsmarthome.entity.IOTProduct;
import java.util.List;

public interface SwitchRepository extends CrudRepository<Switch, Long> {
	
	@Query("select s from Switch s where s.switchNumber = ?#{[0]} and s.iotProduct.id = ?#{[1]}")
	public Switch checkSwitchAlreadyExist(int switchNumber,long iotProductId);

	@Query("select s from Switch s where s.switchStatus = ?#{[1]} and s.iotProduct.room.id = ?#{[0]}")
	public Switch[] getSwichbyRoomId(long ROOMID, String onoffStatus);
	
	@Query("select s from Switch s where s.switchNumber = ?#{[0]} and s.iotProduct.id = ?#{[1]}")
	public Switch getSwitchaldetails(int switchNumber,long iotProductId);

	@Query("select s from Switch s where s.iotProduct.id = ?#{[0]}")
	public Switch[] getSwitchByIOTProductId(int iotProductId);
	
	@Query("select s from Switch s where s.dimmerStatus = ?#{[0]} OR s.dimmerStatus = ?#{[1]}")
	public Switch[] getHomeLock(String dimmerStatus, String dimmerStatusNormalLock);

	@Query("select s from Switch s where s.dimmerStatus = ?#{[0]}")
	public Switch[] getHomeLock(String dimmerStatus);
	
	@Query("select s from Switch s where s.dimmerStatus = ?#{[0]} AND s.id = ?#{[1]}")
	public Switch findHomeLockByIdnDimmerStatus(String dimmerStatus, Long switchId);
	
	List<Switch> findByIotProduct(IOTProduct iotproduct);
	
	public List<Switch> findByDimmerStatus(String dimmerStatus);

}
