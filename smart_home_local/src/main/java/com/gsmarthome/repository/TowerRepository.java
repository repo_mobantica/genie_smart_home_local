package com.gsmarthome.repository;


import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Tower;



public interface TowerRepository extends CrudRepository<Tower, Long> {
	
	  	

}
