package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.ShareControlDetails;
import com.gsmarthome.entity.UserDetails;

public interface ShareControlDetailsRepository extends CrudRepository<ShareControlDetails, String> {

	ShareControlDetails findByEmail(String email);
	
	ShareControlDetails findByPhoneNumber(String phoneNumber);
	
}
