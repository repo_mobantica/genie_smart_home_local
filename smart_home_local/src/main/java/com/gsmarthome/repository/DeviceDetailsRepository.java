package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.DeviceDetails;
public interface DeviceDetailsRepository extends CrudRepository<DeviceDetails, String> {
	DeviceDetails findBydeviceId(String deviceId);
}
