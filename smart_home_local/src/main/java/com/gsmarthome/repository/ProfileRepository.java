package com.gsmarthome.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Profile;
import com.gsmarthome.entity.UserOTP;;

public interface ProfileRepository extends CrudRepository<Profile, Long> {	
	
	@Query("select p from Profile p where p.user.id = ?#{[0]}")
	public List<Profile> getProfileByUserId(Long userId);
	
	public Profile[] findByUserId(Long userId);

}
