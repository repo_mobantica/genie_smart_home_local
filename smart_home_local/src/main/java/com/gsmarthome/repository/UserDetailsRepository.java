package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Home;
import com.gsmarthome.entity.UserDetails;

public interface UserDetailsRepository extends CrudRepository<UserDetails, Long>{
	
	UserDetails findByEmail(String email);
	
	UserDetails findByPhoneNumber(String phoneNumber);
	
	UserDetails[] findByHome(Home home);
	
	UserDetails[] findByHomeId(Long HomeId);
}
