package com.gsmarthome.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.InstallationConfiguration;
import com.gsmarthome.entity.Notification;


public interface InstallationConfigurationRepository extends CrudRepository<InstallationConfiguration, Long> {
	@Query("select n from InstallationConfiguration n")
	public InstallationConfiguration[] getInstallationConfiguration();
}
