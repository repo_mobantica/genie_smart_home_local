package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.Panel;
public interface PanelRepository extends CrudRepository<Panel, Long>{
	 Panel findByPanelId(Integer panelId);	 
	 Panel findBypanelIp(String panelIp);
	 Panel findBypanelName(String panelName);
	 
		@Query("select p from Panel p")
		public Panel[] getPanel();
		
		@Query("select p from Panel p where p.panelName like ?#{[0]}")
		public Panel getBypanelName(String panelName );

}
