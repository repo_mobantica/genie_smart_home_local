package com.gsmarthome.repository;

import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.UserOTP;

public interface UserOTPRepository extends CrudRepository<UserOTP, Long> {
	
	 UserOTP findByUserId(Long userId);

}
