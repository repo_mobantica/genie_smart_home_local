package com.gsmarthome.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gsmarthome.entity.IOTProduct;
import com.gsmarthome.entity.Room;

public interface IOTProductRepository extends CrudRepository<IOTProduct, Long> {
	 
	IOTProduct findByiotProductNumber(Integer iotProductNumber);
	
	IOTProduct findByRoom(Room room);
	
	@Query("select i from IOTProduct i where i.room.id = ?#{[0]}")
	public IOTProduct[] findAllByRoomId(Long roomId);
	
}
