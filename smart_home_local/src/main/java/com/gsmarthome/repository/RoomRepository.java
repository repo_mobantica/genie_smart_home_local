package com.gsmarthome.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gsmarthome.entity.Room;

public interface RoomRepository extends CrudRepository<Room, Long> {
	
	Room findRoomByroomIdentifier(String roomIdentifier);
	
	@Modifying
	@Transactional
	@Query("update Room r set hideStatus = ?#{[1]} where r.id = ?#{[0]}")
	int updateHideStatusById(Long roomId, String hideStatus);
}
