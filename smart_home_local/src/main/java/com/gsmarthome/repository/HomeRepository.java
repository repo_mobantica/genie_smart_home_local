package com.gsmarthome.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.gsmarthome.entity.Home;


public interface HomeRepository extends CrudRepository<Home, Long> {
	
	public	Home findByHomeName(String homename);	

	public Home findByIsArmed(Integer isArmed);	

	List<Home> findById(Long id);
	
	@Modifying
    @Transactional
    @Query("UPDATE Home h SET isArmed = ?#{[1]} where h.id = ?#{[0]}")
    public Integer updateHomeArmed(Long homeId, Integer isArmed);
	
}
