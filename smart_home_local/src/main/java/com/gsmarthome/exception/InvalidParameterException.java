package com.gsmarthome.exception;

public class InvalidParameterException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidParameterException() {
        super();
    }
	
	public InvalidParameterException(String msg) {
        super(msg);
    }
	
	

}
