package com.gsmarthome;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.gsmarthome.constants.Constants;
import com.gsmarthome.dto.ChangeStatusByHomeDTO;
import com.gsmarthome.dto.ChangeStatusByRoomResponseDTO;
import com.gsmarthome.dto.ChangeSwitchStatusDTO;
import com.gsmarthome.dto.ChangeSwitchStatusResponseDTO;
import com.gsmarthome.entity.EventHolderBean;
import com.gsmarthome.entity.Profile;
import com.gsmarthome.entity.ScheduleSwitch;
import com.gsmarthome.entity.UserDetails;
import com.gsmarthome.repository.ActivityRepository;
import com.gsmarthome.repository.CommandRepository;
import com.gsmarthome.repository.DeviceDetailsRepository;
import com.gsmarthome.repository.HomeRepository;
import com.gsmarthome.repository.IOTProductRepository;
import com.gsmarthome.repository.PanelRepository;
import com.gsmarthome.repository.ProfileRepository;
import com.gsmarthome.repository.RoomRepository;
import com.gsmarthome.repository.RoomTypeRepository;
import com.gsmarthome.repository.ScheduleSwitchRepository;
import com.gsmarthome.repository.SwitchRepository;
import com.gsmarthome.repository.SwitchTypeRepository;
import com.gsmarthome.repository.UserDetailsRepository;
import com.gsmarthome.service.NotificationService;
import com.gsmarthome.service.ProfileService;
import com.gsmarthome.service.SwitchService;

@Service
public class ScheduledTasks {

	@Autowired
	EventHolderBean eventHolderBean;

	@Autowired
	NotificationService notificationService;

	@Autowired
	CommandRepository commandRepository;

	@Autowired
	SwitchService switchService;

	@Autowired
	ProfileService profileService;

	@Autowired
	RoomTypeRepository roomTypeRepository;

	@Autowired
	SwitchRepository switchRepository;

	@Autowired
	PanelRepository panelRepository;

	@Autowired
	HomeRepository homeRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	IOTProductRepository iotProductRepository;
	@Autowired
	DeviceDetailsRepository deviceDetailsRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	SwitchTypeRepository switchTypeRepository;

	@Autowired
	ActivityRepository activityRepository;
	@Autowired

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

	private static Long userId = 0l;

	private static final SimpleDateFormat dateFormatByDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat dateFormatByTime = new SimpleDateFormat("HH:mm:00");

	@Autowired
	ScheduleSwitchRepository scheduleSwitchRepository;

	@Async
	@Scheduled(fixedRate = 1000)
	public void reportCurrentTime1() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_WEEK);

		try {
			Date d1 = new Date();
			// Date d1=dateFormat1.format(new Date());
			int sec = d1.getSeconds();
			if (sec == 0) {
				logger.info("from SECOND is " + dateFormatByDate.format(new Date()));
				logger.info("from SECOND is " + dateFormatByTime.format(new Date()));

				ScheduleSwitch scheduleSwitchListByDate[] = scheduleSwitchRepository
						.findSwitchByDate(dateFormatByDate.format(new Date()));

				ScheduleSwitch scheduleSwitchListByTime[] = scheduleSwitchRepository
						.findSwitchByDate(dateFormatByTime.format(new Date()));

				if (scheduleSwitchListByTime.length > 0) {
					for (int i = 0; i < scheduleSwitchListByTime.length; i++) {

						if (scheduleSwitchListByTime[i].getRepeatStatus().equals("1")) {

							List<String> weekList = Arrays
									.asList(scheduleSwitchListByTime[i].getRepeatWeek().split(","));

							for (int j = 0; j < weekList.size(); j++) {
								if (weekList.get(j).equals("1")) {

									if (j == 0) {
										if (Constants.SUNDAY == day) {
											// execute switch on/off command
											logger.info("*****************Sunday**************");
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									} else if (j == 1) {
										if (Constants.MONDAY == day) {
											// execute switch on/off command
											logger.info("*****************Monday**************");
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									} else if (j == 2) {
										if (Constants.TUESDAY == day) {
											// execute switch on/off command
											logger.info("*****************Tuesday**************");
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}

										}
									} else if (j == 3) {
										if (Constants.WEDNESDAY == day) {
											// execute switch on/off command
											logger.info("*****************Wednesday**************");
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									} else if (j == 4) {
										if (Constants.THURSDAY == day) {
											logger.info("*****************Thursday**************");
											// execute switch on/off command
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									} else if (j == 5) {
										if (Constants.FRIDAY == day) {
											logger.info("*****************Friday**************");
											// execute switch on/off command
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									} else if (j == 6) {
										if (Constants.SATURDAY == day) {
											logger.info("*****************Saturday**************");
											// execute switch on/off command
											if (scheduleSwitchListByTime[i].getSwitchId() != 0) {
												scheduleSwitchOnOFF(scheduleSwitchListByTime[i]);
											} else if (scheduleSwitchListByTime[i].getProfileId() != 0) {
												profileOnOff(scheduleSwitchListByTime[i]);
											}
										}
									}

								}
							}

						}

						// Thread.sleep(2000);

					}
				}
				if (scheduleSwitchListByDate.length > 0) {
					for (int i = 0; i < scheduleSwitchListByDate.length; i++) {
						// scheduleSwitchOnOFF(scheduleSwitchListByDate[i]);
						if (scheduleSwitchListByDate[i].getSwitchId() != 0) {
							scheduleSwitchOnOFF(scheduleSwitchListByDate[i]);
						} else if (scheduleSwitchListByDate[i].getProfileId() != 0) {
							profileOnOff(scheduleSwitchListByDate[i]);
						}
						logger.info("*****************By Date Switch On/OFF**************");
						// Thread.sleep(2000);
					}
				}
			}

		} catch (Exception ex) {
			logger.info("Error is Scheduling the Switch Status " + ex.getMessage());
			ex.printStackTrace();

		}
	}

	public void scheduleSwitchOnOFF(ScheduleSwitch scheduleSwitch) {
		ChangeSwitchStatusDTO changeSwitchStatusDTO = new ChangeSwitchStatusDTO();
		changeSwitchStatusDTO.setSwitchId(scheduleSwitch.getSwitchId());
		changeSwitchStatusDTO.setSwitchStatus(scheduleSwitch.getSwitchStatus());
		changeSwitchStatusDTO.setDimmerValue(scheduleSwitch.getDimmerValue());
		changeSwitchStatusDTO.setUserid(scheduleSwitch.getUserId());
		changeSwitchStatusDTO.setMessageFrom("Schedular");
		try {
			eventHolderBean.setEventFired(true);

			Iterable<UserDetails> userDetailsList = userDetailsRepository.findAll();

			Iterator<UserDetails> itr = userDetailsList.iterator();

			while (itr.hasNext()) {
				UserDetails user = itr.next();

				userId = user.getId();

				if (user.getDeviceId() != null) {
					try {
//						UserDetails userDetails = userDetailsRepository.findOne(scheduleSwitch.getUserId());
						String schedulerFired = "Schedular set by "+userDetailsRepository.findOne(scheduleSwitch.getUserId()).getFirstName()+" successfully executed. For more details see activity tab";
						notificationService.sendNotificationMessage(user,schedulerFired);
						logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Notification sent to all users regarding schedular fired\t"+schedulerFired);

					} catch (Exception e) {
						e.printStackTrace();
						logger.error("Exception Message while sending Notification on pyramid restart: "
								+ e.getMessage() + " Cause Exception:" + e.getCause());
					}
				}
			}
			ChangeSwitchStatusResponseDTO changeSwitchStatusRespDTO = switchService.switchOnOff(changeSwitchStatusDTO);
		} catch (Exception e) {
			logger.error("Exception in switchOnOff() Schedular Task " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void profileOnOff(ScheduleSwitch scheduleSwitch) {
		Profile profile = profileRepository.findOne(scheduleSwitch.getProfileId());

		ChangeStatusByHomeDTO changeStatusByHomeDTO = new ChangeStatusByHomeDTO();

		changeStatusByHomeDTO.setSwitchId(profile.getSwitchList());
		changeStatusByHomeDTO.setDimmerValues(profile.getDimmerValues());
		changeStatusByHomeDTO.setModeName(profile.getProfileName());
		changeStatusByHomeDTO.setOnoffstatus(profile.getSwitchOnOffStatus());
		changeStatusByHomeDTO.setProfileId(profile.getId());
		changeStatusByHomeDTO.setUserId(profile.getUser().getId());
		changeStatusByHomeDTO.setMessageFrom("ProfileScheduler");

		try {
			eventHolderBean.setEventFired(true);

			Iterable<UserDetails> userDetailsList = userDetailsRepository.findAll();

			Iterator<UserDetails> itr = userDetailsList.iterator();

			while (itr.hasNext()) {
				UserDetails user = itr.next();

				userId = user.getId();

				if (user.getDeviceId() != null) {
					try {
						String schedulerFired = "Profile Schedular set by "+userDetailsRepository.findOne(scheduleSwitch.getUserId()).getFirstName()+" successfully executed. For more details see activity tab";
						notificationService.sendNotificationMessage(user,schedulerFired);
						logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Notification sent to all users regarding Profile schedular fired\t"+schedulerFired);

					} catch (Exception e) {
						e.printStackTrace();
						logger.error("Exception Message while sending Notification on pyramid restart: "
								+ e.getMessage() + " Cause Exception:" + e.getCause());
					}
				}
			}
			List<ChangeStatusByRoomResponseDTO> changeSwitchStatusRespDTO = profileService
					.profileOnOff(changeStatusByHomeDTO);
		} catch (Exception e) {
			logger.error("Exception in profileOnOff() Schedular Task " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Async
	@Scheduled(fixedRate = 30 * 60 * 1000)
	public void DeleteCommands() {
		try {

			Page<Integer> page = commandRepository.findLatestId(new PageRequest(0, 100));
			commandRepository.deleteByExcludedId(page.getContent());

			Page<Integer> pageActivity = activityRepository.findLatestId(new PageRequest(0, 1000));
			activityRepository.deleteByExcludedId(pageActivity.getContent());

		} catch (Exception ex) {
			System.out.println("Exception in DeleteCommands() Message: " + ex.getMessage());
			ex.printStackTrace();

		}
	}

}
