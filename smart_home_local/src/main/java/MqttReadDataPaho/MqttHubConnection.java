package MqttReadDataPaho;

import javax.swing.SwingUtilities;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.util.json.JSONObject;
import com.gsmarthome.constants.Constants;
import com.gsmarthome.controller.SwitchController;
import com.gsmarthome.repository.PanelRepository;

public class MqttHubConnection implements MqttCallback {

	

String subscribePanel;
private String localClient = "119";
private String localInternet = "119cdcd";
private static MqttClient mqttConnectionLocal = null;
private static MqttClient mqttConnectionInternet1 = null;

private static String localBroker = "tcp://localhost:1883";

private static String internetBroker = "tcp://192.168.0.2:1883";

private static MqttHubConnection currObj = null;

@Autowired
PanelRepository panelRepository;

public MqttHubConnection() {}

public MqttHubConnection(String csubscribePanel)
{
  subscribePanel = csubscribePanel;
}

public MqttClient getLocalMqttConnection() throws MqttException {
	System.out.println(" IN   MqttHubConnection");
  if ((mqttConnectionLocal != null) && (mqttConnectionLocal.isConnected())) {

	  mqttConnectionLocal.subscribe("updateStatus");
    System.out.println("reusing conntion getLocalMqttConnection...");
    return mqttConnectionLocal;
  }
  try {
    MemoryPersistence persistence = new MemoryPersistence();
    mqttConnectionLocal = new MqttClient(localBroker, localClient, persistence);
    MqttConnectOptions connOpts = new MqttConnectOptions();
    connOpts.setCleanSession(true);
    mqttConnectionLocal.connect(connOpts);
    mqttConnectionLocal.subscribe("updateStatus");
    mqttConnectionLocal.setCallback(this);
    System.out.println("Connected");
    return mqttConnectionLocal;
  } catch (MqttException exception) {
    System.out.println("MqttException exception = " + exception);
    System.out.println("unable to connect local hub\t" + localBroker);
    System.out.println("trying...");
    
    getObj().getLocalMqttConnection();
    exception.getStackTrace();
  }
  return mqttConnectionLocal;
}


public MqttClient getInternetMqttConnection() throws MqttException
{
  if ((mqttConnectionInternet1 != null) && (mqttConnectionInternet1.isConnected())) {
    System.out.println("reusing connection...for AWS");
    return mqttConnectionInternet1;
  }
  try {
    MemoryPersistence persistence = new MemoryPersistence();
    mqttConnectionInternet1 = new MqttClient(internetBroker, localInternet, persistence);
    MqttConnectOptions connOpts = new MqttConnectOptions();
    connOpts.setCleanSession(true);
    
    mqttConnectionInternet1.connect(connOpts);
   // mqttConnectionInternet.setCallback(this);
    
    System.out.println("Connected");
    return mqttConnectionInternet1;
  } catch (MqttException exception) {
    System.out.println("unable to connect to AWS hub");
    System.out.println("trying...");
    
    getObj().getLocalMqttConnection();
  }
  return mqttConnectionInternet1;
}

public static MqttHubConnection getObj()
{
  if (currObj == null) {
    currObj = new MqttHubConnection();
    return currObj;
  }
  return currObj;
}



@Override
public void connectionLost(Throwable arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void deliveryComplete(IMqttDeliveryToken arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void messageArrived(String arg0, MqttMessage mqttmessage) throws Exception {
	// TODO Auto-generated method stub
	System.out.println("arg0  = =  "+arg0);
	System.out.println("arg1 refresh = ="+mqttmessage);
	 
	  String msg1 = "";
	  try
	  {
	    msg1 = "" + mqttmessage;
	  } catch (Exception e) {
	    msg1 = "";
	  }
	  
	  getObj().getLocalMqttConnection();
	  getObj().getInternetMqttConnection();
	  String[] msg2 = msg1.split("/");
	  System.out.println("msg2 = "+msg2[0]);
	  
	  if (!msg1.equalsIgnoreCase(""))
	  {
		  

		    if(msg2.length==1 && msg1.charAt(0)=='$')
		    {
		    	
		    	
		    	MqttMessage refreshMessage = new MqttMessage(mqttmessage.toString().getBytes());
		    	System.out.println("refreshMessage= "+refreshMessage);
		    	
		    	System.out.println("updateStatus= "+arg0);
		    	
		    	SwingUtilities.invokeLater(new Runnable() {
		    		@Override
		    		public void run() {
		    			try {
		    				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t getLocalMqttConnection  "+"updateStatus");
		    				MqttlConnection.getObj().getLocalMqttConnection().publish("updateStatus", refreshMessage);
		    			} catch (MqttException e) {
		    				e.printStackTrace();
		    			}
		    		}
		    	});
		    	

				// for central /AWS
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t getInternetMqttConnection  "+"updateStatus");
							MqttHubConnection.getObj().getInternetMqttConnection().publish("updateStatus", refreshMessage);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				});
			/*	
				try {
				SwitchController switchController=new SwitchController();
				boolean flag = switchController.updateSwitchStatus(msg1);
				}catch (Exception e) {
					System.out.println("error =- "+e);
					// TODO: handle exception
				}*/
				
		    }
		      
		  
	  }
	
	
	
/*
	MqttMessage refreshMessage = new MqttMessage("panel1".toString().getBytes());
	SwingUtilities.invokeLater(new Runnable() {
		@Override
		public void run() {
			try {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> aws topic 2:\t"+"refresh");
				MqttlConnection.getObj().getLocalMqttConnection().publish("refresh", refreshMessage);
			} catch (MqttException e) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> problem:\t"+"refresh");
				e.printStackTrace();
			}
		}
	});*/
}

}
